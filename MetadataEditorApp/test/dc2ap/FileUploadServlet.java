package dc2ap;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileItemFactory;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

/**
 * Servlet to process .rdf files opened by the user.
 * This Servlet receive a Form with a File from the client and save  
 * the file in the temporary files server folder.
 *  
 * http://www.jroller.com/hasant/entry/fileupload_with_gwt
 * http://commons.apache.org/fileupload/using.html
 * http://stackoverflow.com/questions/9160535/is-it-possible-to-upload-a-file-via-gwt-rpc-servlets
 */
@SuppressWarnings("serial")
public class FileUploadServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// Check that we have a file upload request
		// process only multipart requests
        if (ServletFileUpload.isMultipartContent(request)) {
        	// Create a factory for disk-based file items
        	FileItemFactory factory = new DiskFileItemFactory();

        	// Create a new file upload handler
        	ServletFileUpload upload = new ServletFileUpload(factory);

        	// Parse the request
			try {
				List<FileItem> items = upload.parseRequest(request);
				for (FileItem item : items) {        
					if (item.isFormField()) continue;
					
					// process only file upload - discard other form item types
	        		if (!item.isFormField()){
	        			// Upload the file to the server and add the file name 
	        			// to the server response
				        processUploadedFile(item, response);
				    }
				}
			} catch (Exception e) {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                        "An error occurred while creating the file: " + e.getMessage());
			}
        } else {
            response.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE,
                    "Request contents type is not supported by the servlet.");
        }
	}
	
	/**
	 * Process the file from the client, save it in the 
	 * temporaryFiles folder. Add to the server response 
	 * the name of the file created.
	 * 
	 * @param item The file item to be saved
	 * @param fileName the file name to be saved in the server
	 * @param response The HttpServletResponse to the client
	 *
	 * @throws Exception if it was not possible to create the 
	 * file in the server 
	 */
	private void processUploadedFile(FileItem item,
			HttpServletResponse response) throws Exception {
		
		// get the path of the repository files in the server
		ServletContext context = getServletConfig().getServletContext();
		final String TEMPORARY_FILES_PATH = context.getRealPath("temporaryFiles");

		// file name in the server
		Date now = new Date();
        String fileName = "uploadedFile_" + now.getTime() + ".rdf";
        
		// save the file in the server
        File uploadedFile = new File(TEMPORARY_FILES_PATH, fileName);

        if (uploadedFile.createNewFile()) {
        	item.write(uploadedFile);
  	
        	// response to the client, return the file name
        	response.setStatus(HttpServletResponse.SC_CREATED);
        	response.getWriter().print(fileName);
        	response.flushBuffer();
        
        	///GAMBIARRA - TENTAR RETORNAR O OBJETO DC2AP 
        	// AO INVES DE APENAS O NOME DO ARQUIVO
			// get the path of the repository files in the server
			
			// the full file path name on the server
        	/*
			String fullFilePath = temporaryFilesPath + "\\" +fileName;
	        // Open RDF file to a DC2AP object
    		DC2AP object = xmlService.readRdfFile(fullFilePath);
    		req.setAttribute("dc2apObject", object);*/
        }
	}
}
