package dc2ap;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.InlineHyperlink;

public class UI_TEST extends DialogBox {

	public UI_TEST(String rdfScript) {
		setGlassEnabled(true);
		setHTML("RDF Script for this Resource");
		
		VerticalPanel dialogContents = new VerticalPanel();
		setWidget(dialogContents);
		dialogContents.setSize("600px", "540px");
		
		ScrollPanel mainScrollPanel = new ScrollPanel();
		dialogContents.add(mainScrollPanel);
		mainScrollPanel.setSize("800px", "500px");
		
		HTML scriptRDF = new HTML("ScriptRDF", true);
		mainScrollPanel.setWidget(scriptRDF);
		scriptRDF.setSize("100%", "100%");
	    
	    HorizontalPanel horizontalPanel = new HorizontalPanel();
	    horizontalPanel.setStyleName("backgroundDarkTop");
	    horizontalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
	    dialogContents.add(horizontalPanel);
	    horizontalPanel.setSize("100%", "50px");
	    
	    InlineHyperlink nlnhprlnkBlablabla = new InlineHyperlink("blablabla", false, "newHistoryToken");
	    horizontalPanel.add(nlnhprlnkBlablabla);
	    
	    HorizontalPanel buttonPanel = new HorizontalPanel();
	    horizontalPanel.add(buttonPanel);
	    buttonPanel.setStyleName("gwt-HorizontalPanel");
	    buttonPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
	    buttonPanel.setSpacing(5);
	    dialogContents.setCellHorizontalAlignment(buttonPanel, HasHorizontalAlignment.ALIGN_RIGHT);
	    buttonPanel.setSize("224px", "100%");
	    
	    		
		// Add the save button at the bottom of the dialog
	    Button saveButton = new Button(
	        "Save", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	  hide();
	          }
	    });
	    saveButton.setStyleName("button");
	    saveButton.setTitle("Save RDF File");
	    	    
		// Add the close button at the bottom of the dialog
	    Button closeButton = new Button(
	        "Close", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	  hide();
	          }
	    });
	    closeButton.setStyleName("button");
	    closeButton.setTitle("Close Dialog Box");
	    
	    buttonPanel.add(saveButton);
	    buttonPanel.add(closeButton);
	}

}
