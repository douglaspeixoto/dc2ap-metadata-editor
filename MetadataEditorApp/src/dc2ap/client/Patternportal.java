/*
 * Este programa � protegido por leis internacionais de software 
 * e pela lei de Deus. 
 * Afinal de contas, s� Deus sabe como essa merda funciona...
 */

package dc2ap.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HTML;

import dc2ap.client.ui.Home;
import dc2ap.client.ui.MetadataEditor;

/**
 * Index page of the application.
 * Main Page.
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
public class Patternportal implements EntryPoint {
	// The panel called in the center of the page
	private static Home homePage = new Home();
	private static MetadataEditor editor = new MetadataEditor();

	// to fit the pages in the center
	private static VerticalPanel centerPanel = new VerticalPanel();
		
	/**
	 * Main function
	 */
	public void onModuleLoad() {
		RootPanel rootPanel = RootPanel.get();
		rootPanel.setStyleName("background");

		// set alert window's name
		Window.setTitle("DC2AP Metadata Editor");
		
		final VerticalPanel mainPanel = new VerticalPanel();
		mainPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		mainPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		mainPanel.setSize("100%", "");
		rootPanel.add(mainPanel, 0, 0);
		
		Image header = new Image("images/header.png");
		header.setStyleName("imageHeader");
		mainPanel.add(header);
		
		centerPanel.setSize("100%", "100%");
		centerPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		centerPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
		mainPanel.add(centerPanel);
		
		// add the home page as default
		centerPanel.add(homePage);	
		
		HTML footer = new HTML("Copyright \u00A9 2012/2013 - Project Analysis Patterns Reuse Infrastructure<br/> " +
				"Departamento de Inform\u00E1tica - Universidade Federal de Vi\u00E7osa", true);
		footer.setStyleName("footer");
		footer.setSize("1240px", "50px");
		
		mainPanel.add(footer);
	}

	/**
	 * Open the metadata editor in the center of the main page.
	 */
	public static void openEditor(){
		// create a new editor every time you open it
		editor.createNewEditor();
		centerPanel.clear();
		centerPanel.add(editor);
	}
	
	/**
	 * Back to the home page.
	 * Add the home page in the center of the main page.
	 */
	public static void backToHomePage(){
		centerPanel.clear();
		centerPanel.add(homePage);
	}

}
