package dc2ap.client.rpc;

import java.util.List;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import dc2ap.client.entities.DC2AP;

/**
 * The client side stub for the RPC (Remote Procedure Call) service.
 * Service to communicate with the server-side.
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
@RemoteServiceRelativePath("rpcService") // Servlet name
public interface RPCService extends RemoteService{
	
	/**
	 * Validate the metadata according with the DC2AP specification.
	 * 
	 * @param dc2apObject the DC2AP object to be validated.
	 * 
	 * @return A list of errors found (Error Log) or an empty list
	 * if the metadata was well validated. 
	 */
	public List<String> validateMetadata(final DC2AP dc2apObject);
	
	/**
	 * Generate a RDF script from a DC2AP object.
	 * 
	 * @param dc2apObject the DC2AP object to be parsed.
	 * 
	 * @return A string with the script of the RDF document, or:
	 * <br/> 'ValidationException' in the case of non-validation of the metadata.
	 * <br/> 'TransformerException'
	 * <br/> 'ParserConfigurationException' 
	 */
	public String generateScriptRDF(final DC2AP dc2apObject);
	
	/**
	 * Generate a RDF file and save it to the disk.
	 * 
	 * @param rdfScript the script of the RDF file.
	 * @param fileName the name of the file to be saved 
	 * in the server-side (without extension)
	 * 
	 * @return Return Exceptions:
	 * <br/> 'IOException' if it was not possible to create the file.
	 */
	public String saveFileRDF(final String rdfScript, final String fileName);

	/**
	 * Read a RDF file on the server and return a DC2AP object with its fields.
	 * Read a file already uploaded to the server.
	 * 
	 * @param fileName The file name in the temporary folder in the server.
	 * 
	 * @return A DC2AP object with the fields read from the RDF file.
	 * <br/> Return null if it was not possible to create the object 
	 * or read the file.
	 */
	public DC2AP readFileRDF(final String fileName);
	
	/**
	 * Save the project to the user disk (XML document).
	 * 
	 * @param dc2apObject the metadata to be saved.
	 * @param fileName the name of the file to be saved 
	 * in the server-side.
	 * 
	 * @return Return Exception:
	 * <br/> 'IOException' if it was not possible to create the file.
	 * <br/> 'TransformerException'
	 * <br/> 'ParserConfigurationException'
	 */
	public String saveProject(final DC2AP dc2apObject, final String fileName);

	/**
	 * Delete a file from the server.
	 * 
	 * @param fileName the name of the file to be deleted 
	 * in the server-side.
	 * 
	 * @return true if the file was successfully deleted.
	 */
	public Boolean deleteTemporaryFile(final String fileName); 
}
