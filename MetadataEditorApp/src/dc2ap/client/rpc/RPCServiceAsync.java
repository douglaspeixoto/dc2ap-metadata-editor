package dc2ap.client.rpc;

import java.util.List;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dc2ap.client.entities.DC2AP;

/**
 * The async counterpart of <code>RPCService</code>.
 * (Make the call with the server-side).
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
public interface RPCServiceAsync {

	/**
	 * Validate the metadata according with the DC2AP specification.
	 */
	void validateMetadata(DC2AP dc2apObject, AsyncCallback<List<String>> callback);
	
	/**
	 * Generate a RDF script from a DC2AP object.
	 */
	void generateScriptRDF(DC2AP dc2apObject, AsyncCallback<String> callback);
	
	/**
	 * Generate a RDF file and save it to the disk.
	 */
	void saveFileRDF(String rdfScript, String fileName, AsyncCallback<String> callback);

	/**
	 * Read a RDF file on the server and return a DC2AP object with its fields.
	 * Read a file already uploaded to the server.
	 */
	void readFileRDF(String fileName, AsyncCallback<DC2AP> callback);
	
	/**
	 * Save the project to the disk (XML document).
	 */
	void saveProject(DC2AP dc2apObject, String fileName, AsyncCallback<String> callback);	
	
	/**
	 * Delete a file from the server.
	 */
	void deleteTemporaryFile(String fileName, AsyncCallback<Boolean> callback);

	
}
