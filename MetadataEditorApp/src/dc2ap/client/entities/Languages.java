package dc2ap.client.entities;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.user.client.ui.MultiWordSuggestOracle;

/**
 * Entity the contains the languages definitions.
 * See more in: http://www.sil.org/iso639-3/codes.asp
 * 
 * @author Douglas A. Peixoto - 06/2012
 *
 */
public class Languages {		
	private MultiWordSuggestOracle valuesOracle = new MultiWordSuggestOracle();
	private MultiWordSuggestOracle labelsOracle = new MultiWordSuggestOracle();
	
	private List<String> valuesStringList = new ArrayList<String>();
	private List<String> labelsStringList = new ArrayList<String>();
	
	/**
	 * Get the language values.
	 * 
	 * @return MultiWordSuggestOracle with the language values
	 */
	public MultiWordSuggestOracle getValuesOracle() {
		return valuesOracle;
	}
	/**
	 * Get the language labels.
	 * 
	 * @return MultiWordSuggestOracle with the language labels
	 */
	public MultiWordSuggestOracle getLabelsOracle() {
		return labelsOracle;
	}
	
	/**
	 * Get the languages ID.
	 * 
	 * @return String List with the languages ID
	 */
	public List<String> getValuesStringList() {
		return valuesStringList;
	}
	/**
	 * Get the languages Names.
	 * 
	 * @return String List with the languages name
	 */
	public List<String> getLabelsStringList() {
		return labelsStringList;
	}

}
