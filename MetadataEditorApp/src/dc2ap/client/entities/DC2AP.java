package dc2ap.client.entities;

import java.io.Serializable;
import java.util.List;

/**
 * The DC2AP entity itself, with its properties.
 * Contains only the fields which might be filled.
 * 
 * @author Douglas A. Peixoto - 06/2012
 */
@SuppressWarnings("serial")
public class DC2AP implements Serializable{
	private String identifier;        		// 1.
	private String title;					// 2.
	private List<String> alternativeTitle;	// 2.1.
	private List<String> creator;			// 3.
	private List<String> subject;			// 4.
	private String problem; 				// 5.1.
	private List<String> motivation; 		// 5.2.
	private List<String> example;			// 5.2.1.
	private List<String> knownUses; 		// 5.2.2.
	private String context;					// 5.3.
	private List<String> publisher; 		// 6.
	private List<String> contributor;		// 7.
	private String created; 		// 8.1.
	private String modified;		// 8.2.
	private String type; 			// 9.
	private String notation;		// 9.1.
	private List<String> format;  	// 10.
	private String source; 			// 11.
	private String languageValue; 	// 12.
	private String languageLabel; 	// 12.
	private String isVersionOf; 			// 13.1.
	private List<String> isReplacedBy; 		// 13.2.
	private List<String> replaces;			// 13.3.
	private List<String> isPartOf; 			// 13.4.
	private List<String> hasPart; 			// 13.5.
	private List<String> isDesignedWith;	// 13.6.
	private List<String> shouldAvoid; 		// 13.7.
	private List<String> complementedBy; 	// 13.8.
	private String about;					// 13.9.
	private List<String> coverage; 			// 14.
	private List<String> rights;			// 15.
	private String eventDate; 			// 16.1.
	private List<String> author; 		// 16.2.
	private String reason; 				// 16.3.
	private String changes; 			// 16.4.
	private List<String> functionalRequirements;		// 17.1. 
	private List<String> nonFunctionalRequirements; 	// 17.2.
	private String dependencesAndContributions;			// 17.3.
	private String dependencyGraph;						// 17.3.1.
	private String contributionGraph;					// 17.3.2
	private List<String> conflictAndResolution;			// 17.4.
	private String prioritiesDiagram;					// 17.5.
	private List<String> participants;					// 17.6.
	private String useCaseDiagram;						// 18.1.1.
	private List<String> collaborationSequenceDiagrams;	// 18.1.2.
	private List<String> activityStateDiagrams;			// 18.1.3.
	private String classDiagram;						// 18.2.1.
	private List<String> classDescriptions;				// 18.2.2.
	private List<String> relationshipDescriptions;		// 18.2.3.
	private List<String> solutionVariants;				// 18.3.
	private List<String> resultingContext;		// 19.
	private List<String> designGuidelines;		// 20.
	private List<String> positive;	// 21.1.
	private List<String> negative;	// 21.2.

	
	/**
	 * An unambiguous reference to the resource within a given context.
	 * 
	 * @return 1. Identifier
	 */
	public String getIdentifier() {
		return identifier;
	}
	/**
	 * @param identifier the identifier to be set
	 */
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	/**
	 * Formal name by which the analysis pattern is 
	 * widely known.
	 * 
	 * @return 2. Title
	 */	
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Any form of the title used as a substitute or alternative 
	 * to the formal title of the resource.
	 * 
	 * @return 2.1. Alternative Title
	 */	
	public List<String> getAlternativeTitle() {
		return alternativeTitle;
	}
	/**
	 * @param alternativeTitle the alternativeTitle to be set
	 */
	public void setAlternativeTitle(List<String> alternativeTitle) {
		this.alternativeTitle = alternativeTitle;
	}

	/**
	 * Name of the creator of the analysis pattern.
	 * 
	 * @return 3. Creator
	 */		
	public List<String> getCreator() {
		return creator;
	}	
	/**
	 * @param creator the creator to be set
	 */
	public void setCreator(List<String> creator) {
		this.creator = creator;
	}
	
	/**
	 * Is a keyword related to the scope in which the analysis pattern 
	 * applies, thus providing a brief contextualization.
	 * 
	 * @return 4. Subject
	 */	
	public List<String> getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to be set
	 */
	public void setSubject(List<String> subject) {
		this.subject = subject;
	}
	
	/**
	 * This element refinement was defined for this DCAP.
	 * 
	 * @return 5.1. Motivation
	 */	
	public String getProblem() {
		return problem;
	}
    /**
     * @param problem the problem to be set
     */
	public void setProblem(String problem) {
		this.problem = problem;
	}
	
	/**
	 * Specific reasons that justify the existence of the documented 
	 * analysis pattern. This refinement for element documents 
	 * peculiarities of the problem should be solved by the analysis 
	 * pattern, regardless of application domain.
	 * 
	 * @return 5.2. Motivation
	 */		
	public List<String> getMotivation() {
		return motivation;
	}	
	/**
	 * @param motivation the motivation to be set
	 */
	public void setMotivation(List<String> motivation) {
		this.motivation = motivation;
	}
	
	/**
	 * Examples of applications where the described analysis pattern 
	 * can be applied. This element refinement  is useful to facilitate 
	 * the abstraction, since it seeks to illustrate the use of 
	 * analysis pattern in practice.
	 * 
	 * @return 5.2.1. Example
	 */
	public List<String> getExample() {
		return example;
	}
	/**
	 * @param example the examples to be set
	 */
	public void setExample(List<String> example) {
		this.example = example;
	}
	
	/**
	 * Known uses of the described analysis pattern in real systems 
	 * and applications. This element refinement is intended to 
	 * share experiences of using of the analysis pattern, since 
	 * after its use, the responsible analyst can describe briefly 
	 * the application or system that used the pattern and how it 
	 * was useful, contributing so to future users.
	 * 
	 * @return 5.2.2. Known Uses
	 */
	public List<String> getKnownUses() {
		return knownUses;
	}
	/**
	 * @param knownUses the knownUses to be set
	 */
	public void setKnownUses(List<String> knownUses) {
		this.knownUses = knownUses;
	}
	
	/**
	 * Description of contextual coverage of the documented 
	 * analysis pattern. The domains where the problem solved 
	 * by analysis patterns is recurrent, and the possible 
	 * causes of recurrence are described by this element refinement. 
	 * This description should be general, enabling new domains of 
	 * recurrence of the problem are identified.
	 * 
	 * @return 5.3. Context
	 */
	public String getContext() {
		return context;
	}	
	/**
	 * @param context the context to be set
	 */
	public void setContext(String context) {
		this.context = context;
	}
	
	/**
	 * Names of those responsible for providing the 
	 * analysis pattern for the public. Publishers can
	 * be people or companies.
	 * 
	 * @return 6. Publisher
	 */
	public List<String> getPublisher() {
		return publisher;
	}	
	/**
	 * @param publisher the publisher to be set
	 */
	public void setPublisher(List<String> publisher) {
		this.publisher = publisher;
	}
	
	/**
	 * Names of those responsible for changes made in the 
	 * original version of the  analysis pattern. 
	 * Contributors may be people or companies.
	 * 
	 * @return 7. Contributor
	 */
	public List<String> getContributor() {
		return contributor;
	}
    /**
     * @param contributor the contributor to be set
     */
	public void setContributor(List<String> contributor) {
		this.contributor = contributor;
	}
	
	/**
	 * Creation date of the version of the analysis pattern documented.
	 * 
	 * @return 8.1. Created
	 */
	public String getCreated() {
		return created;
	}	
	/**
	 * @param created the created date to be set
	 */
	public void setCreated(String created) {
		this.created = created;
	}
	
	/**
	 * Date of the last modification of the analysis pattern documented.
	 * 
	 * @return 8.2. Modified
	 */
	public String getModified() {
		return modified;
	}
	/**
	 * @param modified the modified date to be set
	 */
	public void setModified(String modified) {
		this.modified = modified;
	}
	
	/**
	 * The nature of resource documented. Because it is an 
	 * element belonging to a profile of analysis pattern 
	 * documentation, it should always inform only the nature 
	 * "Analysis Pattern" in the appropriate language used 
	 * in the documentation.
	 * 
	 * @return 9. Type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * The notation used to create the diagrams during the 
	 * documentation of the analysis pattern requirements.
	 * 
	 * @return 9.1. Notation
	 */
	public String getNotation() {
		return notation;
	}	
	/**
	 * @param notation the notation to set
	 */
	public void setNotation(String notation) {
		this.notation = notation;
	}
	
	/**
	 * Digital format of the diagrams used in the documentation 
	 * of  the  analysis pattern requirements.
	 * 
	 * @return 10. Format
	 */
	public List<String> getFormat() {
		return format;
	}	
	/**
	 * @param format A format to set
	 */
	public void setFormat(String format) {
		this.format.add(format);
	}
	/**
	 * @param format A format list to set
	 */
	public void setFormat(List<String> format) {
		this.format = format;
	}
	
	/**
	 * A related resource from which the described resource is derived.
	 * 
	 * @return 11. Source
	 */
	public String getSource() {
		return source;
	}	
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Language used for documenting the analysis pattern.
	 * The value of the language, String size=3 (i.e. eng, deu).
	 * 
	 * @return 12. Language Value
	 */
	public String getLanguageValue() {
		return languageValue;
	}	
	/**
	 * @param language the language value to set
	 */
	public void setLanguageValue(String languageValue) {
		this.languageValue = languageValue;
	}
	
	/**
	 * Language used for documenting the analysis pattern.
	 * The label of the language (i.e. English, German).
	 * 
	 * @return 12. Language Label
	 */
	public String getLanguageLabel() {
		return languageLabel;
	}	
	/**
	 * @param languageLabel the language label to set
	 */
	public void setLanguageLabel(String languageLabel) {
		this.languageLabel = languageLabel;
	}
	
	/**
	 * A related resource of which the described resource 
	 * is a version, edition, or adaptation.
	 * 
	 * @return 13.1. Is Version Of
	 */
	public String getIsVersionOf() {
		return isVersionOf;
	}
	/**
	 * @param isVersionOf the isVersionOf to set
	 */
	public void setIsVersionOf(String isVersionOf) {
		this.isVersionOf = isVersionOf;
	}
	
	/**
	 * A related resource that supplants, displaces, 
	 * or supersedes the described resource.
	 * 
	 * @return 13.2. Is Replaced By
	 */
	public List<String> getIsReplacedBy() {
		return isReplacedBy;
	}
	/**
	 * @param isReplacedBy the isReplacedBy to set
	 */
	public void setIsReplacedBy(List<String> isReplacedBy) {
		this.isReplacedBy = isReplacedBy;
	}

	/**
	 * A related resource that is supplanted, displaced, 
	 * or superseded by the described resource.
	 * 
	 * @return 13.3. Replaces
	 */
	public List<String> getReplaces() {
		return replaces;
	}
	/**
	 * @param replaces the replaces to set
	 */
	public void setReplaces(List<String> replaces) {
		this.replaces = replaces;
	}

	/**
	 * A related resource in which the described resource 
	 * is physically or logically included.
	 * 
	 * @return 13.4. Is Part Of
	 */
	public List<String> getIsPartOf() {
		return isPartOf;
	}
	/**
	 * @param isPartOf the isPartOf to set
	 */
	public void setIsPartOf(List<String> isPartOf) {
		this.isPartOf = isPartOf;
	}
	
	/**
	 * References  for analysis patterns  which are contained as part 
	 * of  the composition of  the analysis pattern documented.
	 * 
	 * @return 13.5. Has Part
	 */
	public List<String> getHasPart() {
		return hasPart;
	}
	/**
	 * @param hasPart the hasPart to set
	 */
	public void setHasPart(List<String> hasPart) {
		this.hasPart = hasPart;
	}

	/**
	 * Known design patterns that can be used during 
	 * the implementation of the analysis pattern documented.
	 * 
	 * @return 13.6. Is Designed With
	 */
	public List<String> getIsDesignedWith() {
		return isDesignedWith;
	}
	/**
	 * @param isDesignedWith the isDesignedWith to set
	 */
	public void setIsDesignedWith(List<String> isDesignedWith) {
		this.isDesignedWith = isDesignedWith;
	}
	
	/**
	 * Anti-patterns important to avoid common errors identified 
	 * during the application of the analysis pattern documented.
	 * 
	 * @return 13.7. Should Avoid
	 */
	public List<String> getShouldAvoid() {
		return shouldAvoid;
	}
	/**
	 * @param shouldAvoid the shouldAvoid to set
	 */
	public void setShouldAvoid(List<String> shouldAvoid) {
		this.shouldAvoid = shouldAvoid;
	}

	/**
	 * Known analysis patterns that can be used to complement 
	 * the analysis pattern documented.
	 * 
	 * @return 13.8. Complemented By
	 */
	public List<String> getComplementedBy() {
		return complementedBy;
	}
	/**
	 * @param complementedBy the complementedBy to set
	 */
	public void setComplementedBy(List<String> complementedBy) {
		this.complementedBy = complementedBy;
	}
	
	/**
	 * Description of details of the relationships established 
	 * between the  analysis pattern documented and other patterns.
	 * 
	 * @return 13.9. About
	 */
	public String getAbout() {
		return about;
	}
	/**
	 * @param about the about to set
	 */
	public void setAbout(String about) {
		this.about = about;
	}
	
	/**
	 * The spatial or temporal topic of the resource, 
	 * the spatial applicability of the resource, or the 
	 * jurisdiction under which the resource is relevant.
	 * 
	 * @return 14. Coverage
	 */
	public List<String> getCoverage() {
		return coverage;
	}
	/**
	 * @param coverage the coverage to set
	 */
	public void setCoverage(List<String> coverage) {
		this.coverage = coverage;
	}
	
	
	/**
	 * Information about rights held in and over the resource.
	 * 
	 * @return 15. Rights
	 */
	public List<String> getRights() {
		return rights;
	}
	/**
	 * @param rights the rights to set
	 */
	public void setRights(List<String> rights) {
		this.rights = rights;
	}
	
	/**
	 * Date of the historic event occurrence.
	 * 
	 * @return 16.1. Event Date
	 */
	public String getEventDate() {
		return eventDate;
	}
	/**
	 * @param eventDate the eventDate to set
	 */
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	
	/**
	 * Names of those responsible for the occurrence 
	 * of the historic event.
	 * 
	 * @return 16.2. Author
	 */
	public List<String> getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(List<String> author) {
		this.author = author;
	}
	
	/**
	 * Reason for the occurrence of the historical event.
	 * 
	 * @return 16.3. Reason
	 */
	public String getReason() {
		return reason;
	}
	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	/**
	 * Changes made in the analysis pattern during the event of changes.
	 * 
	 * @return 16.4. Changes
	 */
	public String getChanges() {
		return changes;
	}
	/**
	 * @param changes the changes to set
	 */
	public void setChanges(String changes) {
		this.changes = changes;
	}
	
	/**
	 * Responsibilities and restrictions for meet each 
	 * functional requirement of the analysis pattern.
	 * 
	 * @return 17.1. Functional Requirements
	 */
	public List<String> getFunctionalRequirements() {
		return functionalRequirements;
	}
	/**
	 * @param functionalRequirements the functionalRequirements to set
	 */
	public void setFunctionalRequirements(List<String> functionalRequirements) {
		this.functionalRequirements = functionalRequirements;
	}
	
	/**
	 * Responsibilities and restrictions for meet each 
	 * non-functional requirement of the analysis pattern.
	 * 
	 * @return 17.2. Non-Functional Requirements
	 */
	public List<String> getNonFunctionalRequirements() {
		return nonFunctionalRequirements;
	}
	/**
	 * @param nonFunctionalRequirements the nonFunctionalRequirements to set
	 */
	public void setNonFunctionalRequirements(List<String> nonFunctionalRequirements) {
		this.nonFunctionalRequirements = nonFunctionalRequirements;
	}
	
	/**
	 * Brief explanation about the identified relations of dependency 
	 * and contributions among the requirements of the analysis 
	 * pattern documented.
	 * 
	 * @return 17.3. Dependencies and Contributions
	 */
	public String getDependencesAndContributions() {
		return dependencesAndContributions;
	}
	/**
	 * @param dependencesAndContributions the dependencesAndContributions to set
	 */
	public void setDependencesAndContributions(String dependencesAndContributions) {
		this.dependencesAndContributions = dependencesAndContributions;
	}
	
	/**
	 * Graph Diagram used to  represent the identified relations 
	 * of dependency among the requirements of the analysis 
	 * pattern documented.
	 * 
	 * @return 17.3.1. Dependency Graph
	 */
	public String getDependencyGraph() {
		return dependencyGraph;
	}
	/**
	 * @param dependencyGraph the dependencyGraph to set
	 */
	public void setDependencyGraph(String dependencyGraph) {
		this.dependencyGraph = dependencyGraph;
	}
	
	/**
	 * Graph diagram used to represent  the identified relations 
	 * of contribution among the requirements of the analysis 
	 * pattern documented.
	 * 
	 * @return 17.3.2. Contribution Graph
	 */
	public String getContributionGraph() {
		return contributionGraph;
	}
	/**
	 * @param contributionGraph the contributionGraph to set
	 */
	public void setContributionGraph(String contributionGraph) {
		this.contributionGraph = contributionGraph;
	}
	
	/**
	 * Brief explanation about the identified conflicts among 
	 * the requirements of the analysis pattern and possible
	 * manners to solve them.
	 * 
	 * @return 17.4. Conflict identification and Guidance to Resolution
	 */
	public List<String> getConflictAndResolution() {
		return conflictAndResolution;
	}
	/**
	 * @param conflictAndResolution the conflictAndResolution to set
	 */
	public void setConflictAndResolution(List<String> conflictAndResolution) {
		this.conflictAndResolution = conflictAndResolution;
	}
	
	/**
	 * The priorities diagram represents the order which 
	 * the requirements of the analysis pattern should be 
	 * satisfied respecting their dependencies.
	 * 
	 * @return 17.5. Priorities Diagram
	 */
	public String getPrioritiesDiagram() {
		return prioritiesDiagram;
	}
	/**
	 * @param prioritiesDiagram the prioritiesDiagram to set
	 */
	public void setPrioritiesDiagram(String prioritiesDiagram) {
		this.prioritiesDiagram = prioritiesDiagram;
	}
	
	/**
	 * Entities that interact with the requirements of 
	 * the analysis pattern.
	 * 
	 * @return 17.6. Participants
	 */
	public List<String> getParticipants() {
		return participants;
	}
	/**
	 * @param participants the participants to set
	 */
	public void setParticipants(List<String> participants) {
		this.participants = participants;
	}
	
	/**
	 * Diagram used to represents a system that meets the 
	 * requirements of the analysis pattern together with 
	 * the entities which interact with them.
	 * 
	 * @return 18.1.1. Use Case Diagram
	 */
	public String getUseCaseDiagram() {
		return useCaseDiagram;
	}
	/**
	 * @param useCaseDiagram the useCaseDiagram to set
	 */
	public void setUseCaseDiagram(String useCaseDiagram) {
		this.useCaseDiagram = useCaseDiagram;
	}
	
	/**
	 * Collaboration and Sequence diagrams are used to create 
	 * scenarios that represent the execution of each use case 
	 * of the analysis pattern.
	 * 
	 * @return 18.1.2. Collaboration and Sequence Diagrams
	 */
	public List<String> getCollaborationSequenceDiagrams() {
		return collaborationSequenceDiagrams;
	}
	/**
	 * @param colaborationSequenceDiagrams the colaborationSequenceDiagrams to set
	 */
	public void setCollaborationSequenceDiagrams(
			List<String> collaborationSequenceDiagrams) {
		this.collaborationSequenceDiagrams = collaborationSequenceDiagrams;
	}
	
	/**
	 * Activity and State diagrams are used to represent the general 
	 * behavior of the  system specified by the analysis pattern.
	 * 
	 * @return 18.1.3. Activity and State Diagrams
	 */
	public List<String> getActivityStateDiagrams() {
		return activityStateDiagrams;
	}
	/**
	 * @param activityStateDiagrams the activityStateDiagrams to set
	 */
	public void setActivityStateDiagrams(List<String> activityStateDiagrams) {
		this.activityStateDiagrams = activityStateDiagrams;
	}
	
	/**
	 * Class diagram is the main part of the problem solution 
	 * treated by the analysis pattern. This diagram should be 
	 * created according with the objects identified by the 
	 * behavioral model of the analysis pattern.
	 * 
	 * @return 18.2.1. Class Diagram
	 */
	public String getClassDiagram() {
		return classDiagram;
	}
	/**
	 * @param classDiagram the classDiagram to set
	 */
	public void setClassDiagram(String classDiagram) {
		this.classDiagram = classDiagram;
	}
	
	/**
	 * Brief description of each class of the class diagram.
	 * 
	 * @return 18.2.2. Class Descriptions
	 */
	public List<String> getClassDescriptions() {
		return classDescriptions;
	}
	/**
	 * @param classDescriptions the classDescriptions to set
	 */
	public void setClassDescriptions(List<String> classDescriptions) {
		this.classDescriptions = classDescriptions;
	}
	
	/**
	 * Relationship Descriptions.
	 * 
	 * @return 18.2.3. Relationship Descriptions
	 */
	public List<String> getRelationshipDescriptions() {
		return relationshipDescriptions;
	}
	/**
	 * @param relationshipDescriptions the relationshipDescriptions to set
	 */
	public void setRelationshipDescriptions(List<String> relationshipDescriptions) {
		this.relationshipDescriptions = relationshipDescriptions;
	}
	
	/**
	 * This element refinement presents alternative models of behavior 
	 * and structure to solve the problem treated by the analysis pattern.
	 * 
	 * @return 18.3. Solution Variants
	 */
	public List<String> getSolutionVariants() {
		return solutionVariants;
	}
	/**
	 * @param solutionVariants the solutionVariants to set
	 */
	public void setSolutionVariants(List<String> solutionVariants) {
		this.solutionVariants = solutionVariants;
	}
	
	/**
	 * This element describes adaptations realized in the analysis pattern 
	 * when it is applied in specific systems.
	 * 
	 * @return 19. Resulting Context
	 */
	public List<String> getResultingContext() {
		return resultingContext;
	}
	/**
	 * @param resultingContext the resultingContext to set
	 */
	public void setResultingContext(List<String> resultingContext) {
		this.resultingContext = resultingContext;
	}
	
	/**
	 * This element presents general tips for the implementation
	 * of the documented analysis pattern.
	 * 
	 * @return 20. Design Guidelines
	 */
	public List<String> getDesignGuidelines() {
		return designGuidelines;
	}
	/**
	 * @param designGuidelines the designGuidelines to set
	 */
	public void setDesignGuidelines(List<String> designGuidelines) {
		this.designGuidelines = designGuidelines;
	}
	
	/**
	 * Positive consequences of the use of the analysis pattern documented.
	 * 
	 * @return 21.1. Positive
	 */
	public List<String> getPositive() {
		return positive;
	}
	/**
	 * @param positive the positive consequences to set
	 */
	public void setPositive(List<String> positive) {
		this.positive = positive;
	}
	
	/**
	 * Negative consequences of the use of the analysis pattern documented.
	 * 
	 * @return 21.2. Negative
	 */
	public List<String> getNegative() {
		return negative;
	}
	/**
	 * @param negative the negative consequences to set
	 */
	public void setNegative(List<String> negative) {
		this.negative = negative;
	}
	
}