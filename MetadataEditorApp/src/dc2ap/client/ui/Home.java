package dc2ap.client.ui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.Patternportal;


/**
 * Home page of the Web site.
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
public class Home extends Composite {

	public Home() {
		
		AbsolutePanel absolutePanel = new AbsolutePanel();
		absolutePanel.setStyleName("whiteBackground");
		absolutePanel.setSize("1240px", "550px");
		
		HTML htmlNewHtml = new HTML("<p align=\"justify\">The DC2AP is a Dublin Core Application Profile to analysis patterns description. " +
				"This Dublin Core Application Profile was developed by the Information Systems Group of Federal University " +
				"of Vi\u00E7osa based on the template proposed by Pantoquilho et al. (2003) and Raminhos et al. (2006) to " +
				"specify analysis patterns. The aims of the DC2AP are to improve the retrieving and reuse of analysis " +
				"patterns through a more precise way to be treated by a computer, thus offering important information missing before.</p>", true);
		htmlNewHtml.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		htmlNewHtml.setStyleName("textBoldLargeBlue");
		absolutePanel.add(htmlNewHtml, 98, 21);
		htmlNewHtml.setSize("1050px", "109px");
		
		HTML htmlNewHtml_1 = new HTML("<p align=\"justify\">The DC2AP Metadata Editor is a Web based system developed at the Federal University of Vi\u00E7osa " +
				"to allow users of analysis pattern to create, edit and save metadata following the rules described in the DC2AP and also export metadata to RDF file format; " +
				"this Web system will also allow users to control versions of their metadata.</p>", true);
		htmlNewHtml_1.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		htmlNewHtml_1.setStyleName("textBoldLargeBlue");
		absolutePanel.add(htmlNewHtml_1, 98, 136);
		htmlNewHtml_1.setSize("1050px", "84px");
		
		Button startButton = new Button("DC2AP Metadata Editor");
		startButton.setTitle("Start DC2AP Metadata Editor");
		startButton.setStyleName("button");
		startButton.setSize("230px", "63px");
		
		absolutePanel.add(startButton, 499, 323);
		
		// create event of the start button 
		startButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Patternportal.openEditor();				
			}
		});
		
		this.ensureDebugId("home");
		initWidget(absolutePanel);
		
		@SuppressWarnings("deprecation")
		DisclosurePanel disclosurePanel = new DisclosurePanel("Contacts", true);
		absolutePanel.add(disclosurePanel, 98, 422);
		disclosurePanel.setSize("772px", "98px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		disclosurePanel.setContent(horizontalPanel);
		horizontalPanel.setSize("100%", "100%");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		horizontalPanel.add(verticalPanel);
		verticalPanel.setSize("337px", "64px");
		
		Label lblJugurta = new Label("Jugurta Lisboa Filho (Coordinator)");
		lblJugurta.setStyleName("textBoldLarge");
		verticalPanel.add(lblJugurta);
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel.add(horizontalPanel_1);
		
		Label mail1 = new Label("Email: ");
		horizontalPanel_1.add(mail1);
		mail1.setWidth("50px");
		
		Anchor hprlnkJugurta = new Anchor("jugurta@ufv.br", "mailto:jugurta@ufv.br");
		horizontalPanel_1.add(hprlnkJugurta);
		hprlnkJugurta.setWidth("115px");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_1);
		verticalPanel_1.setSize("337px", "64px");
		
		Label lblLucas = new Label("Lucas Francisco Da Matta Vegi");
		lblLucas.setStyleName("textBoldLarge");
		verticalPanel_1.add(lblLucas);
		
		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel_2);
		
		Label mail2 = new Label("Email: ");
		horizontalPanel_2.add(mail2);
		mail2.setWidth("50px");
		
		Anchor hprlnkLucas = new Anchor("lucasvegi@gmail.com", "mailto:lucasvegi@gmail.com");
		horizontalPanel_2.add(hprlnkLucas);
		hprlnkLucas.setWidth("140px");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		verticalPanel_2.setSize("337px", "64px");
		
		Label lblDouglasAlvesPeixoto = new Label("Douglas Alves Peixoto");
		lblDouglasAlvesPeixoto.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblDouglasAlvesPeixoto);
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		verticalPanel_2.add(horizontalPanel_3);
		
		Label mail3 = new Label("Email: ");
		horizontalPanel_3.add(mail3);
		mail3.setWidth("50px");
		
		Anchor hprlnkDouglas = new Anchor("douglasalves.ufv@gmail.com","mailto:douglasalves.ufv@gmail.com");
		horizontalPanel_3.add(hprlnkDouglas);
		hprlnkDouglas.setWidth("174px");
		
		Label lblMor = new Label("An detailed technical description about DC2AP can be obtained at:");
		lblMor.setStyleName("textLarge");
		absolutePanel.add(lblMor, 98, 254);
		
		Anchor hprlnkDcap = new Anchor("DC2AP: Technical Description", "http://www.dpi.ufv.br/projetos/apri/?page_id=316");
		absolutePanel.add(hprlnkDcap, 586, 258);
		
	}
}
