package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

/**
 * Implement the UI panel with the Identifier and Title information.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public final class IdentifierPanel extends GenericPanel{
	private TextBox identifierBox = new TextBox();
	private TextBox titleBox = new TextBox();
	private MultipleTextField altTitleBoxList = new MultipleTextField(730);

	/*/ instance to this class
	private static final IdentifierPanel INSTANCE = 
		new IdentifierPanel();*/
	
	public IdentifierPanel() {
		
		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		mainPanel.setSpacing(10);
		mainPanel.setStyleName("cw-DialogBox");
		mainPanel.setSize("980px", "540px");
		
		DecoratedStackPanel identifierStackPanel = new DecoratedStackPanel();
		mainPanel.add(identifierStackPanel);
		identifierStackPanel.setSize("965px", "120px");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		identifierStackPanel.add(verticalPanel_1, "1. Identifier", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblWhatIsThe = new Label("An unambiguous reference to the resource within a given context.");
		lblWhatIsThe.setStyleName("definitionLabel");
		verticalPanel_1.add(lblWhatIsThe);
		lblWhatIsThe.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setSize("100%", "62px");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_4);
		
		Label lblIdentifier = new Label("Identifier:");
		verticalPanel_4.add(lblIdentifier);
		lblIdentifier.setStyleName("textBoldLarge");
		lblIdentifier.setWidth("145px");
		
		// create the identifier label menu
		ElementLabel labelMenuId = new ElementLabel("[M]", "[S]", "[UNS]", "63px");
		verticalPanel_4.add(labelMenuId);
		
		// add the identifier box to the panel
		horizontalPanel.add(identifierBox);
		identifierBox.setSize("740px", "40px");
		identifierBox.setStyleName("textBox");

	    // Create the image icon that opens the dialog
		ImageInfo infoId = new ImageInfo("Identifier Information", 
	    			"IDENTIFIER", "570px");

		// add the image icon to the panel
		horizontalPanel.add(infoId);

		final DecoratedStackPanel titleStackPanel = new DecoratedStackPanel();
		mainPanel.add(titleStackPanel);
		titleStackPanel.setSize("965px", "380px");
		
		final VerticalPanel verticalPanel_2 = new VerticalPanel();
		titleStackPanel.add(verticalPanel_2, "2. Title", false);
		verticalPanel_2.setSize("100%", "313px");
		
		Label lblWhatIsThe_1 = new Label("A name given to the resource.");
		lblWhatIsThe_1.setStyleName("definitionLabel");
		verticalPanel_2.add(lblWhatIsThe_1);
		verticalPanel_2.setCellVerticalAlignment(lblWhatIsThe_1, HasVerticalAlignment.ALIGN_BOTTOM);
		lblWhatIsThe_1.setSize("100%", "30px");
		
		final HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_2.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "70px");
		
		final VerticalPanel verticalPanel_5 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_5);
		
		Label lblTitle = new Label("Title:");
		verticalPanel_5.add(lblTitle);
		lblTitle.setStyleName("textBoldLarge");
		lblTitle.setWidth("145px");

		// create the title label menu
		ElementLabel labelMenuTitle = new ElementLabel("[M]", "[S]", "[St]", "52px");
		verticalPanel_5.add(labelMenuTitle);
		titleBox.setStyleName("textBox");

		horizontalPanel_1.add(titleBox);
		titleBox.setSize("740px", "40px");

	    // Create the image icon that opens the dialog
		ImageInfo infoTitle = new ImageInfo("Title Information", 
						"TITLE", "550px");
		
		// add the image icon to the panel
		horizontalPanel_1.add(infoTitle);
		
		final DecoratedStackPanel altTitleStackPanel = new DecoratedStackPanel();
		verticalPanel_2.add(altTitleStackPanel);
		altTitleStackPanel.setSize("950px", "100%");
		
		final VerticalPanel verticalPanel_3 = new VerticalPanel();
		altTitleStackPanel.add(verticalPanel_3, "2.1. Alternative Title", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblDoesTehResource = new Label("Any form of the title used as a substitute or " +
				"alternative to the formal title of the resource. One per line.");
		lblDoesTehResource.setStyleName("definitionLabel");
		verticalPanel_3.add(lblDoesTehResource);
		verticalPanel_2.setCellVerticalAlignment(lblDoesTehResource, HasVerticalAlignment.ALIGN_BOTTOM);
		lblDoesTehResource.setSize("100%", "30px");
		
		final HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "100%");
		
		final VerticalPanel verticalPanel_6 = new VerticalPanel();
		horizontalPanel_2.add(verticalPanel_6);
		
		Label lblAlternativeTitle = new Label("Alternative Titles:");
		verticalPanel_6.add(lblAlternativeTitle);
		lblAlternativeTitle.setStyleName("textBoldLarge");
		lblAlternativeTitle.setWidth("140px");

		// create the alternative title label menu
		ElementLabel labelMenuAltTitle = new ElementLabel("[O]", "[Mu]", "[St]", "60px");
		verticalPanel_6.add(labelMenuAltTitle);
		
		// add the Alternative Title field to the panel
		horizontalPanel_2.add(altTitleBoxList);

	    // Create the image icon that opens the Info dialog box
		ImageInfo infoAltTitle = new ImageInfo("Alternative Title Information", 
						"ALTERNATIVE", "590px");
		
		// add the image icon to the panel
		horizontalPanel_2.add(infoAltTitle);
		
		this.ensureDebugId("identifierPanel");
	}
	  
    /**
     * Get the TextBox with the Id information.
     * 
     * @return Identifier Box
     */
	public TextBox getIdentifierBox() {
		return identifierBox;
	}
    /**
     * Get the TextBox with the Title information.
     * 
     * @return Title Box
     */
	public TextBox getTitleBox() {
		return titleBox;
	}
    /**
     * Get the MultipleTextField with the Alternative Title information.
     * 
     * @return Alternative Title field
     */
	public MultipleTextField getAltTitleBox() {
		return altTitleBoxList;
	}

	@Override
	public void clearFields() {
		identifierBox.setText("");
		titleBox.setText("");
		altTitleBoxList.clear(); 
		
		identifierBox.removeStyleName("errorTextBox");
		titleBox.removeStyleName("errorTextBox");
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		identifierBox.setFocus(focused);
	}
	
	/**
	 * Unique access to this object.
	 * 
	 * @return An instance to this panel
	 *//*
	public static synchronized IdentifierPanel getInstance(){
        return INSTANCE;
	}*/
	
}
