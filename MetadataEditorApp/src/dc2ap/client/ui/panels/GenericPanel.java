package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.Composite;

/**
 * Generic Composite for all element Panels.
 * All element Panels must extends this class.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public abstract class GenericPanel extends Composite {
	private GenericPanel previousPanel = null;
	private GenericPanel nextPanel = null;

	/**
	 * Return the current's previous panel, according
	 * with the Tree hierarchy.
	 * 
	 * @return Current's previous Panel.
	 */
	public GenericPanel getPreviousPanel() {
		return previousPanel;
	}
	
	/**
	 * Return the current's next panel, according
	 * with the Tree hierarchy.
	 * 
	 * @return Current's previous Panel.
	 */
	public GenericPanel getNextPanel() {
		return nextPanel;
	}
	
	/**
	 * Set the current's previous panel, according
	 * with the Tree hierarchy.
	 * 
	 * @param panel Current's previous panel.
	 */
	public void setPreviousPanel(GenericPanel panel) {
		previousPanel = panel;
	}
	
	/**
	 * Set the current's next panel, according
	 * with the Tree hierarchy.
	 * 
	 * @param panel Current's next panel.
	 */
	public void setNextPanel(GenericPanel panel) {
		nextPanel = panel;
	}
	
	/**
	 * Clear all fields of this panel. If the field is
	 * a Multiple field, then all rows will be removed.
	 * Each panel implements its own clearFields method.
	 */
	public abstract void clearFields();
	
	/**
	 * Set focus on the main field of the panel.
	 * 
	 * @param focused Whether this field should take focus or release it
	 */
	public abstract void setMainFieldFocus(boolean focused);
}
