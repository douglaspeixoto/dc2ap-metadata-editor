package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

import com.google.gwt.user.client.ui.TextBox;

/**
 * Implement the UI panel with the Requirements information.
 *
 * @author Douglas A. Peixoto - 06/2012
 */
public class RequirementsPanel extends GenericPanel {
	private MultipleTextField funcReqBoxList = new MultipleTextField(710);
	private MultipleTextField nonFuncReqBoxList = new MultipleTextField(710);
	private TextBox dependAndContribBox = new TextBox();
	private TextBox dependGraphBox = new TextBox();
	private TextBox contribGraphBox = new TextBox();
	private MultipleTextField conflictAndResolBoxList = new MultipleTextField(710);
	private TextBox priorDiagramBox = new TextBox();
	private MultipleTextField participantsBoxList = new MultipleTextField(710);
	
	public RequirementsPanel() {
		
		ScrollPanel mainPanel = new ScrollPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("scrollPanel");
		mainPanel.setSize("985px", "600px");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(10);
		mainPanel.setWidget(verticalPanel);
		verticalPanel.setSize("955px", "100%");
		
		DecoratedStackPanel requirementStackPanel = new DecoratedStackPanel();
		verticalPanel.add(requirementStackPanel);
		requirementStackPanel.setSize("930px", "100%");
		
		FlexTable flexTable = new FlexTable();
		flexTable.setCellPadding(5);
		requirementStackPanel.add(flexTable, "17. Requirements", false);
		flexTable.setSize("100%", "100%");
		
		DecoratedStackPanel funcReqStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(0, 0, funcReqStackPanel);
		funcReqStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		funcReqStackPanel.add(verticalPanel_3, "17.1. Functional Requirements", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblResponsibilitiesAndRestrictions = new Label("Responsibilities and restrictions for meet each " +
				"functional requirement of the analysis pattern. One per line.");
		lblResponsibilitiesAndRestrictions.setStyleName("definitionLabel");
		verticalPanel_3.add(lblResponsibilitiesAndRestrictions);
		lblResponsibilitiesAndRestrictions.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblFuncReq = new Label("Functional Req.:");
		lblFuncReq.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblFuncReq);
		lblFuncReq.setWidth("130px");
		
		// create the Fuctional label menu
		ElementLabel labelFuncReq = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_4.add(labelFuncReq);

		// add the Functional field to the panel
		horizontalPanel_1.add(funcReqBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo funcReqInfo = new ImageInfo("Functional Requirements Information", 
						"FUNCTIONAL", "570px");

		// add the image icon to the panel
		horizontalPanel_1.add(funcReqInfo);
		
		DecoratedStackPanel nonFuncReqStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(1, 0, nonFuncReqStackPanel);
		nonFuncReqStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		nonFuncReqStackPanel.add(verticalPanel_1, "17.2. Non-Functional Requirements", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblResponsibilitiesAndRestrictions_1 = new Label("Responsibilities and restrictions for meet each " +
				"non-functional requirement of the analysis pattern. One per line.");
		lblResponsibilitiesAndRestrictions_1.setStyleName("definitionLabel");
		verticalPanel_1.add(lblResponsibilitiesAndRestrictions_1);
		lblResponsibilitiesAndRestrictions_1.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblNonfuncReq = new Label("Non-Func. Req.:");
		lblNonfuncReq.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblNonfuncReq);
		lblNonfuncReq.setWidth("130px");

		// create the Non-Functional label menu
		ElementLabel labelNonFReq = new ElementLabel("[O]", "[Mu]", "[St]", "60px");
		verticalPanel_2.add(labelNonFReq);

		// add the Non-Functional field to the panel
		horizontalPanel.add(nonFuncReqBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo nonFReqInfo = new ImageInfo("Non-Functional Information", 
						"NON_FUNCTIONAL", "580px");

		// add the image icon to the panel
		horizontalPanel.add(nonFReqInfo);
		
		DecoratedStackPanel depAndContribStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(2, 0, depAndContribStackPanel);
		depAndContribStackPanel.setSize("920px", "100%");
		
		FlexTable flexTable_1 = new FlexTable();
		flexTable_1.setCellPadding(5);
		depAndContribStackPanel.add(flexTable_1, "17.3. Dependencies and Contributions", false);
		flexTable_1.setSize("100%", "100%");
		
		Label lblBriefExplanationAbout = new Label("Brief explanation about the identified relations of " +
				"dependency and contributions among the requirements of the documented analysis pattern.");
		lblBriefExplanationAbout.setStyleName("definitionLabel");
		flexTable_1.setWidget(0, 0, lblBriefExplanationAbout);
		lblBriefExplanationAbout.setSize("100%", "18px");
		
		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		flexTable_1.setWidget(1, 0, horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_5 = new VerticalPanel();
		horizontalPanel_2.add(verticalPanel_5);
		
		Label lblDepAndContrib = new Label("Dep. and Contr.:");
		lblDepAndContrib.setStyleName("textBoldLarge");
		verticalPanel_5.add(lblDepAndContrib);
		lblDepAndContrib.setWidth("130px");
		
		// create the Dependencies and OCntributions label menu
		ElementLabel labelDepAndContr = new ElementLabel("[M]", "[S]", "[St]", "53px");
		verticalPanel_5.add(labelDepAndContr);

		// add the Dependencies and Contributions field to the panel
		dependAndContribBox.setStyleName("textBox");
		dependAndContribBox.setSize("700px", "40px");
		horizontalPanel_2.add(dependAndContribBox);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo depAndContribInfo = new ImageInfo("Dependencies and Contributions Information", 
						"DEPENDENCIES", "570px");

		// add the image icon to the panel
		horizontalPanel_2.add(depAndContribInfo);
		
		DecoratedStackPanel depGraphStackPanel = new DecoratedStackPanel();
		flexTable_1.setWidget(2, 0, depGraphStackPanel);
		depGraphStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_10 = new VerticalPanel();
		depGraphStackPanel.add(verticalPanel_10, "17.3.1. Dependency Graph", false);
		verticalPanel_10.setSize("100%", "100%");
		
		Label lblGraphDiagramUsed = new Label("Graph Diagram used to represent the identified relations of " +
				"dependency among the requirements of the documented analysis pattern.");
		lblGraphDiagramUsed.setStyleName("definitionLabel");
		verticalPanel_10.add(lblGraphDiagramUsed);
		lblGraphDiagramUsed.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_5 = new HorizontalPanel();
		verticalPanel_10.add(horizontalPanel_5);
		horizontalPanel_5.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_11 = new VerticalPanel();
		horizontalPanel_5.add(verticalPanel_11);
		
		Label lblDepGraph = new Label("Depend. Graph:");
		lblDepGraph.setStyleName("textBoldLarge");
		verticalPanel_11.add(lblDepGraph);
		lblDepGraph.setWidth("130px");

		// create the Dependency Graph label menu
		ElementLabel labelDependGraph = new ElementLabel("[M]", "[S]", "[U]", "50px");
		verticalPanel_11.add(labelDependGraph);

		// add the Dependency Graph field to the panel
		dependGraphBox.setStyleName("textBox");
		dependGraphBox.setSize("690px", "40px");
		horizontalPanel_5.add(dependGraphBox);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo dependGraphInfo = new ImageInfo("Dependency Graph Information", 
						"DEP_GRAPH", "570px");

		// add the image icon to the panel
		horizontalPanel_5.add(dependGraphInfo);
		
		DecoratedStackPanel contribGraphStackPanel = new DecoratedStackPanel();
		flexTable_1.setWidget(3, 0, contribGraphStackPanel);
		contribGraphStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_6 = new VerticalPanel();
		contribGraphStackPanel.add(verticalPanel_6, "17.3.2. Contribution Graph", false);
		verticalPanel_6.setSize("100%", "100%");
		
		Label lblGraphDiagramUsed_1 = new Label("Graph diagram used to represent the identified relations of " +
				"contribution among the requirements of the documented analysis pattern.");
		lblGraphDiagramUsed_1.setStyleName("definitionLabel");
		verticalPanel_6.add(lblGraphDiagramUsed_1);
		lblGraphDiagramUsed_1.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		verticalPanel_6.add(horizontalPanel_3);
		horizontalPanel_3.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_7 = new VerticalPanel();
		horizontalPanel_3.add(verticalPanel_7);
		
		Label lblContribGraph = new Label("Contrib. Graph:");
		lblContribGraph.setStyleName("textBoldLarge");
		verticalPanel_7.add(lblContribGraph);
		lblContribGraph.setWidth("130px");
		
		// create the Contribution Graph label menu
		ElementLabel labelContribGraph = new ElementLabel("[Cd]", "[S]", "[U]", "53px");
		verticalPanel_7.add(labelContribGraph);

		// add the Contribution Graph field to the panel
		contribGraphBox.setStyleName("textBox");
		contribGraphBox.setSize("690px", "40px");
		horizontalPanel_3.add(contribGraphBox);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo contribGraphInfo = new ImageInfo("Contribution Graph Information", 
						"CONTRI_GRAPH", "600px");

		// add the image icon to the panel
		horizontalPanel_3.add(contribGraphInfo);
		
		DecoratedStackPanel conflictStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(3, 0, conflictStackPanel);
		conflictStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_8 = new VerticalPanel();
		conflictStackPanel.add(verticalPanel_8, "17.4. Conflict Identification and Guidance to Resolution", false);
		verticalPanel_8.setSize("100%", "100%");
		
		Label lblBriefExplanationAbout_1 = new Label("Brief explanation about the identified conflicts among the " +
				"requirements of an analysis pattern and possible manners to solve them. One per line.");
		lblBriefExplanationAbout_1.setStyleName("definitionLabel");
		verticalPanel_8.add(lblBriefExplanationAbout_1);
		lblBriefExplanationAbout_1.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
		verticalPanel_8.add(horizontalPanel_4);
		horizontalPanel_4.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_9 = new VerticalPanel();
		horizontalPanel_4.add(verticalPanel_9);
		
		Label lblConfAndResol = new Label("Conf. and Resol.:");
		lblConfAndResol.setStyleName("textBoldLarge");
		verticalPanel_9.add(lblConfAndResol);
		lblConfAndResol.setWidth("130px");

		// create the Conflict and Resolution label menu
		ElementLabel labelConflict = new ElementLabel("[Cd]", "[Mu]", "[St]", "62px");
		verticalPanel_9.add(labelConflict);

		// add the Conflict and Resolution field to the panel
		horizontalPanel_4.add(conflictAndResolBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo conflictInfo = new ImageInfo("Conflict and Resolution Information", 
						"CONFLICT", "600px");

		// add the image icon to the panel
		horizontalPanel_4.add(conflictInfo);
		
		DecoratedStackPanel prioritiesStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(4, 0, prioritiesStackPanel);
		prioritiesStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_12 = new VerticalPanel();
		prioritiesStackPanel.add(verticalPanel_12, "17.5. Priorities Diagram", false);
		verticalPanel_12.setSize("100%", "100%");
		
		Label lblThePrioritiesDiagram = new Label("The priorities diagram represents the order which " +
				"requirements of an analysis pattern should be satisfied respecting their dependencies.");
		lblThePrioritiesDiagram.setStyleName("definitionLabel");
		verticalPanel_12.add(lblThePrioritiesDiagram);
		lblThePrioritiesDiagram.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_6 = new HorizontalPanel();
		verticalPanel_12.add(horizontalPanel_6);
		horizontalPanel_6.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_13 = new VerticalPanel();
		horizontalPanel_6.add(verticalPanel_13);
		
		Label lblPriorDiagr = new Label("Prior. Diagram:");
		lblPriorDiagr.setStyleName("textBoldLarge");
		verticalPanel_13.add(lblPriorDiagr);
		lblPriorDiagr.setWidth("130px");
		
		// create the Priorities Diagram label menu
		ElementLabel labelPriorities = new ElementLabel("[M]", "[S]", "[U]", "50px");
		verticalPanel_13.add(labelPriorities);

		// add the Priorities Diagram field to the panel
		priorDiagramBox.setStyleName("textBox");
		priorDiagramBox.setSize("710px", "40px");
		horizontalPanel_6.add(priorDiagramBox);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo prioritiesInfo = new ImageInfo("Priorities Diagram Information", 
						"PRIORITIES_DIAGRAM", "600px");

		// add the image icon to the panel
		horizontalPanel_6.add(prioritiesInfo);
		
		DecoratedStackPanel participantsStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(5, 0, participantsStackPanel);
		participantsStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_14 = new VerticalPanel();
		participantsStackPanel.add(verticalPanel_14, "17.6. Participants", false);
		verticalPanel_14.setSize("100%", "100%");
		
		Label lblEntitiesThatInteract = new Label("Entities that interact with analysis pattern " +
				"requirements. One per line.");
		lblEntitiesThatInteract.setStyleName("definitionLabel");
		verticalPanel_14.add(lblEntitiesThatInteract);
		lblEntitiesThatInteract.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_7 = new HorizontalPanel();
		verticalPanel_14.add(horizontalPanel_7);
		horizontalPanel_7.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_15 = new VerticalPanel();
		horizontalPanel_7.add(verticalPanel_15);
		
		Label lblParticipants = new Label("Participants:");
		lblParticipants.setStyleName("textBoldLarge");
		verticalPanel_15.add(lblParticipants);
		lblParticipants.setWidth("130px");

		// create the Particiapants label menu
		ElementLabel labelParticipants = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_15.add(labelParticipants);

		// add the Participants field to the panel
		horizontalPanel_7.add(participantsBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo participantsInfo = new ImageInfo("Participants Information", 
						"PARTICIPANTS", "580px");

		// add the image icon to the panel
		horizontalPanel_7.add(participantsInfo);
		
		this.ensureDebugId("requirementsPanel");
	}

    /**
     * Get the MultipleTextField with the Functional Requirements information.
     * 
     * @return Functional Requirements field
     */
	public MultipleTextField getFunctionalRequirementsBox() {
		return funcReqBoxList;
	}
    /**
     * Get the MultipleTextField with the Non-Functional Requirements information.
     * 
     * @return Non-Functional Requirements field
     */
	public MultipleTextField getNonFunctionalRequirementsBox() {
		return nonFuncReqBoxList;
	}
    /**
     * Get the TextBox with the Dependencies and Contributions information.
     * 
     * @return Dependencies and Contributions Box
     */
	public TextBox getDependenciesAndContributionsBox() {
		return dependAndContribBox;
	}
    /**
     * Get the TextBox with the Dependency Graph information.
     * 
     * @return Dependency Graph Box
     */
	public TextBox getDependencyGraphBox() {
		return dependGraphBox;
	}
    /**
     * Get the TextBox with the Dependency Graph information.
     * 
     * @return Dependency Graph Box
     */
	public TextBox getContributionGraphBox() {
		return contribGraphBox;
	}
    /**
     * Get the MultipleTextField with the Conflict and Resolution information.
     * 
     * @return Non-Functional Requirements field
     */
	public MultipleTextField getConflictAndResolutionBox() {
		return conflictAndResolBoxList;
	}
    /**
     * Get the TextBox with the Priorities Diagram information.
     * 
     * @return Priorities Diagram Box
     */
	public TextBox getPrioritiesDiagramBox() {
		return priorDiagramBox;
	}
    /**
     * Get the MultipleTextField with the Participants information.
     * 
     * @return Participants field
     */
	public MultipleTextField getParticipantsBox() {
		return participantsBoxList;
	}

	@Override
	public void clearFields() {
		funcReqBoxList.clear();
		nonFuncReqBoxList.clear();
		dependAndContribBox.setText("");
		dependGraphBox.setText("");
		contribGraphBox.setText("");
		conflictAndResolBoxList.clear();
		priorDiagramBox.setText("");
		participantsBoxList.clear();
		
		dependAndContribBox.removeStyleName("errorTextBox");
		dependGraphBox.removeStyleName("errorTextBox");
		contribGraphBox.removeStyleName("warningTextBox");
		priorDiagramBox.removeStyleName("errorTextBox");
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		funcReqBoxList.setFocus(focused, 0);
	}

}
