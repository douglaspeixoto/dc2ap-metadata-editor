package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

/**
 * Implement the UI panel with the Coverage information.
 *  
 * @author Douglas A. Peixoto - 06/2012
 */
public final class CoveragePanel extends GenericPanel {
	private MultipleTextField coverageBoxList = new MultipleTextField(780);
	private MultipleTextField rightsBoxList = new MultipleTextField(780);
	
	public CoveragePanel() {
		
		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("cw-DialogBox");
		mainPanel.setSpacing(10);
		mainPanel.setSize("980px", "510px");
		
		DecoratedStackPanel coverageStackPanel = new DecoratedStackPanel();
		mainPanel.add(coverageStackPanel);
		coverageStackPanel.setSize("965px", "120px");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		coverageStackPanel.add(verticalPanel_1, "14. Coverage", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblTheSpatialOr = new Label("The spatial or temporal topic of the resource, the spatial " +
				"applicability of the resource, or the jurisdiction under which the resource is relevant. One per line.");
		lblTheSpatialOr.setStyleName("definitionLabel");
		verticalPanel_1.add(lblTheSpatialOr);
		lblTheSpatialOr.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setWidth("100%");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblCoverage = new Label("Coverage:");
		lblCoverage.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblCoverage);
		lblCoverage.setWidth("100px");
		
		// create the creator label menu
		ElementLabel labelMenuCoverage = new ElementLabel("[O]", "[Mu]", "[St]", "60px");
		verticalPanel_2.add(labelMenuCoverage);
		
		// add the coverage field to the panel
		horizontalPanel.add(coverageBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo coverageInfo = new ImageInfo("Coverage Information", 
						"COVERAGE", "600px");

		// add the image icon to the panel
		horizontalPanel.add(coverageInfo);
		
		DecoratedStackPanel rightsStackPanel = new DecoratedStackPanel();
		mainPanel.add(rightsStackPanel);
		rightsStackPanel.setSize("965px", "120px");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		rightsStackPanel.add(verticalPanel_3, "15. Rights", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblInformationAboutRights = new Label("Information about rights held in and over the resource. One per line.");
		lblInformationAboutRights.setStyleName("definitionLabel");
		verticalPanel_3.add(lblInformationAboutRights);
		lblInformationAboutRights.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setWidth("100%");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblRights = new Label("Rights:");
		lblRights.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblRights);
		lblRights.setWidth("100px");

		// create the rights label menu
		ElementLabel labelMenuRights = new ElementLabel("[Cd]", "[Mu]", "[US]", "68px");
		verticalPanel_4.add(labelMenuRights);
		
		// add the rights field to the panel
		horizontalPanel_1.add(rightsBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo rightsInfo = new ImageInfo("Rights Information", 
						"RIGHTS", "560px");

		// add the image icon to the panel
		horizontalPanel_1.add(rightsInfo);
		
		this.ensureDebugId("coveragePanel");
	}
		
    /**
     * Get the MultipleTextField with the Coverage information.
     * 
     * @return Coverage field
     */
	public MultipleTextField getCoverageBox() {
		return coverageBoxList;
	}
    /**
     * Get the MultipleTextField with the Rights information.
     * 
     * @return Rights field
     */
	public MultipleTextField getRightsBox() {
		return rightsBoxList;
	}

	@Override
	public void clearFields() {
		coverageBoxList.clear();
		rightsBoxList.clear();
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		coverageBoxList.setFocus(focused, 0);
	}

}
