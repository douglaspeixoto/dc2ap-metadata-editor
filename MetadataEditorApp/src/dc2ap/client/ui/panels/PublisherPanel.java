package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

/**
 * Implement the UI panel with the Publisher and
 * Contributor information.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public class PublisherPanel extends GenericPanel {
	private MultipleTextField publisherBoxList = new MultipleTextField(775);
	private MultipleTextField contributorBoxList = new MultipleTextField(775);
	
	public PublisherPanel() {	
		
		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("cw-DialogBox");
		mainPanel.setSpacing(10);
		mainPanel.setSize("980px", "593px");
		
		DecoratedStackPanel publisherStackPanel = new DecoratedStackPanel();
		mainPanel.add(publisherStackPanel);
		publisherStackPanel.setSize("965px", "100%");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		publisherStackPanel.add(verticalPanel_1, "6. Publisher", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblAnEntityResponsible_1 = new Label("An entity responsible for making " +
				"the resource available. One per Line.");
		lblAnEntityResponsible_1.setStyleName("definitionLabel");
		verticalPanel_1.add(lblAnEntityResponsible_1);
		lblAnEntityResponsible_1.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setSize("100%", "100%");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblPublisher = new Label("Publisher:");
		lblPublisher.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblPublisher);
		lblPublisher.setWidth("110px");
		
		// create the publisher label menu
		ElementLabel labelMenuPublisher = new ElementLabel("[O]", "[Mu]", "[St]", "60px");
		verticalPanel_2.add(labelMenuPublisher);
		
		// add the publisher field to the panel
		horizontalPanel.add(publisherBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo publisherInfo = new ImageInfo("Publisher Information", 
						"PUBLISHER", "560px");
		
		// add the image icon to the panel
		horizontalPanel.add(publisherInfo);
		
		DecoratedStackPanel contributorStackPanel = new DecoratedStackPanel();
		mainPanel.add(contributorStackPanel);
		contributorStackPanel.setSize("965px", "100%");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		contributorStackPanel.add(verticalPanel_3, "7. Contributor", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblAnEntityResponsible = new Label("An entity responsible for making " +
				"contributions to the resource. One per line.");
		lblAnEntityResponsible.setStyleName("definitionLabel");
		verticalPanel_3.add(lblAnEntityResponsible);
		lblAnEntityResponsible.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "100%");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblContributor = new Label("Contributor:");
		lblContributor.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblContributor);
		lblContributor.setWidth("110px");
		
		// create the contributor label menu
		ElementLabel labelMenuContributor = new ElementLabel("[Cd]", "[Mu]", "[St]", "63px");
		verticalPanel_4.add(labelMenuContributor);
		
		// add the contributor field to the panel
		horizontalPanel_1.add(contributorBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo contributorInfo = new ImageInfo("Contributor Information", 
						"CONTRIBUTOR", "585px");
		
		// add the image icon to the panel
		horizontalPanel_1.add(contributorInfo);
		
		this.ensureDebugId("publisherPanel");
	}

    /**
     * Get the MultipleTextField with the Publisher information.
     * 
     * @return Publisher field
     */
	public MultipleTextField getPublisherBox() {
		return publisherBoxList;
	}
    /**
     * Get the MultipleTextField with the Contributor information.
     * 
     * @return Contributor field
     */
	public MultipleTextField getContributorBox() {
		return contributorBoxList;
	}
	
	@Override
	public void clearFields() {
		publisherBoxList.clear();
		contributorBoxList.clear();
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		publisherBoxList.setFocus(focused, 0);
		
		contributorBoxList.removeStyleName("warningTextBox");
	}
}
