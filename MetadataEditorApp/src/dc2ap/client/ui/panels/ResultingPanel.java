package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

/**
 * Implement the UI panel with the Resulting Context and
 * Design Guidelines information.
 *
 * @author Douglas A. Peixoto - 05/2012
 */
public class ResultingPanel extends GenericPanel {
	private MultipleTextField resContextBoxList = new MultipleTextField(760);
	private MultipleTextField designGuidelinesBoxList = new MultipleTextField(760);
	
	public ResultingPanel() {
		
		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("cw-DialogBox");
		mainPanel.setSpacing(10);
		mainPanel.setSize("980px", "510px");
		
		DecoratedStackPanel resContextStackPanel = new DecoratedStackPanel();
		mainPanel.add(resContextStackPanel);
		resContextStackPanel.setSize("965px", "120px");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		resContextStackPanel.add(verticalPanel_1, "19. Resulting Context", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblThisElementDescribes = new Label("This element describes adaptations realized in " +
				"the analysis pattern when it is applied in specific systems. One per line.");
		lblThisElementDescribes.setStyleName("definitionLabel");
		verticalPanel_1.add(lblThisElementDescribes);
		lblThisElementDescribes.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setWidth("100%");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblResulting = new Label("Res. Contexts:");
		lblResulting.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblResulting);
		lblResulting.setWidth("120px");
		
		// create the Resulting Context label menu
		ElementLabel labelResContext = new ElementLabel("[O]", "[Mu]", "[St]", "60px");
		verticalPanel_2.add(labelResContext);

		// add the Resulting Context field to the panel
		horizontalPanel.add(resContextBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo resContextInfo = new ImageInfo("Resulting Context Information", 
						"RESULTING_CONTEXT", "580px");

		// add the image icon to the panel
		horizontalPanel.add(resContextInfo);
		
		DecoratedStackPanel designGuidStackPanel = new DecoratedStackPanel();
		mainPanel.add(designGuidStackPanel);
		designGuidStackPanel.setSize("965px", "120px");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		designGuidStackPanel.add(verticalPanel_3, "20. Design Guidelines", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblThisElementPresents = new Label("This element presents general tips for implementation of " +
				"the documented analysis pattern. One per line.");
		lblThisElementPresents.setStyleName("definitionLabel");
		verticalPanel_3.add(lblThisElementPresents);
		lblThisElementPresents.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setWidth("100%");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblDesignGuid = new Label("Design Guide.:");
		lblDesignGuid.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblDesignGuid);
		lblDesignGuid.setWidth("120px");
		
		// create the Design Guidelines label menu
		ElementLabel elementLabel_1 = new ElementLabel("[O]", "[Mu]", "[St]", "60px");
		verticalPanel_4.add(elementLabel_1);

		// add the Design Guidelines field to the panel
		horizontalPanel_1.add(designGuidelinesBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo desGuidInfo = new ImageInfo("Design Guidelines Information", 
						"DESIGN_GUIDE", "580px");

		// add the image icon to the panel
		horizontalPanel_1.add(desGuidInfo);
		
		this.ensureDebugId("resultingPanel");
	}

    /**
     * Get the MultipleTextField with the Resulting Context information.
     * 
     * @return Resulting Context field
     */
	public MultipleTextField getResultingContextBox() {
		return resContextBoxList;
	}
    /**
     * Get the MultipleTextField with the Design Guidelines information.
     * 
     * @return Design Guidelines field
     */
	public MultipleTextField getDesignGuidelinesBox() {
		return designGuidelinesBoxList;
	}

	@Override
	public void clearFields() {
		resContextBoxList.clear();
		designGuidelinesBoxList.clear();
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		resContextBoxList.setFocus(focused, 0);
	}

}
