package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;

/**
 * Implement the UI panel with the History information.
 * Implement a Singleton (Design Pattern) class.
 * Access this class using the method getInstance().
 * 
 * @author Douglas A. Peixoto - 06/2012
 */
public final class HistoryPanel extends GenericPanel {
	private DateBox eventDateBox = new DateBox();
	private MultipleTextField authorBoxList = new MultipleTextField(720);
	private TextBox reasonBox = new TextBox();
	private TextBox changesBox = new TextBox();
	
	public HistoryPanel() {
		
		ScrollPanel mainPanel = new ScrollPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("scrollPanel");
		mainPanel.setSize("985px", "600px");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(10);
		mainPanel.setWidget(verticalPanel);
		verticalPanel.setSize("955px", "100%");
		
		DecoratedStackPanel historyStackPanel = new DecoratedStackPanel();
		verticalPanel.add(historyStackPanel);
		historyStackPanel.setSize("930px", "100%");
		
		FlexTable flexTable = new FlexTable();
		flexTable.setCellPadding(5);
		historyStackPanel.add(flexTable, "16. History", false);
		flexTable.setSize("100%", "100%");
		
		DecoratedStackPanel eventDateStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(0, 0, eventDateStackPanel);
		eventDateStackPanel.setWidth("500px");
		
		VerticalPanel verticalPanel_5 = new VerticalPanel();
		eventDateStackPanel.add(verticalPanel_5, "16.1. Event Date", false);
		verticalPanel_5.setSize("100%", "100%");
		
		Label lblAPointOr = new Label("A point or period of time associated with an " +
				"event in the lifecycle of the resource.");
		lblAPointOr.setStyleName("definitionLabel");
		verticalPanel_5.add(lblAPointOr);
		lblAPointOr.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		verticalPanel_5.add(horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "100%");
		
		VerticalPanel verticalPanel_6 = new VerticalPanel();
		horizontalPanel_2.add(verticalPanel_6);
		
		Label lblEventDate = new Label("Event Date:");
		lblEventDate.setStyleName("textBoldLarge");
		verticalPanel_6.add(lblEventDate);
		lblEventDate.setWidth("110px");

		// create the date created label menu
		ElementLabel labelEventDate = new ElementLabel("[M]", "[S]", "[D]", "50px");
		verticalPanel_6.add(labelEventDate);
		
		// configure and add the event date field to the panel
		eventDateBox.setStylePrimaryName("textBox");
		eventDateBox.setFormat(new DefaultFormat(DateTimeFormat.getFormat("yyyy-MM-dd")));
		eventDateBox.setSize("200px", "40px");
		horizontalPanel_2.add(eventDateBox);

	    // Create the image icon that opens the Info dialog box
		ImageInfo eventDateInfo = new ImageInfo("Event Date Information", 
						"EVENT_DATE", "535px");
		
		// add the image icon to the panel
		horizontalPanel_2.add(eventDateInfo);
		
		DecoratedStackPanel authorStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(1, 0, authorStackPanel);
		authorStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		authorStackPanel.add(verticalPanel_3, "16.2. Author", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblAPersonFamily = new Label("A person, family or corporate body responsible " +
				"for the creation of a work. One per line.");
		lblAPersonFamily.setStyleName("definitionLabel");
		verticalPanel_3.add(lblAPersonFamily);
		lblAPersonFamily.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblAuthor = new Label("Authors:");
		lblAuthor.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblAuthor);
		lblAuthor.setWidth("120px");
		
		// create the Author label menu
		ElementLabel labelAuthor = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_4.add(labelAuthor);

		// add the Auhtor field to the panel
		horizontalPanel_1.add(authorBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo authorInfo = new ImageInfo("Author Information", 
						"AUTHOR", "570px");

		// add the image icon to the panel
		horizontalPanel_1.add(authorInfo);
		
		DecoratedStackPanel reasonStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(2, 0, reasonStackPanel);
		reasonStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		reasonStackPanel.add(verticalPanel_1, "16.3. Reason", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblReasonForThe = new Label("Reason for the occurrence of the historical event.");
		lblReasonForThe.setStyleName("definitionLabel");
		verticalPanel_1.add(lblReasonForThe);
		lblReasonForThe.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblReason = new Label("Reason:");
		lblReason.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblReason);
		lblReason.setWidth("120px");

		// create the Reason label menu
		ElementLabel labelReason = new ElementLabel("[M]", "[S]", "[St]", "52px");
		verticalPanel_2.add(labelReason);

		// add the Reason field to the panel
		reasonBox.setStyleName("textBox");
		reasonBox.setSize("720px", "40px");
		horizontalPanel.add(reasonBox);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo reasonInfo = new ImageInfo("Reason Information", 
						"REASON", "555px");

		// add the image icon to the panel
		horizontalPanel.add(reasonInfo);
		
		DecoratedStackPanel changesStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(3, 0, changesStackPanel);
		changesStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_7 = new VerticalPanel();
		changesStackPanel.add(verticalPanel_7, "16.4. Changes", false);
		verticalPanel_7.setSize("100%", "100%");
		
		Label lblChangesMadeIn = new Label("Changes made in the analysis pattern during an event of changes.");
		lblChangesMadeIn.setStyleName("definitionLabel");
		verticalPanel_7.add(lblChangesMadeIn);
		lblChangesMadeIn.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		verticalPanel_7.add(horizontalPanel_3);
		horizontalPanel_3.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_8 = new VerticalPanel();
		horizontalPanel_3.add(verticalPanel_8);
		
		Label lblChanges = new Label("Changes:");
		lblChanges.setStyleName("textBoldLarge");
		verticalPanel_8.add(lblChanges);
		lblChanges.setWidth("120px");
		
		// create the Changes label menu
		ElementLabel labelChanges = new ElementLabel("[Cd]", "[S]", "[St]", "56px");
		verticalPanel_8.add(labelChanges);

		// add the Changes field to the panel
		changesBox.setStyleName("textBox");
		changesBox.setSize("720px", "40px");
		horizontalPanel_3.add(changesBox);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo changesInfo = new ImageInfo("Changes Information", 
						"CHANGES", "570px");

		// add the image icon to the panel
		horizontalPanel_3.add(changesInfo);
		
		this.ensureDebugId("historyPanel");
	}

    /**
     * Get the DateBox with the Event Date information.
     * 
     * @return Event Date Date Box
     */
	public DateBox getEventDateBox() {
		return eventDateBox;
	}
    /**
     * Get the MultipleTextField with the Authors information.
     * 
     * @return Author field
     */
	public MultipleTextField getAuthorBox() {
		return authorBoxList;
	}
    /**
     * Get the TextBox with the Reason information.
     * 
     * @return Reason Text Box
     */
	public TextBox getReasonBox() {
		return reasonBox;
	}
    /**
     * Get the TextBox with the Changes information.
     * 
     * @return Changes Text Box
     */
	public TextBox getChangesBox() {
		return changesBox;
	}

	@Override
	public void clearFields() {
		eventDateBox.setValue(null);
		authorBoxList.clear();
		reasonBox.setText("");
		changesBox.setText("");
		
		eventDateBox.removeStyleName("errorTextBox");
		reasonBox.removeStyleName("errorTextBox");
		changesBox.removeStyleName("warningTextBox");
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		eventDateBox.setFocus(focused);
	}
	
}
