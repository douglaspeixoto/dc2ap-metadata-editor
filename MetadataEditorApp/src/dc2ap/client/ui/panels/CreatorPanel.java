package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

/**
 * Implement the UI panel with the Creator and Subject information.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public final class CreatorPanel extends GenericPanel {
	private MultipleTextField creatorBoxList = new MultipleTextField(780);
	private MultipleTextField subjectBoxList = new MultipleTextField(780);

	public CreatorPanel() {
		
		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		mainPanel.setSpacing(10);
		mainPanel.setStyleName("cw-DialogBox");
		mainPanel.setSize("980px", "510px");
		
		DecoratedStackPanel creatorStackPanel = new DecoratedStackPanel();
		mainPanel.add(creatorStackPanel);
		creatorStackPanel.setSize("965px", "120px");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		creatorStackPanel.add(verticalPanel_3, "3. Creator", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblAnEntityPrimarily = new Label("An entity primarily responsible for making the resource. One per line.");
		lblAnEntityPrimarily.setStyleName("definitionLabel");
		verticalPanel_3.add(lblAnEntityPrimarily);
		lblAnEntityPrimarily.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_3);
		horizontalPanel_3.setWidth("100%");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel_3.add(verticalPanel_2);
		
		Label lblCreator = new Label("Creators:");
		verticalPanel_2.add(lblCreator);
		lblCreator.setStyleName("textBoldLarge");
		lblCreator.setWidth("100px");
		
		// create the creator label menu
		ElementLabel labelMenuCreator = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_2.add(labelMenuCreator);

		// add the creator field to the panel
		horizontalPanel_3.add(creatorBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo creatorInfo = new ImageInfo("Creator Information", 
						"CREATOR", "555px");	

		// add the image icon to the panel
		horizontalPanel_3.add(creatorInfo);
		
		DecoratedStackPanel subjectStackPanel = new DecoratedStackPanel();
		mainPanel.add(subjectStackPanel);
		subjectStackPanel.setSize("965px", "120px");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		subjectStackPanel.add(verticalPanel_1, "4. Subject", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblSubjectOfThe = new Label("The topic of the resource. One per line.");
		lblSubjectOfThe.setStyleName("definitionLabel");
		verticalPanel_1.add(lblSubjectOfThe);
		lblSubjectOfThe.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setWidth("100%");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_4);
		
		Label lblSubject = new Label("Subjects:");
		verticalPanel_4.add(lblSubject);
		lblSubject.setStyleName("textBoldLarge");
		lblSubject.setWidth("100px");
		
		// create the creator label menu
		ElementLabel labelMenuSubject = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_4.add(labelMenuSubject);
				
		// add the Subject field to the panel
		horizontalPanel.add(subjectBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo subjectInfo = new ImageInfo("Subject Information", 
						"SUBJECT", "535px");

		// add the image icon to the panel
		horizontalPanel.add(subjectInfo);
		
		this.ensureDebugId("creatorPanel");
	}

    /**
     * Get the MultipleTextField with the Creator information.
     * 
     * @return Creator field
     */
	public MultipleTextField getCreatorBox() {
		return creatorBoxList;
	}
    /**
     * Get the MultipleTextField with the Subject information.
     * 
     * @return Subject field
     */
	public MultipleTextField getSubjectBox() {
		return subjectBoxList;
	}

	@Override
	public void clearFields() {
		creatorBoxList.clear();	
		subjectBoxList.clear();
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		creatorBoxList.setFocus(focused, 0);
	}

}
