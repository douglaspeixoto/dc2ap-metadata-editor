package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

import com.google.gwt.user.client.ui.TextBox;

/**
 * Implement the UI panel with the Relation information.
 * 
 * @author Douglas A. Peixoto - 06/2012
 */
public class RelationPanel extends GenericPanel {
	private TextBox isVersionOfBox = new TextBox();
	private MultipleTextField isReplacedByBoxList = new MultipleTextField(720);
	private MultipleTextField replacesBoxList = new MultipleTextField(720);
	private MultipleTextField isPartOfBoxList = new MultipleTextField(720);
	private MultipleTextField hasPartBoxList = new MultipleTextField(720);
	private MultipleTextField isDesignedWithBoxList = new MultipleTextField(720);
	private MultipleTextField shouldAvoidBoxList = new MultipleTextField(720);
	private MultipleTextField complementedByBoxList = new MultipleTextField(720);
	private TextBox aboutBox = new TextBox();
	
	public RelationPanel() {
		
		ScrollPanel mainPanel = new ScrollPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("scrollPanel");
		mainPanel.ensureDebugId("relationPanel");
		mainPanel.setSize("985px", "600px");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(10);
		mainPanel.setWidget(verticalPanel);
		verticalPanel.setSize("955px", "100%");
		
		DecoratedStackPanel relationStackPanel = new DecoratedStackPanel();
		verticalPanel.add(relationStackPanel);
		relationStackPanel.setSize("930px", "100%");
		
		FlexTable mainTable = new FlexTable();
		mainTable.setCellPadding(5);
		relationStackPanel.add(mainTable, "13. Relation", false);
		mainTable.setSize("100%", "100%");
		
		DecoratedStackPanel isVersOfStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(0, 0, isVersOfStackPanel);
		isVersOfStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		isVersOfStackPanel.add(verticalPanel_1, "13.1. Is Version Of", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblARelatedResource = new Label("A related resource of which the described " +
				"resource is a version, edition, or adaptation.");
		lblARelatedResource.setStyleName("definitionLabel");
		verticalPanel_1.add(lblARelatedResource);
		lblARelatedResource.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblIsVersionOf = new Label("Is Version Of:");
		lblIsVersionOf.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblIsVersionOf);
		lblIsVersionOf.setWidth("120px");
		
		// create the Is Version Of label menu
		ElementLabel labelMenuIsVerOf = new ElementLabel("[Cd]", "[S]", "[UNS]", "68px");
		verticalPanel_2.add(labelMenuIsVerOf);
		
		// add the isVersionOf field to the panel
		horizontalPanel.add(isVersionOfBox);
		isVersionOfBox.setStyleName("textBox");
		isVersionOfBox.setSize("720px", "40px");
		
	    // Create the image icon that opens the dialog
		ImageInfo isVerOfInfo = new ImageInfo("Is Version Of Information", 
						"IS_VERSION_OF", "550px");
		
		// add the image icon to the panel
		horizontalPanel.add(isVerOfInfo);
		
		DecoratedStackPanel isRepByStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(1, 0, isRepByStackPanel);
		isRepByStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		isRepByStackPanel.add(verticalPanel_3, "13.2. Is Replaced By", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblARelatedResource_1 = new Label("A related resource that supplants, displaces, " +
				"or supersedes the described resource. One per line.");
		lblARelatedResource_1.setStyleName("definitionLabel");
		verticalPanel_3.add(lblARelatedResource_1);
		lblARelatedResource_1.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblIsReplacedBy = new Label("Is Replaced By:");
		lblIsReplacedBy.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblIsReplacedBy);
		lblIsReplacedBy.setWidth("120px");

		// create the Is replaced By label menu
		ElementLabel labelMenuRepBy = new ElementLabel("[Cd]", "[Mu]", "[UNS]", "75px");
		verticalPanel_4.add(labelMenuRepBy);
		
		// add the isReplacedBy field to the panel
		horizontalPanel_1.add(isReplacedByBoxList);

	    // Create the image icon that opens the dialog
		ImageInfo isRepByInfo = new ImageInfo("Is Replaced By Information", 
						"IS_REPLACED_BY", "560px");

		// add the image icon to the panel
		horizontalPanel_1.add(isRepByInfo);
		
		DecoratedStackPanel replacesStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(2, 0, replacesStackPanel);
		replacesStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_5 = new VerticalPanel();
		replacesStackPanel.add(verticalPanel_5, "13.3. Replaces", false);
		verticalPanel_5.setSize("100%", "100%");
		
		Label lblARelatedResource_2 = new Label("A related resource that is supplanted, displaced, " +
				"or superseded by the described resource. One per line.");
		lblARelatedResource_2.setStyleName("definitionLabel");
		verticalPanel_5.add(lblARelatedResource_2);
		lblARelatedResource_2.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		verticalPanel_5.add(horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_6 = new VerticalPanel();
		horizontalPanel_2.add(verticalPanel_6);
		
		Label lblReplaces = new Label("Replaces:");
		lblReplaces.setStyleName("textBoldLarge");
		verticalPanel_6.add(lblReplaces);
		lblReplaces.setWidth("120px");
		
		// create the Replaces By label menu
		ElementLabel labelMenuReplaces = new ElementLabel("[Cd]", "[Mu]", "[UNS]", "75px");
		verticalPanel_6.add(labelMenuReplaces);
		
		// add the Replaces field to the panel
		horizontalPanel_2.add(replacesBoxList);

	    // Create the image icon that opens the dialog
		ImageInfo replacesInfo = new ImageInfo("Replaces Information", 
						"REPLACES", "560px");

		// add the image icon to the panel
		horizontalPanel_2.add(replacesInfo);
		
		DecoratedStackPanel isPartOfStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(3, 0, isPartOfStackPanel);
		isPartOfStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_7 = new VerticalPanel();
		isPartOfStackPanel.add(verticalPanel_7, "13.4. Is Part Of", false);
		verticalPanel_7.setSize("100%", "100%");
		
		Label lblARelatedResource_3 = new Label("A related resource in which the described " +
				"resource is physically or logically included. One per line.");
		lblARelatedResource_3.setStyleName("definitionLabel");
		verticalPanel_7.add(lblARelatedResource_3);
		lblARelatedResource_3.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		verticalPanel_7.add(horizontalPanel_3);
		horizontalPanel_3.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_8 = new VerticalPanel();
		horizontalPanel_3.add(verticalPanel_8);
		
		Label lblIspartof = new Label("Is Part Of:");
		lblIspartof.setStyleName("textBoldLarge");
		verticalPanel_8.add(lblIspartof);
		lblIspartof.setWidth("120px");

		// create the Is Part Of label menu
		ElementLabel labelMenuIsPartOf = new ElementLabel("[O]", "[Mu]", "[UNS]", "70px");
		verticalPanel_8.add(labelMenuIsPartOf);
		
		// add the Is Part Of field to the panel
		horizontalPanel_3.add(isPartOfBoxList);

	    // Create the image icon that opens the dialog
		ImageInfo isPartOfInfo = new ImageInfo("Is Part Of Information", 
						"IS_PART_OF", "590px");

		// add the image icon to the panel
		horizontalPanel_3.add(isPartOfInfo);
		
		DecoratedStackPanel hasPartStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(4, 0, hasPartStackPanel);
		hasPartStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_9 = new VerticalPanel();
		hasPartStackPanel.add(verticalPanel_9, "13.5. Has Part", false);
		verticalPanel_9.setSize("100%", "100%");
		
		Label lblARelatedResource_4 = new Label("A related resource that is included either " +
				"physically or logically in the described resource. One per line.");
		lblARelatedResource_4.setStyleName("definitionLabel");
		verticalPanel_9.add(lblARelatedResource_4);
		lblARelatedResource_4.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
		verticalPanel_9.add(horizontalPanel_4);
		horizontalPanel_4.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_10 = new VerticalPanel();
		horizontalPanel_4.add(verticalPanel_10);
		
		Label lblHasPart = new Label("Has Part:");
		lblHasPart.setStyleName("textBoldLarge");
		verticalPanel_10.add(lblHasPart);
		lblHasPart.setWidth("120px");
		
		// create the Has Part label menu
		ElementLabel labelMenuHasPart = new ElementLabel("[O]", "[Mu]", "[UNS]", "70px");
		verticalPanel_10.add(labelMenuHasPart);
		
		// add the Has Part field to the panel
		horizontalPanel_4.add(hasPartBoxList);

	    // Create the image icon that opens the dialog
		ImageInfo hasPartInfo = new ImageInfo("Has Part Information", 
						"HAS_PART", "580px");

		// add the image icon to the panel
		horizontalPanel_4.add(hasPartInfo);
		
		DecoratedStackPanel isDesigWithStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(5, 0, isDesigWithStackPanel);
		isDesigWithStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_11 = new VerticalPanel();
		isDesigWithStackPanel.add(verticalPanel_11, "13.6. Is Designed With", false);
		verticalPanel_11.setSize("100%", "100%");
		
		Label lblKnownDesignPatterns = new Label("Known design patterns that can be used during the " +
				"implementation of the documented analysis pattern. One per line.");
		lblKnownDesignPatterns.setStyleName("definitionLabel");
		verticalPanel_11.add(lblKnownDesignPatterns);
		lblKnownDesignPatterns.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_5 = new HorizontalPanel();
		verticalPanel_11.add(horizontalPanel_5);
		horizontalPanel_5.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_12 = new VerticalPanel();
		horizontalPanel_5.add(verticalPanel_12);
		
		Label lblIsDesignedWith = new Label("Is Desig. With:");
		lblIsDesignedWith.setStyleName("textBoldLarge");
		verticalPanel_12.add(lblIsDesignedWith);
		lblIsDesignedWith.setWidth("120px");
		
		// create the Is Designed With label menu
		ElementLabel labelMenuIsDesigWith = new ElementLabel("[O]", "[Mu]", "[UNS]", "70px");
		verticalPanel_12.add(labelMenuIsDesigWith);
		
		// add the isDesignedWith field to the panel
		horizontalPanel_5.add(isDesignedWithBoxList);

	    // Create the image icon that opens the dialog
		ImageInfo isDesigWithInfo = new ImageInfo("Is Designed With Information", 
						"IS_DESIGNED_WITH", "600px");

		// add the image icon to the panel
		horizontalPanel_5.add(isDesigWithInfo);
		
		DecoratedStackPanel shouldAvoidStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(6, 0, shouldAvoidStackPanel);
		shouldAvoidStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_13 = new VerticalPanel();
		shouldAvoidStackPanel.add(verticalPanel_13, "13.7. Should Avoid", false);
		verticalPanel_13.setSize("100%", "100%");
		
		Label lblAntipatternsImportantTo = new Label("Anti-patterns important to avoid common errors " +
				"identified during the application of the documented analysis pattern. One per line.");
		lblAntipatternsImportantTo.setStyleName("definitionLabel");
		verticalPanel_13.add(lblAntipatternsImportantTo);
		lblAntipatternsImportantTo.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_6 = new HorizontalPanel();
		verticalPanel_13.add(horizontalPanel_6);
		horizontalPanel_6.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_14 = new VerticalPanel();
		horizontalPanel_6.add(verticalPanel_14);
		
		Label lblShouldAvoid = new Label("Should Avoid:");
		lblShouldAvoid.setStyleName("textBoldLarge");
		verticalPanel_14.add(lblShouldAvoid);
		lblShouldAvoid.setWidth("120px");

		// create the Is Designed By label menu
		ElementLabel labelMenuSouldAvoid = new ElementLabel("[O]", "[Mu]", "[UNS]", "70px");
		verticalPanel_14.add(labelMenuSouldAvoid);
		
		// add the isDesignedWith field to the panel
		horizontalPanel_6.add(shouldAvoidBoxList);

	    // Create the image icon that opens the dialog
		ImageInfo shouldAvoidInfo = new ImageInfo("Should Avoid Information", 
						"SHOULD_AVOID", "595px");

		// add the image icon to the panel
		horizontalPanel_6.add(shouldAvoidInfo);
		
		DecoratedStackPanel compByStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(7, 0, compByStackPanel);
		compByStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_15 = new VerticalPanel();
		compByStackPanel.add(verticalPanel_15, "13.8. Complemented By", false);
		verticalPanel_15.setSize("100%", "100%");
		
		Label lblKnownAnalysisPatterns = new Label("Known analysis patterns that can be used to " +
				"complement the documented analysis pattern. One per line.");
		lblKnownAnalysisPatterns.setStyleName("definitionLabel");
		verticalPanel_15.add(lblKnownAnalysisPatterns);
		lblKnownAnalysisPatterns.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_7 = new HorizontalPanel();
		verticalPanel_15.add(horizontalPanel_7);
		horizontalPanel_7.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_16 = new VerticalPanel();
		horizontalPanel_7.add(verticalPanel_16);
		
		Label lblComplemBy = new Label("Complem. By:");
		lblComplemBy.setStyleName("textBoldLarge");
		verticalPanel_16.add(lblComplemBy);
		lblComplemBy.setWidth("120px");
		
		// create the Complemented By label menu
		ElementLabel labelMenuCompBy = new ElementLabel("[O]", "[Mu]", "[UNS]", "70px");
		verticalPanel_16.add(labelMenuCompBy);
		
		// add the complementedBy field to the panel
		horizontalPanel_7.add(complementedByBoxList);

	    // Create the image icon that opens the dialog
		ImageInfo compByInfo = new ImageInfo("Complemented By Information", 
						"COMPLEMENTED_BY", "615px");

		// add the image icon to the panel
		horizontalPanel_7.add(compByInfo);
		
		DecoratedStackPanel aboutStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(8, 0, aboutStackPanel);
		aboutStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_17 = new VerticalPanel();
		aboutStackPanel.add(verticalPanel_17, "13.9. About", false);
		verticalPanel_17.setSize("100%", "100%");
		
		Label lblAnAnnotationProviding = new Label("An annotation providing additional " +
				"information relating to data recorded in another element.");
		lblAnAnnotationProviding.setStyleName("definitionLabel");
		verticalPanel_17.add(lblAnAnnotationProviding);
		lblAnAnnotationProviding.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_8 = new HorizontalPanel();
		verticalPanel_17.add(horizontalPanel_8);
		horizontalPanel_8.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_18 = new VerticalPanel();
		horizontalPanel_8.add(verticalPanel_18);
		
		Label lblAbout = new Label("About:");
		lblAbout.setStyleName("textBoldLarge");
		verticalPanel_18.add(lblAbout);
		lblAbout.setWidth("120px");

		// create the About label menu
		ElementLabel labelMenuAbout = new ElementLabel("[Cd]", "[S]", "[St]", "60px");
		verticalPanel_18.add(labelMenuAbout);
		
		// add the About field to the panel
		horizontalPanel_8.add(aboutBox);
		aboutBox.setStyleName("textBox");
		aboutBox.setSize("720px", "40px");
		
	    // Create the image icon that opens the dialog
		ImageInfo aboutInfo = new ImageInfo("About Information", 
						"ABOUT", "560px");
		
		// add the image icon to the panel
		horizontalPanel_8.add(aboutInfo);
		
		this.ensureDebugId("relationPanel");
	}

    /**
     * Get the TextBox with the Is Version Of information.
     * 
     * @return Is Version Of TextBox
     */	
	public TextBox getIsVersionOfBox() {
		return isVersionOfBox;
	}
    /**
     * Get the MultipleTextField with the Is Replaced By information.
     * 
     * @return Is Replaced By field
     */
	public MultipleTextField getIsReplacedByBox() {
		return isReplacedByBoxList;
	}
    /**
     * Get the MultipleTextField with the Replaces information.
     * 
     * @return Replaces MultipleTextField List
     */
	public MultipleTextField getReplacesBox() {
		return replacesBoxList;
	}
    /**
     * Get the MultipleTextField with the Is Part Of information.
     * 
     * @return Is Part Of field
     */
	public MultipleTextField getIsPartOfBox() {
		return isPartOfBoxList;
	}
    /**
     * Get the MultipleTextField with the Has Part information.
     * 
     * @return Has Part field
     */
	public MultipleTextField getHasPartBox() {
		return hasPartBoxList;
	}
    /**
     * Get the String list with the Is Designed With information.
     * 
     * @return Is Designed With String List
     */
	public MultipleTextField getIsDesignedWithBox() {
		return isDesignedWithBoxList;
	}
    /**
     * Get the MultipleTextField with the Should Avoid information.
     * 
     * @return Is Replaced By field
     */
	public MultipleTextField getShouldAvoidBox() {
		return shouldAvoidBoxList;
	}
    /**
     * Get the MultipleTextField with the Complemented By information.
     * 
     * @return Complemented By String List
     */
	public MultipleTextField getComplementedByBox() {
		return complementedByBoxList;
	}
    /**
     * Get the TextBox with the About information.
     * 
     * @return About TextBox
     */	
	public TextBox getAboutBox() {
		return aboutBox;
	}

	@Override
	public void clearFields() {
		isVersionOfBox.setText("");
		isReplacedByBoxList.clear();
		replacesBoxList.clear();
		isPartOfBoxList.clear();
		hasPartBoxList.clear();
		isDesignedWithBoxList.clear();
		shouldAvoidBoxList.clear();
		complementedByBoxList.clear();
		aboutBox.setText("");
		
		isVersionOfBox.removeStyleName("warningTextBox");
		aboutBox.removeStyleName("warningTextBox");
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		isVersionOfBox.setFocus(focused);
	}

}
