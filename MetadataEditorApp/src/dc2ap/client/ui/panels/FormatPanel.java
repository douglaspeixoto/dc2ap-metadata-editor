package dc2ap.client.ui.panels;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;
import dc2ap.client.entities.Languages;
import dc2ap.client.services.UIService;
import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;

/**
 * Implement the UI panel with the Format, Source
 * and Language information.
 * 
 * @author Douglas A. Peixoto - 06/2012
 */
public final class FormatPanel extends GenericPanel {	
	private CheckBox xmiBox = new CheckBox("XMI");
	private CheckBox jpegBox = new CheckBox("JPEG");
	private CheckBox pngBox = new CheckBox("PNG");
	private TextBox sourceBox = new TextBox();
	private SuggestBox languageValueBox = null;
	private SuggestBox languageLabelBox = null;

	// list of chosen formats
	private List<String> formatList = new ArrayList<String>();
	
	// Languages to be used in the SuggestBox Fields
	private Languages languages = UIService.getLanguages();

	public FormatPanel() {
	
		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		mainPanel.setSpacing(10);
		mainPanel.setSize("980px", "344px");
		
		DecoratedStackPanel formatStackPanel = new DecoratedStackPanel();
		mainPanel.add(formatStackPanel);
		formatStackPanel.setSize("532px", "96px");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		formatStackPanel.add(verticalPanel_1, "10. Format", false);
		verticalPanel_1.setSize("520px", "66px");
		
		Label lblTheFileFormat = new Label("The file format, physical medium, or dimensions of the resource.");
		lblTheFileFormat.setStyleName("definitionLabel");
		verticalPanel_1.add(lblTheFileFormat);
		lblTheFileFormat.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setSize("100%", "55px");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblFormat = new Label("Format:");
		lblFormat.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblFormat);
		lblFormat.setWidth("130px");
		
		// create the format label menu
		ElementLabel labelMenuFomat = new ElementLabel("[M]", "[Mu]", "[US]", "63px");
		verticalPanel_2.add(labelMenuFomat);
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		horizontalPanel_3.setVerticalAlignment(HasVerticalAlignment.ALIGN_BOTTOM);
		horizontalPanel_3.setStyleName("backgroundDarkBox");
		horizontalPanel.add(horizontalPanel_3);
		horizontalPanel_3.setSize("323px", "50px");
		
		// add the check boxes to the panel
		horizontalPanel_3.add(xmiBox);
		horizontalPanel_3.setCellVerticalAlignment(xmiBox, HasVerticalAlignment.ALIGN_MIDDLE);
		horizontalPanel.setCellVerticalAlignment(xmiBox, HasVerticalAlignment.ALIGN_MIDDLE);
		xmiBox.setSize("69px", "25px");
		
		horizontalPanel_3.add(jpegBox);
		horizontalPanel_3.setCellVerticalAlignment(jpegBox, HasVerticalAlignment.ALIGN_MIDDLE);
		horizontalPanel.setCellVerticalAlignment(jpegBox, HasVerticalAlignment.ALIGN_MIDDLE);
		jpegBox.setSize("25px", "25px");
		
		horizontalPanel_3.add(pngBox);
		horizontalPanel_3.setCellVerticalAlignment(pngBox, HasVerticalAlignment.ALIGN_MIDDLE);
		horizontalPanel.setCellVerticalAlignment(pngBox, HasVerticalAlignment.ALIGN_MIDDLE);
		pngBox.setSize("87px", "25px");
		
	    // Create the image icon that opens the dialog
		ImageInfo formatInfo = new ImageInfo("Format Information", 
	    			"FORMAT", "625px");
		
		// add the image icon to the panel
		horizontalPanel.add(formatInfo);

		DecoratedStackPanel decoratedStackPanel = new DecoratedStackPanel();
		mainPanel.add(decoratedStackPanel);
		decoratedStackPanel.setSize("965px", "40px");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		decoratedStackPanel.add(verticalPanel_3, "11. Source", false);
		verticalPanel_3.setSize("100%", "45px");
		
		Label lblARelatedResource = new Label("A related resource from which the described resource is derived.");
		lblARelatedResource.setStyleName("definitionLabel");
		verticalPanel_3.add(lblARelatedResource);
		lblARelatedResource.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "62px");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblSource = new Label("Source:");
		lblSource.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblSource);
		lblSource.setWidth("130px");

		// create the source label menu
		ElementLabel labelMenuSource = new ElementLabel("[Cd]", "[S]", "[UNS]", "66px");
		verticalPanel_4.add(labelMenuSource);
		
		// add the source box to the panel
		sourceBox.setStyleName("textBox");
		horizontalPanel_1.add(sourceBox);
		sourceBox.setSize("750px", "40px");
		
	    // Create the image icon that opens the dialog
		Image infoSource = new Image("images/info.png");
		infoSource.setTitle("Element Info");
		infoSource.setStyleName("imageLink");
		
		// show pop up event
		ImageInfo sourceInfo = new ImageInfo("Source Information", 
	    			"SOURCE", "555px");

		// add the image icon to the panel
		horizontalPanel_1.add(sourceInfo);
		
		DecoratedStackPanel decoratedStackPanel_1 = new DecoratedStackPanel();
		mainPanel.add(decoratedStackPanel_1);
		decoratedStackPanel_1.setSize("965px", "76px");
		
		VerticalPanel verticalPanel_5 = new VerticalPanel();
		decoratedStackPanel_1.add(verticalPanel_5, "12. Language", false);
		verticalPanel_5.setSize("100%", "55px");
		
		Label label = new Label("Language used for documenting the analysis pattern.");
		label.setStyleName("definitionLabel");
		verticalPanel_5.add(label);
		label.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		verticalPanel_5.add(horizontalPanel_2);
		verticalPanel_5.setCellHorizontalAlignment(horizontalPanel_2, 
				HasHorizontalAlignment.ALIGN_CENTER);
		horizontalPanel_2.setSize("100%", "62px");
		
		VerticalPanel verticalPanel_6 = new VerticalPanel();
		horizontalPanel_2.add(verticalPanel_6);
		
		Label lblLanguage = new Label("Language Label:");
		lblLanguage.setStyleName("textBoldLarge");
		verticalPanel_6.add(lblLanguage);
		lblLanguage.setWidth("130px");

		// create the source label menu
		ElementLabel labelMenuLanguage = new ElementLabel("[M]", "[S]", "[US]", "56px");
		verticalPanel_6.add(labelMenuLanguage);

		// add the language box to the panel, set the languages names
		languageLabelBox = new SuggestBox(languages.getLabelsOracle());
		languageLabelBox.setStyleName("textBox");
		languageLabelBox.setSize("420px", "40px");
		horizontalPanel_2.add(languageLabelBox);

		// add event that set in the languageIdBox the Id of the language
		// according with the language name selected
		languageLabelBox.addSelectionHandler(new SelectionHandler<SuggestOracle.Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				setLanguageValueField();
			}
		});
		
		Label lblId = new Label("Language Value:");
		lblId.setStyleName("textBoldLarge");
		horizontalPanel_2.add(lblId);
		lblId.setSize("130px", "35px");
		
		// add the language box to the panel, set the languages id
		languageValueBox = new SuggestBox(languages.getValuesOracle());
		languageValueBox.setStyleName("textBox");
		languageValueBox.setSize("150px", "40px");
		horizontalPanel_2.add(languageValueBox);
		
		// add event that set in the languageNameBox the Name of the language
		// according with the language Id selected
		languageValueBox.addSelectionHandler(new SelectionHandler<SuggestOracle.Suggestion>() {
			@Override
			public void onSelection(SelectionEvent<Suggestion> event) {
				setLanguageLabelField();
			}
		});
		
		// set English as default language
		languageLabelBox.setText("English");
		languageValueBox.setText("eng");
		
	    // Create the image icon that opens the dialog
		ImageInfo languageInfo = new ImageInfo("Language Information", 
	    			"LANGUAGE", "575px");
		
		// add the image icon to the panel
		horizontalPanel_2.add(languageInfo);
		
		this.ensureDebugId("formatPanel");
	}

    /**
     * Get the CheckBox with the Format (XMI) information.
     * 
     * @return Format (XMI) Box
     */
	public CheckBox getXmiBox() {
		return xmiBox;
	}
    /**
     * Get the CheckBox with the Format (JPEG) information.
     * 
     * @return Format (JEPEG) Box
     */
	public CheckBox getJpegBox() {
		return jpegBox;
	}
    /**
     * Get the CheckBox with the Format (PNG) information.
     * 
     * @return Format (PNG) Box
     */
	public CheckBox getPngBox() {
		return pngBox;
	}
	/**
     * Get the list of formats chosen.
     * 
     * @return Formats list
     */
    public List<String> getFormatList() {
		if(xmiBox.getValue()){
			formatList.add("XMI");
		}else{
			formatList.remove("XMI");
		}
		if(jpegBox.getValue()){
			formatList.add("JPEG");
		}else{
			formatList.remove("JPEG");
		}
		if(pngBox.getValue()){
			formatList.add("PNG");
		}else{
			formatList.remove("PNG");
		}
		
    	return formatList;
	}
	/**
     * Set the formats.
     * 
     * @return Formats string list to be set.
     * Must contain the Strings: 'XMI', 'JPEG' and/or 'PNG'.
     */
    public void setFormatList(List<String> list) {
		if(list != null){
			if(list.contains("XMI")){
				xmiBox.setValue(true);
			} else{
				xmiBox.setValue(false);
			}
			if(list.contains("JPEG")){
				jpegBox.setValue(true);
			} else{
				jpegBox.setValue(false);
			}
			if(list.contains("PNG")){
				pngBox.setValue(true);
			} else{
				pngBox.setValue(false);
			}
		}    	
	}
    
	/**
     * Get the TextBox with the Source information.
     * 
     * @return Source Box
     */
	public TextBox getSourceBox() {
		return sourceBox;
	}
    /**
     * Get the SuggestBox with the Language Name information.
     * 
     * @return Language Name Box
     */
	public SuggestBox getLanguageLabelBox() {
		return languageLabelBox;
	}
    /**
     * Get the SuggestBox with the Language Id information.
     * 
     * @return Language Id Box
     */
	public SuggestBox getLanguageValueBox() {
		return languageValueBox;
	}
	
	@Override
	public void clearFields() {
		xmiBox.setValue(false);	
		jpegBox.setValue(false);
		pngBox.setValue(false);
		sourceBox.setText("");
		languageLabelBox.setText("");
		languageValueBox.setText("");
		
		sourceBox.removeStyleName("errorTextBox");
		languageValueBox.removeStyleName("errorTextBox");
		languageLabelBox.removeStyleName("errorTextBox");
	}
	
	/**
	 * Set the Id in the field languageIdBox when
	 * the field languageNameBox changes.
	 */
	private void setLanguageValueField(){
		String name = languageLabelBox.getText();		
		if(name != null && !"".equals(name)){
			String id = UIService
				.getLanguageValueByLanguageLabel(name);
			languageValueBox.setText(id);
		}
	}
	
	/**
	 * Set the Name in the field languageNameBox when
	 * the languageIdBox field changes.
	 */
	private void setLanguageLabelField(){
		String id = languageValueBox.getText();
		if(id != null && !"".equals(id)){
			String name = UIService
				.getLanguageLabelByLanguageValue(id);
			languageLabelBox.setText(name);
		}
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		xmiBox.setFocus(focused);
	}

}
