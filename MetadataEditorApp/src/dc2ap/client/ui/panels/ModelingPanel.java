package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

import com.google.gwt.user.client.ui.TextBox;

/**
 * Implement the UI panel with the Modeling information.
 * 
 * @author Douglas A. Peixoto - 06/2012
 */
public final class ModelingPanel extends GenericPanel {
	private TextBox useCaseDiagramBox = new TextBox();
	private MultipleTextField colabSeqDiagBoxList = new MultipleTextField(690);
	private MultipleTextField actStateBoxList = new MultipleTextField(690);
	private TextBox classDiagramBox = new TextBox();
	private MultipleTextField classDescrBoxList = new MultipleTextField(690);
	private MultipleTextField relatDescrBoxList = new MultipleTextField(690);
	private MultipleTextField solutionVarBoxList = new MultipleTextField(710);
	
	public ModelingPanel() {
		
		ScrollPanel mainPanel = new ScrollPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("scrollPanel");
		mainPanel.setSize("985px", "600px");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(10);
		mainPanel.setWidget(verticalPanel);
		verticalPanel.setSize("955px", "100%");
		
		DecoratedStackPanel modellingStackPanel = new DecoratedStackPanel();
		verticalPanel.add(modellingStackPanel);
		modellingStackPanel.setSize("930px", "100%");
		
		FlexTable flexTable = new FlexTable();
		flexTable.setCellPadding(5);
		modellingStackPanel.add(flexTable, "18. Modeling", false);
		flexTable.setSize("100%", "100%");
		
		DecoratedStackPanel behaviorStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(0, 0, behaviorStackPanel);
		behaviorStackPanel.setSize("920px", "100%");
		
		FlexTable flexTable_1 = new FlexTable();
		flexTable_1.setCellPadding(5);
		behaviorStackPanel.add(flexTable_1, "18.1. Behavior", false);
		flexTable_1.setSize("100%", "100%");
		
		DecoratedStackPanel useCaseStackPanel = new DecoratedStackPanel();
		flexTable_1.setWidget(0, 0, useCaseStackPanel);
		useCaseStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_6 = new VerticalPanel();
		useCaseStackPanel.add(verticalPanel_6, "18.1.1. Use Case Diagram", false);
		verticalPanel_6.setSize("100%", "100%");
		
		Label lblDiagramUsedTo = new Label("Diagram used to represent a system that meets requirements " +
				"of an analysis pattern together with entities which interact with them.");
		lblDiagramUsedTo.setStyleName("definitionLabel");
		verticalPanel_6.add(lblDiagramUsedTo);
		lblDiagramUsedTo.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		verticalPanel_6.add(horizontalPanel_3);
		horizontalPanel_3.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_7 = new VerticalPanel();
		horizontalPanel_3.add(verticalPanel_7);
		
		Label lblUseCaseDiagram = new Label("Use Case Diag.:");
		lblUseCaseDiagram.setStyleName("textBoldLarge");
		verticalPanel_7.add(lblUseCaseDiagram);
		lblUseCaseDiagram.setWidth("130px");

		// create the Use Case Diagram label menu
		ElementLabel labelUseCase = new ElementLabel("[M]", "[S]", "[U]", "50px");
		verticalPanel_7.add(labelUseCase);

		// add the Use Case Diagram field to the panel
		useCaseDiagramBox.setStyleName("textBox");
		useCaseDiagramBox.setSize("690px", "40px");
		horizontalPanel_3.add(useCaseDiagramBox);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo useCaseInfo = new ImageInfo("Use Case Diagram Information", 
						"USE_CASE", "565px");

		// add the image icon to the panel
		horizontalPanel_3.add(useCaseInfo);

		DecoratedStackPanel colabSeqStackPanel = new DecoratedStackPanel();
		flexTable_1.setWidget(1, 0, colabSeqStackPanel);
		colabSeqStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_8 = new VerticalPanel();
		colabSeqStackPanel.add(verticalPanel_8, "18.1.2. Collaboration/Sequence Diagrams", false);
		verticalPanel_8.setSize("100%", "100%");
		
		Label lblCollaborationAndSequence = new Label("Collaboration and Sequence diagrams are used to " +
				"create scenarios that represent the execution of each use case of the analysis pattern. One per line.");
		lblCollaborationAndSequence.setStyleName("definitionLabel");
		verticalPanel_8.add(lblCollaborationAndSequence);
		lblCollaborationAndSequence.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
		verticalPanel_8.add(horizontalPanel_4);
		horizontalPanel_4.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_9 = new VerticalPanel();
		horizontalPanel_4.add(verticalPanel_9);
		
		Label lblColseqDiag = new Label("Coll. Seq. Diag.:");
		lblColseqDiag.setStyleName("textBoldLarge");
		verticalPanel_9.add(lblColseqDiag);
		lblColseqDiag.setWidth("130px");

		// create the Collaboration Sequence Diagrams label menu
		ElementLabel labelColabSeq = new ElementLabel("[M]", "[Mu]", "[U]", "57px");
		verticalPanel_9.add(labelColabSeq);

		// add the Collaboration Sequence Diagrams field to the panel
		horizontalPanel_4.add(colabSeqDiagBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo colabSeqInfo = new ImageInfo("Collaboration/Sequence Diagrams Information", 
						"COLLABORATION", "620px");

		// add the image icon to the panel
		horizontalPanel_4.add(colabSeqInfo);
		
		DecoratedStackPanel actStatStackPanel = new DecoratedStackPanel();
		flexTable_1.setWidget(2, 0, actStatStackPanel);
		actStatStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		actStatStackPanel.add(verticalPanel_1, "18.1.3. Activity/State Diagrams", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblActivityAndState = new Label("Activity and State diagrams are used to represent the " +
				"general behaviour of the system specified by the analysis pattern. One per line.");
		lblActivityAndState.setStyleName("definitionLabel");
		verticalPanel_1.add(lblActivityAndState);
		lblActivityAndState.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblActivDiag = new Label("Activ. Stat. Diag.:");
		lblActivDiag.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblActivDiag);
		lblActivDiag.setWidth("130px");

		// create the Activity State Diagrams label menu
		ElementLabel labelActState = new ElementLabel("[O]", "[Mu]", "[U]", "57px");
		verticalPanel_2.add(labelActState);

		// add the Activity State Diagrams field to the panel
		horizontalPanel.add(actStateBoxList);		
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo actStatInfo = new ImageInfo("Activity/State Diagrams Information", 
						"ACTIVITY", "595px");

		// add the image icon to the panel
		horizontalPanel.add(actStatInfo);
		
		DecoratedStackPanel structureStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(1, 0, structureStackPanel);
		structureStackPanel.setSize("920px", "100%");
		
		FlexTable flexTable_2 = new FlexTable();
		flexTable_2.setCellPadding(5);
		structureStackPanel.add(flexTable_2, "18.2. Structure", false);
		flexTable_2.setSize("100%", "100%");
		
		DecoratedStackPanel classDiagStackPanel = new DecoratedStackPanel();
		flexTable_2.setWidget(0, 0, classDiagStackPanel);
		classDiagStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		classDiagStackPanel.add(verticalPanel_3, "18.2.1. Class Diagram", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblClassDiagramIs = new Label("Class diagram is the main part of the problem solution treated by the analysis pattern. ");
		lblClassDiagramIs.setStyleName("definitionLabel");
		verticalPanel_3.add(lblClassDiagramIs);
		lblClassDiagramIs.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblClassDiagram = new Label("Class Diagram:");
		lblClassDiagram.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblClassDiagram);
		lblClassDiagram.setWidth("130px");

		// create the Class Diagram label menu
		ElementLabel labelClassDiag = new ElementLabel("[M]", "[S]", "[U]", "50px");
		verticalPanel_4.add(labelClassDiag);

		// add the Class Diagram field to the panel
		classDiagramBox.setStyleName("textBox");
		classDiagramBox.setSize("690px", "40px");
		horizontalPanel_1.add(classDiagramBox);				
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo classDiagInfo = new ImageInfo("Class Diagram Information", 
						"CLASS_DIAGRAM", "570px");

		// add the image icon to the panel
		horizontalPanel_1.add(classDiagInfo);
		
		DecoratedStackPanel classDescrStackPanel = new DecoratedStackPanel();
		flexTable_2.setWidget(1, 0, classDescrStackPanel);
		classDescrStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_5 = new VerticalPanel();
		classDescrStackPanel.add(verticalPanel_5, "18.2.2. Class Descriptions", false);
		verticalPanel_5.setSize("100%", "100%");
		
		Label lblBriefDescriptionOf = new Label("Brief description of each class of the class diagram.");
		lblBriefDescriptionOf.setStyleName("definitionLabel");
		verticalPanel_5.add(lblBriefDescriptionOf);
		lblBriefDescriptionOf.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		verticalPanel_5.add(horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_16 = new VerticalPanel();
		horizontalPanel_2.add(verticalPanel_16);
		
		Label lblClassDescription = new Label("Class Descr.:");
		lblClassDescription.setStyleName("textBoldLarge");
		verticalPanel_16.add(lblClassDescription);
		lblClassDescription.setWidth("130px");
		
		// create the Class Description label menu
		ElementLabel labelClassDescr = new ElementLabel("[M]", "[S]", "[U]", "50px");
		verticalPanel_16.add(labelClassDescr);

		// add the Class Description field to the panel
		horizontalPanel_2.add(classDescrBoxList);			
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo classDescrInfo = new ImageInfo("Class Descriptions Information", 
						"CLASS_DESCRIPTIONS", "580px");

		// add the image icon to the panel
		horizontalPanel_2.add(classDescrInfo);

		DecoratedStackPanel relatDescrStackPanel = new DecoratedStackPanel();
		flexTable_2.setWidget(2, 0, relatDescrStackPanel);
		relatDescrStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_17 = new VerticalPanel();
		relatDescrStackPanel.add(verticalPanel_17, "18.2.3. Relationship Descriptions", false);
		verticalPanel_17.setSize("100%", "100%");
		
		Label lblBriefDescriptionOf_1 = new Label("Brief description of the main relationships " +
				"present in the class diagram.. One per line.");
		lblBriefDescriptionOf_1.setStyleName("definitionLabel");
		verticalPanel_17.add(lblBriefDescriptionOf_1);
		lblBriefDescriptionOf_1.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_8 = new HorizontalPanel();
		verticalPanel_17.add(horizontalPanel_8);
		horizontalPanel_8.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_18 = new VerticalPanel();
		horizontalPanel_8.add(verticalPanel_18);
		
		Label lblRelationshipDescr = new Label("Relation. Descr.:");
		lblRelationshipDescr.setStyleName("textBoldLarge");
		verticalPanel_18.add(lblRelationshipDescr);
		lblRelationshipDescr.setWidth("130px");
		
		// create the Relationship Descriptions label menu
		ElementLabel labelRelatDescr = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_18.add(labelRelatDescr);

		// add the Relationship Descriptions field to the panel
		horizontalPanel_8.add(relatDescrBoxList);		
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo relatDescrInfo = new ImageInfo("Relationship Descriptions Information", 
						"RELATIONSHIP_DESCRIPTIONS", "580px");

		// add the image icon to the panel
		horizontalPanel_8.add(relatDescrInfo);
		
		DecoratedStackPanel solutionVarStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(2, 0, solutionVarStackPanel);
		solutionVarStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_10 = new VerticalPanel();
		solutionVarStackPanel.add(verticalPanel_10, "18.3. Solution Variants", false);
		verticalPanel_10.setSize("100%", "100%");
		
		Label lblThisElementRefinement = new Label("This element refinement presents alternative models" +
				" of behaviour and structure to solve the problem treated by the analysis pattern. One per line.");
		lblThisElementRefinement.setStyleName("definitionLabel");
		verticalPanel_10.add(lblThisElementRefinement);
		lblThisElementRefinement.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_5 = new HorizontalPanel();
		verticalPanel_10.add(horizontalPanel_5);
		horizontalPanel_5.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_11 = new VerticalPanel();
		horizontalPanel_5.add(verticalPanel_11);
		
		Label lblSolutionVar = new Label("Solution Var.:");
		lblSolutionVar.setStyleName("textBoldLarge");
		verticalPanel_11.add(lblSolutionVar);
		lblSolutionVar.setWidth("130px");

		// create the Solution Variants label menu
		ElementLabel labelSolVar = new ElementLabel("[O]", "[Mu]", "[U]", "57px");
		verticalPanel_11.add(labelSolVar);

		// add the Solution Variants field to the panel
		horizontalPanel_5.add(solutionVarBoxList);	
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo solVarInfo = new ImageInfo("Solution Variants Information", 
						"SOLUTION_VARIANTS", "635px");

		// add the image icon to the panel
		horizontalPanel_5.add(solVarInfo);
		
		this.ensureDebugId("modelingPanel");
	}

	
    /**
     * Get the TextBox with the Use Case Diagram information.
     * 
     * @return Use Case Diagram Box
     */
	public TextBox getUseCaseDiagramBox() {
		return useCaseDiagramBox;
	}
    /**
     * Get the MultipleTextField with the Collaboration/Sequence
     * Diagram information.
     * 
     * @return Collaboration/Sequence Diagram field
     */
	public MultipleTextField getColaborationSequenceDiagramBox() {
		return colabSeqDiagBoxList;
	}
    /**
     * Get the MultipleTextField with the Activity/State
     * Diagram information.
     * 
     * @return Activity/State Diagram field
     */
	public MultipleTextField getActivityStateBox() {
		return actStateBoxList;
	}
    /**
     * Get the TextBox with the Class Diagram information.
     * 
     * @return Class Diagram Box
     */
	public TextBox getClassDiagramBox() {
		return classDiagramBox;
	}
    /**
     * Get the MultipleTextField with the Class Description information.
     * 
     * @return Class Description field
     */
	public MultipleTextField getClassDescriptionBox() {
		return classDescrBoxList;
	}
    /**
     * Get the MultipleTextField with the Relationship Description information.
     * 
     * @return Relationship Description field
     */
	public MultipleTextField getRelationshipDescriptionBox() {
		return relatDescrBoxList;
	}
    /**
     * Get the MultipleTextField with the Solution Variants information.
     * 
     * @return Solution Variants field
     */
	public MultipleTextField getSolutionVariantsBox() {
		return solutionVarBoxList;
	}

	@Override
	public void clearFields() {
		useCaseDiagramBox.setText("");
		colabSeqDiagBoxList.clear();
		actStateBoxList.clear();
		classDiagramBox.setText("");
		classDescrBoxList.clear();
		relatDescrBoxList.clear();
		solutionVarBoxList.clear();
		
		useCaseDiagramBox.removeStyleName("errorTextBox");
		classDiagramBox.removeStyleName("errorTextBox");
		classDescrBoxList.removeStyleName("errorTextBox");
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		useCaseDiagramBox.setFocus(focused);
	}
}
