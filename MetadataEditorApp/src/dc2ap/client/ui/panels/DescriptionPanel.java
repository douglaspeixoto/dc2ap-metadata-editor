package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

/**
 * Implement the UI panel with the Description information.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public final class DescriptionPanel extends GenericPanel {
	private TextBox problemBox = new TextBox();
	private TextBox contextBox = new TextBox();
	private MultipleTextField motivationBoxList = new MultipleTextField(730);
	private MultipleTextField exampleBoxList = new MultipleTextField(720);
	private MultipleTextField knownUsesBoxList = new MultipleTextField(720);

	public DescriptionPanel() {
		
		ScrollPanel mainPanel = new ScrollPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("scrollPanel");
		mainPanel.setSize("985px", "600px");
		
		VerticalPanel vPanel = new VerticalPanel();
		vPanel.setStyleName("cw-DialogBox");
		vPanel.setSpacing(10);
		mainPanel.add(vPanel);
		vPanel.setSize("955px", "100%");
		
		DecoratedStackPanel descriptionStackPanel = new DecoratedStackPanel();
		vPanel.add(descriptionStackPanel);
		descriptionStackPanel.setSize("930px", "100%");
		
		FlexTable mainTable = new FlexTable();
		mainTable.setBorderWidth(0);
		mainTable.setCellPadding(5);
		descriptionStackPanel.add(mainTable, "5. Description", false);
		mainTable.setSize("100%", "100%");
	
		DecoratedStackPanel problemStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(0, 0, problemStackPanel);
		problemStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_5 = new VerticalPanel();
		problemStackPanel.add(verticalPanel_5, "5.1. Problem", false);
		verticalPanel_5.setSize("100%", "100%");

		Label lblBriefTextualDescription = new Label("Brief textual description of the problem solved by the analysis pattern.");
		lblBriefTextualDescription.setStyleName("definitionLabel");
		verticalPanel_5.add(lblBriefTextualDescription);
		lblBriefTextualDescription.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_5.add(horizontalPanel);
		horizontalPanel.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_6 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_6);
		
		Label problemLabel = new Label("Problem:");
		verticalPanel_6.add(problemLabel);
		problemLabel.setStyleName("textBoldLarge");
		problemLabel.setWidth("100px");
		
		// create the Problem label menu
		ElementLabel labelMenuProblem = new ElementLabel("[M]", "[S]", "[St]", "53px");
		verticalPanel_6.add(labelMenuProblem);
		
		problemBox.setStyleName("textBox");
		problemBox.setSize("730px", "40px");
		horizontalPanel.add(problemBox);
		
	    // Create the image icon that opens the dialog
		ImageInfo problemInfo = new ImageInfo("Problem Information", 
						"PROBLEM", "550px");

		// add the image icon to the panel
		horizontalPanel.add(problemInfo);
		
		DecoratedStackPanel motivationStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(1, 0, motivationStackPanel);
		motivationStackPanel.setSize("920px", "100%");
		
		FlexTable flexTable = new FlexTable();
		flexTable.setCellPadding(5);
		motivationStackPanel.add(flexTable, "5.2. Motivation", false);
		flexTable.setSize("100%", "100%");
		
		Label lblSpecificReasonsThat = new Label("Specific reasons that justify the existence of the documented analysis pattern. One per line.");
		lblSpecificReasonsThat.setStyleName("definitionLabel");
		flexTable.setWidget(0, 0, lblSpecificReasonsThat);
		lblSpecificReasonsThat.setSize("100%", "18px");
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		flexTable.setWidget(1, 0, horizontalPanel_3);
		horizontalPanel_3.setSize("100%", "183px");
		
		VerticalPanel verticalPanel_9 = new VerticalPanel();
		horizontalPanel_3.add(verticalPanel_9);
		
		Label motivationLabel = new Label("Motivations:");
		verticalPanel_9.add(motivationLabel);
		motivationLabel.setStyleName("textBoldLarge");
		motivationLabel.setWidth("100px");
		
		// create the Motivation label menu
		ElementLabel labelMenuMotivation = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_9.add(labelMenuMotivation);
		
		// add the Motivation field to the panel
		horizontalPanel_3.add(motivationBoxList);
		
	    // Create the image icon that opens the dialog
		ImageInfo motivationInfo = new ImageInfo("Motivation Information", 
						"MOTIVATION", "585px");

		// add the image icon to the panel
		horizontalPanel_3.add(motivationInfo);
		
		DecoratedStackPanel exampleStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(2, 0, exampleStackPanel);
		exampleStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		exampleStackPanel.add(verticalPanel, "5.2.1. Example", false);
		verticalPanel.setSize("100%", "200px");
		
		Label lblExamplesOfApplications = new Label("Examples of applications where the described " +
				"analysis pattern can be applied. One per line.");
		lblExamplesOfApplications.setStyleName("definitionLabel");
		verticalPanel.add(lblExamplesOfApplications);
		lblExamplesOfApplications.setHeight("30px");
		
		HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
		verticalPanel.add(horizontalPanel_4);
		horizontalPanel_4.setSize("100%", "183px");
		
		VerticalPanel verticalPanel_10 = new VerticalPanel();
		horizontalPanel_4.add(verticalPanel_10);
		
		Label exampleLabel = new Label("Examples:");
		exampleLabel.setStyleName("textBoldLarge");
		verticalPanel_10.add(exampleLabel);
		exampleLabel.setWidth("100px");
		
		// create the Example label menu
		ElementLabel labelMenuExample = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_10.add(labelMenuExample);
		
		// add the Example field to the panel
		horizontalPanel_4.add(exampleBoxList);
		
	    // Create the image icon that opens the dialog
		ImageInfo exampleInfo = new ImageInfo("Example Information", 
						"EXAMPLE", "585px");

		// add the image icon to the panel
		horizontalPanel_4.add(exampleInfo);
		
		DecoratedStackPanel knownUsesStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(3, 0, knownUsesStackPanel);
		knownUsesStackPanel.setSize("900px", "100%");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		knownUsesStackPanel.add(verticalPanel_2, "5.2.2. Known Uses", false);
		verticalPanel_2.setSize("100%", "200px");
		
		Label lblKnownUsesOf = new Label("Known uses of the described analysis pattern in real " +
				"systems and applications. One per line.");
		lblKnownUsesOf.setStyleName("definitionLabel");
		verticalPanel_2.add(lblKnownUsesOf);
		lblKnownUsesOf.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		verticalPanel_2.add(horizontalPanel_2);
		horizontalPanel_2.setSize("100%", "183px");
		
		VerticalPanel verticalPanel_11 = new VerticalPanel();
		horizontalPanel_2.add(verticalPanel_11);
		
		Label knownUsesLabel = new Label("Known Uses:");
		knownUsesLabel.setStyleName("textBoldLarge");
		verticalPanel_11.add(knownUsesLabel);
		knownUsesLabel.setWidth("100px");
		
		// create the Known Uses label menu
		ElementLabel labelMenuKnownUses = new ElementLabel("[O]", "[Mu]", "[St]", "60px");
		verticalPanel_11.add(labelMenuKnownUses);
		
		// add the Known Uses field to the panel
		horizontalPanel_2.add(knownUsesBoxList);
		
	    // Create the image icon that opens the dialog
		ImageInfo knownUsesInfo = new ImageInfo("Known Uses Information", 
						"KNOWN", "600px");

		// add the image icon to the panel
		horizontalPanel_2.add(knownUsesInfo);
		
		DecoratedStackPanel contextStackPanel = new DecoratedStackPanel();
		mainTable.setWidget(2, 0, contextStackPanel);
		contextStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_7 = new VerticalPanel();
		contextStackPanel.add(verticalPanel_7, "5.3. Context", false);
		verticalPanel_7.setSize("100%", "100%");
		
		Label lblDescriptionOfContextual = new Label("Description of contextual coverage " +
				"of the documented analysis pattern.");
		lblDescriptionOfContextual.setStyleName("definitionLabel");
		lblDescriptionOfContextual.setWordWrap(false);
		verticalPanel_7.add(lblDescriptionOfContextual);
		lblDescriptionOfContextual.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_6 = new HorizontalPanel();
		verticalPanel_7.add(horizontalPanel_6);
		horizontalPanel_6.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_8 = new VerticalPanel();
		horizontalPanel_6.add(verticalPanel_8);
		
		Label contextLabel = new Label("Context:");
		verticalPanel_8.add(contextLabel);
		contextLabel.setStyleName("textBoldLarge");
		contextLabel.setWidth("100px");
		
		// create the Context label menu
		ElementLabel labelMenuContext = new ElementLabel("[M]", "[S]", "[St]", "53px");
		verticalPanel_8.add(labelMenuContext);
		
		horizontalPanel_6.add(contextBox);
		contextBox.setSize("730px", "40px");
		contextBox.setStyleName("textBox");
		
	    // Create the image icon that opens the dialog
		ImageInfo contextInfo = new ImageInfo("Context Information", 
						"CONTEXT", "580px");
		
		// add the image icon to the panel
		horizontalPanel_6.add(contextInfo);
		
		this.ensureDebugId("descriptionPanel");
	}

    /**
     * Get the Text Box with the Problem information.
     * 
     * @return Problem TextBox
     */
	public TextBox getProblemBox() {
		return problemBox;
	}
    /**
     * Get the Text Box with the Context information.
     * 
     * @return Context TextBox
     */
	public TextBox getContextBox() {
		return contextBox;
	}
    /**
     * Get the MultipleTextField with the Motivation information.
     * 
     * @return Motivation field
     */
	public MultipleTextField getMotivationBox() {
		return motivationBoxList;
	}
    /**
     * Get the MultipleTextField with the Examples information.
     * 
     * @return Examples field
     */
	public MultipleTextField getExampleBox() {
		return exampleBoxList;
	}
    /**
     * Get the MultipleTextField with the Known Uses information.
     * 
     * @return Known Uses field
     */
	public MultipleTextField getKnownUsesBox() {
		return knownUsesBoxList;
	}
	
	@Override
	public void clearFields() {
		problemBox.setText("");	
		contextBox.setText("");
		motivationBoxList.clear();
		exampleBoxList.clear();
		knownUsesBoxList.clear();
		
		problemBox.removeStyleName("errorTextBox");
		contextBox.removeStyleName("errorTextBox");
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		problemBox.setFocus(focused);
	}

}
