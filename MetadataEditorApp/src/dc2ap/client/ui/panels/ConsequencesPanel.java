package dc2ap.client.ui.panels;

import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import dc2ap.client.ui.components.MultipleTextField;

/**
 * Implement the UI panel with the Consequences information.
 *
 * @author Douglas A. Peixoto - 06/2012
 */
public final class ConsequencesPanel extends GenericPanel {
	private MultipleTextField positiveBoxList = new MultipleTextField(740);
	private MultipleTextField negativeBoxList = new MultipleTextField(740);
		
	public ConsequencesPanel() {
		
		ScrollPanel mainPanel = new ScrollPanel();
		initWidget(mainPanel);
		mainPanel.setStyleName("scrollPanel");
		mainPanel.setSize("985px", "600px");
		
		VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setSpacing(10);
		mainPanel.setWidget(verticalPanel);
		verticalPanel.setSize("955px", "100%");
		
		DecoratedStackPanel consequencesStackPanel = new DecoratedStackPanel();
		verticalPanel.add(consequencesStackPanel);
		consequencesStackPanel.setSize("930px", "100%");
		
		FlexTable flexTable = new FlexTable();
		flexTable.setCellPadding(5);
		consequencesStackPanel.add(flexTable, "21. Consequences", false);
		flexTable.setSize("100%", "100%");
		
		DecoratedStackPanel positiveStackPanel = new DecoratedStackPanel();
		flexTable.setWidget(0, 0, positiveStackPanel);
		positiveStackPanel.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_1 = new VerticalPanel();
		positiveStackPanel.add(verticalPanel_1, "21.1. Positive", false);
		verticalPanel_1.setSize("100%", "100%");
		
		Label lblPositiveConsequencesOf = new Label("Positive consequences of use of " +
				"the documented analysis pattern. One per line.");
		lblPositiveConsequencesOf.setStyleName("definitionLabel");
		verticalPanel_1.add(lblPositiveConsequencesOf);
		lblPositiveConsequencesOf.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		verticalPanel_1.add(horizontalPanel);
		horizontalPanel.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		horizontalPanel.add(verticalPanel_2);
		
		Label lblPositive = new Label("Positive:");
		lblPositive.setStyleName("textBoldLarge");
		verticalPanel_2.add(lblPositive);
		lblPositive.setWidth("100px");

		// create the Positive label menu
		ElementLabel labelPositive = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_2.add(labelPositive);

		// add the Positive field to the panel
		horizontalPanel.add(positiveBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo positiveInfo = new ImageInfo("Positive Consequences Information", 
						"POSITIVE", "570px");	

		// add the image icon to the panel
		horizontalPanel.add(positiveInfo);

		DecoratedStackPanel decoratedStackPanel_2 = new DecoratedStackPanel();
		flexTable.setWidget(1, 0, decoratedStackPanel_2);
		decoratedStackPanel_2.setSize("920px", "100%");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		decoratedStackPanel_2.add(verticalPanel_3, "21.2. Negative", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblNegativeConsequencesOf = new Label("Negative consequences of use of " +
				"the documented analysis pattern. One per line.");
		lblNegativeConsequencesOf.setStyleName("definitionLabel");
		verticalPanel_3.add(lblNegativeConsequencesOf);
		lblNegativeConsequencesOf.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_4 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_4);
		
		Label lblNegative = new Label("Negative");
		lblNegative.setStyleName("textBoldLarge");
		verticalPanel_4.add(lblNegative);
		lblNegative.setWidth("100px");

		// create the Positive label menu
		ElementLabel labelNegative = new ElementLabel("[M]", "[Mu]", "[St]", "60px");
		verticalPanel_4.add(labelNegative);

		// add the Positive field to the panel
		horizontalPanel_1.add(negativeBoxList);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo negativeInfo = new ImageInfo("Negative Consequences Information", 
						"NEGATIVE", "570px");

		// add the image icon to the panel
		horizontalPanel_1.add(negativeInfo);
		
		this.ensureDebugId("consequencesPanel");
	}

    /**
     * Get the MultipleTextField with the Positive Consequences information.
     * 
     * @return Positive field
     */
	public MultipleTextField getPositiveBox() {
		return positiveBoxList;
	}
    /**
     * Get the MultipleTextField with the Negative Consequences information.
     * 
     * @return Negative field
     */
	public MultipleTextField getNegativeBox() {
		return negativeBoxList;
	}

	@Override
	public void clearFields() {
		positiveBoxList.clear();
		negativeBoxList.clear();
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		positiveBoxList.setFocus(focused, 0);		
	}

}
