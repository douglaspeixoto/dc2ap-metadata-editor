package dc2ap.client.ui.panels;

import dc2ap.client.ui.components.ElementLabel;
import dc2ap.client.ui.components.ImageInfo;
import com.google.gwt.user.client.ui.DecoratedStackPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.user.datepicker.client.DateBox.DefaultFormat;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * Implement the UI panel with the Date and Type information.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public final class DatePanel extends GenericPanel {
	private DateBox createdBox = new DateBox();
	private DateBox modifiedBox = new DateBox();
	private TextBox typeBox = new TextBox();
	private TextBox notationBox = new TextBox();
		
	public DatePanel() {
		
		VerticalPanel mainPanel = new VerticalPanel();
		initWidget(mainPanel);
		mainPanel.setSpacing(10);
		mainPanel.setSize("980px", "476px");

		DecoratedStackPanel dateStackPanel = new DecoratedStackPanel();
		mainPanel.add(dateStackPanel);
		dateStackPanel.setSize("965px", "100%");

		HorizontalPanel horizontalPanel_2 = new HorizontalPanel();
		horizontalPanel_2.setStylePrimaryName("dialogVPanel");
		dateStackPanel.add(horizontalPanel_2, "8. Date", false);
		horizontalPanel_2.setSize("100%", "100%");
		
		DecoratedStackPanel createdStackPanel = new DecoratedStackPanel();
		horizontalPanel_2.add(createdStackPanel);
		createdStackPanel.setWidth("460px");
		
		VerticalPanel vpanelCreated = new VerticalPanel();
		createdStackPanel.add(vpanelCreated, "8.1. Created", false);
		vpanelCreated.setSize("100%", "100%");
		
		Label lblNewLabel = new Label("Date of creation of the resource.");
		lblNewLabel.setStyleName("definitionLabel");
		vpanelCreated.add(lblNewLabel);
		lblNewLabel.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_3 = new HorizontalPanel();
		vpanelCreated.add(horizontalPanel_3);
		horizontalPanel_3.setSize("100%", "100%");
		
		VerticalPanel verticalPanel_6 = new VerticalPanel();
		horizontalPanel_3.add(verticalPanel_6);
		
		Label lblCreated = new Label("Created:");
		lblCreated.setStyleName("textBoldLarge");
		verticalPanel_6.add(lblCreated);
		lblCreated.setWidth("110px");
		
		// create the date created label menu
		ElementLabel labelMenuCreated = new ElementLabel("[M]", "[S]", "[D]", "50px");
		verticalPanel_6.add(labelMenuCreated);
	
		// configure and add the date created field to the panel
		createdBox.setFormat(new DefaultFormat(DateTimeFormat.getFormat("yyyy-MM-dd")));
		createdBox.setStylePrimaryName("textBox");
		createdBox.setSize("200px", "40px");
		// createdBox.setValue(new Date()); // already sets the default date (it is now in the UIService)
		horizontalPanel_3.add(createdBox);
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo createdInfo = new ImageInfo("Date Created Information", 
						"CREATED", "550px");
		
		// add the image icon to the panel
		horizontalPanel_3.add(createdInfo);
		
		DecoratedStackPanel modifiedStackPanel = new DecoratedStackPanel();
		horizontalPanel_2.add(modifiedStackPanel);
		modifiedStackPanel.setWidth("460px");
		
		VerticalPanel vpanelModified = new VerticalPanel();
		modifiedStackPanel.add(vpanelModified, "8.2. Modified", false);
		vpanelModified.setSize("100%", "100%");
		
		Label lblNewLabel_1 = new Label("Date on which the resource was changed.");
		lblNewLabel_1.setStyleName("definitionLabel");
		vpanelModified.add(lblNewLabel_1);
		lblNewLabel_1.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_4 = new HorizontalPanel();
		vpanelModified.add(horizontalPanel_4);
		horizontalPanel_4.setSize("100%", "100%");
		
		VerticalPanel verticalPanel_7 = new VerticalPanel();
		horizontalPanel_4.add(verticalPanel_7);
		
		Label lblModified = new Label("Modified:");
		lblModified.setStyleName("textBoldLarge");
		verticalPanel_7.add(lblModified);
		lblModified.setWidth("110px");

		// create the date modified label menu
		ElementLabel labelMenuModified = new ElementLabel("[Cd]", "[S]", "[D]", "53px");
		verticalPanel_7.add(labelMenuModified);
		
		// configure and add the date created field to the panel
		modifiedBox.setFormat(new DefaultFormat(DateTimeFormat.getFormat("yyyy-MM-dd")));
		modifiedBox.setStylePrimaryName("textBox");
		modifiedBox.setSize("200px", "40px");
		horizontalPanel_4.add(modifiedBox);

	    // Create the image icon that opens the Info dialog box
		ImageInfo modifiedInfo = new ImageInfo("Date Modified Information", 
						"MODIFIED", "580px");
		
		// add the image icon to the panel
		horizontalPanel_4.add(modifiedInfo);

		DecoratedStackPanel typeStackPanel = new DecoratedStackPanel();
		mainPanel.add(typeStackPanel);
		typeStackPanel.setSize("965px", "100%");
		
		VerticalPanel verticalPanel_2 = new VerticalPanel();
		typeStackPanel.add(verticalPanel_2, "9. Type", false);
		verticalPanel_2.setSize("100%", "100%");
		
		Label lblWhatIsThe_1 = new Label("The nature or genre of the resource.");
		lblWhatIsThe_1.setStyleName("definitionLabel");
		verticalPanel_2.add(lblWhatIsThe_1);
		verticalPanel_2.setCellVerticalAlignment(lblWhatIsThe_1, HasVerticalAlignment.ALIGN_BOTTOM);
		lblWhatIsThe_1.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		verticalPanel_2.add(horizontalPanel_1);
		horizontalPanel_1.setSize("100%", "70px");
		
		VerticalPanel verticalPanel_5 = new VerticalPanel();
		horizontalPanel_1.add(verticalPanel_5);
		
		Label lblType = new Label("Type:");
		verticalPanel_5.add(lblType);
		lblType.setStyleName("textBoldLarge");
		lblType.setWidth("100px");

		// create the Type label menu
		ElementLabel labelMenuType = new ElementLabel("[M]", "[S]", "[US]", "57px");
		verticalPanel_5.add(labelMenuType);
		
		// add the Type field to the panel
		typeBox.setStyleName("textBox");
		typeBox.setSize("770px", "40px");
		// typeBox.setText("Analysis Pattern");  // default (it is now in the UIService)
		horizontalPanel_1.add(typeBox);
		
	    // Create the image icon that opens the dialog
		ImageInfo typeInfo = new ImageInfo("Type Information", 
						"TYPE", "585px");
		
		// add the image icon to the panel
		horizontalPanel_1.add(typeInfo);
		
		DecoratedStackPanel notationStackPanel = new DecoratedStackPanel();
		verticalPanel_2.add(notationStackPanel);
		notationStackPanel.setSize("950px", "100%");
		
		VerticalPanel verticalPanel_3 = new VerticalPanel();
		notationStackPanel.add(verticalPanel_3, "9.1. Notation", false);
		verticalPanel_3.setSize("100%", "100%");
		
		Label lblDoesTehResource = new Label("A set of characters and/or symbols used to express the content of a resource.");
		lblDoesTehResource.setStyleName("definitionLabel");
		verticalPanel_3.add(lblDoesTehResource);
		verticalPanel_2.setCellVerticalAlignment(lblDoesTehResource, HasVerticalAlignment.ALIGN_BOTTOM);
		lblDoesTehResource.setSize("100%", "30px");
		
		HorizontalPanel horizontalPanel_5 = new HorizontalPanel();
		verticalPanel_3.add(horizontalPanel_5);
		horizontalPanel_5.setSize("100%", "100%");
		
		VerticalPanel verticalPanel_8 = new VerticalPanel();
		horizontalPanel_5.add(verticalPanel_8);
		
		Label lblNotation = new Label("Notation:");
		verticalPanel_8.add(lblNotation);
		lblNotation.setStyleName("textBoldLarge");
		lblNotation.setWidth("95px");

		// create the alternative title label menu
		ElementLabel labelMenuNotation = new ElementLabel("[M]", "[S]", "[St]", "53px");
		verticalPanel_8.add(labelMenuNotation);
		
		// add the Notation field to the panel
		horizontalPanel_5.add(notationBox);
		notationBox.setStyleName("textBox");
		notationBox.setSize("770px", "40px");
		
	    // Create the image icon that opens the Info dialog box
		ImageInfo notationInfo = new ImageInfo("Notation Information", 
						"NOTATION", "555px");
		
		// add the image icon to the panel
		horizontalPanel_5.add(notationInfo);
		
		this.ensureDebugId("datePanel");
	}
	
    /**
     * Get the DateBox with the Creation Date information.
     * 
     * @return Date Created Box
     */
	public DateBox getCreatedBox() {
		return createdBox;
	}
    /**
     * Get the DateBox with the Modification Date information.
     * 
     * @return Date modification Box
     */
	public DateBox getModifiedBox() {
		return modifiedBox;
	}
    /**
     * Get the TextBox with the Type information.
     * 
     * @return Type Box
     */
	public TextBox getTypeBox() {
		return typeBox;
	}
    /**
     * Get the TextBox with the Notation information.
     * 
     * @return Notation Box
     */
	public TextBox getNotationBox() {
		return notationBox;
	}
	
	@Override
	public void clearFields() {
		createdBox.setValue(null);	
		modifiedBox.setValue(null);
		typeBox.setText("");
		notationBox.setText("");
		
		createdBox.removeStyleName("errorTextBox");
		modifiedBox.removeStyleName("warningTextBox");
		typeBox.removeStyleName("errorTextBox");
		notationBox.removeStyleName("errorTextBox");
	}

	@Override
	public void setMainFieldFocus(boolean focused) {
		createdBox.setFocus(focused);
	}

}
