package dc2ap.client.ui.components;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import dc2ap.client.ui.panels.GenericPanel;

/**
 * A DockLayoutPanel that fits the elements panel
 * in the main page. This is the center panel 
 * of the Patterportal.
 *  
 * @author Douglas A. Peixoto - 06/2012
 */
public class CenterPanel extends Composite {
	private static DockLayoutPanel centerPanel = new DockLayoutPanel(Unit.EM);
//private static TabLayoutPanel pan = new TabLayoutPanel(0.0, Unit.PX);//, barUnit)
	
	// instances of the panels
/*	private static IdentifierPanel idPanel = PanelService.getIdentifierPanel();
	private static CreatorPanel creatorPanel = PanelService.getCreatorPanel();
	private static DescriptionPanel descrPanel = PanelService.getDescriptionPanel();
	private static PublisherPanel publisherPanel = PanelService.getPublisherPanel(); 
	private static DatePanel datePanel = PanelService.getDatePanel();
	private static FormatPanel formatPanel = PanelService.getFormatPanel(); 
	private static RelationPanel relationPanel = PanelService.getRelationPanel();
	private static CoveragePanel coveragePanel = PanelService.getCoveragePanel();
	private static HistoryPanel historyPanel = PanelService.getHistoryPanel();
	private static RequirementsPanel reqPanel = PanelService.getRequirementsPanel();
	private static ModelingPanel modelPanel = PanelService.getModelingPanel();
	private static ResultingPanel resultingPanel = PanelService.getResultingPanel();
	private static ConsequencesPanel conseqPanel = PanelService.getConsequencesPanel();*/
	
	public CenterPanel() {
		/*pan.setStyleName("tabLayoutPanel");
		pan.add(idPanel);
		pan.add(creatorPanel);
		pan.add(descrPanel);
		pan.add(publisherPanel);
		pan.add(datePanel);
		pan.add(formatPanel);
		pan.add(relationPanel);
		pan.add(coveragePanel);
		pan.add(historyPanel);
		pan.add(reqPanel);
		pan.add(modelPanel);
		pan.add(resultingPanel);
		pan.add(conseqPanel);
		pan.setAnimationDuration(100);*/
		initWidget(centerPanel);
		
		this.ensureDebugId("centerPanel");
		/*initWidget(pan);
		pan.setSize("980px", "600px");*/
	}

	/**
	 * Fit the current panel to the center panel.
	 * 
	 * @param centerPanel The GenericPanel to be set.
	 */
	public static void setPanel(GenericPanel currentPanel) {
		centerPanel.clear();	
		centerPanel.add(currentPanel);
		
		//pan.selectTab(currentPanel);
	}

}
