package dc2ap.client.ui.components;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;

/**
 * This composite implements the Image that appears in front of
 * each element field, showing its information.
 * 
 * @author Douglas A. Peixoto - 06/2012
 */
public class ImageInfo extends Composite {
	/**
	 * Composite constructor.
	 * 
	 * @param popup Label Label of the pop up menu
	 * @param URLName Name of the element (in the URL)
	 * @param height Height of the pop up window
	 */
	public ImageInfo(final String popupLabel, final String URLName,
			final String height) {
	    // Create the image icon that opens the dialog
		Image image = new Image("images/info.png");
		image.setTitle("Element Info");
		image.setStyleName("imageLink");
		image.setSize("48", "48");
		initWidget(image);
		
		// create the dialog box to fit the info
		final InfoDialogBox infoDialogBox = 
			new InfoDialogBox(popupLabel, URLName, height);
		
		// show pop up event
		image.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				infoDialogBox.show();
			}
		});
	
		this.ensureDebugId("imageInfo");
	}

}
