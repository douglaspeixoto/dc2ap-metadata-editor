package dc2ap.client.ui.components;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Dialog box with the errors and warnings messages
 * found in the metadata validation.
 * 
 * @author Douglas A. Peixoto - 07/2012
 */
public class ValidationDialogBox extends DialogBox {

	/**
	 * Create a dialog box and set the caption text
	 * 
	 * @param errorMessage an error message to be show in the DialogBox
	 * @param errorLog The error log string
	 * @param warningLog The warning log string
	 * @param validated If the metadata validation is OK
	 */
	public ValidationDialogBox(String errorMessage, Integer numErrors,  
			Integer numWrns, String errorLog, String warningLog) {
		setGlassEnabled(true);
		
		setHTML("Validation Message");

		VerticalPanel dialogContents = new VerticalPanel();
		dialogContents.setSpacing(5);
		dialogContents.setSize("550px", "100%");
		setWidget(dialogContents);
		
		// panel to fit the header message
		HorizontalPanel headerPanel = new HorizontalPanel();
		headerPanel.setSpacing(5);
		
		HTML mainlabel = new HTML("Metadata validation OK!");	
		mainlabel.setStyleName("textBoldSmallBlue");
		mainlabel.setHeight("30px");
		
		// content labels
		HTML numErrosLabel = new HTML(numErrors + " errors found.<br/>");
		numErrosLabel.setStyleName("errorLabel");
		
		HTML numWrnsLabel = new HTML(numWrns + " warning messages.<br/>");
		numWrnsLabel.setStyleName("warningLabel");
		
		HTML errorsList = new HTML(errorLog + "<br/>");
		errorsList.setStyleName("errorLabel");
		
		HTML warningsList = new HTML(warningLog);
		warningsList.setStyleName("warningLabel");
		
		DisclosurePanel errorDisclosurePanel = new DisclosurePanel("Error Log");
		errorDisclosurePanel.setSize("530px", "40px");

		DisclosurePanel warningDisclosurePanel = new DisclosurePanel("Warning Messages");
		warningDisclosurePanel.setSize("530px", "30px");

		// add confirm or error icon
		Image headerImg;
		if(numErrors > 0){
			headerImg = new Image("images/error.gif");
			// set the error message in the case of non-validation of the metadata
			mainlabel.setText(errorMessage);
		} else {
			headerImg = new Image("images/confirm.gif");
		}
		headerPanel.add(headerImg);
		headerPanel.add(mainlabel);
		headerPanel.setCellVerticalAlignment(mainlabel, HasVerticalAlignment.ALIGN_BOTTOM);
		
		// add the header message to the main panel
		dialogContents.add(headerPanel);
		
		// add the error and warn widgets
		// add the messages to the disclosure panels
		VerticalPanel errorContent = new VerticalPanel();
		errorContent.setSize("100%", "100%");
		errorContent.add(numErrosLabel);
		errorContent.add(numWrnsLabel);
		errorContent.add(errorsList);
		
		warningDisclosurePanel.add(warningsList);
		if(numWrns > 0){
			errorContent.add(warningDisclosurePanel);
		}
		errorDisclosurePanel.setContent(errorContent);
					
		// let the error panel opened
		errorDisclosurePanel.setOpen(true);
		
		// add to the main panel
		dialogContents.add(errorDisclosurePanel);
	    
	    HorizontalPanel horizontalPanel = new HorizontalPanel();
	    horizontalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
	    horizontalPanel.setStyleName("backgroundDarkTop");
	    horizontalPanel.setSpacing(5);
	    dialogContents.add(horizontalPanel);
	    horizontalPanel.setSize("100%", "50px");
	    
		// Add a close button at the bottom of the dialog
	    Button closeButton = new Button(
	        "Close", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	  hide();
	          }
	    });
	    horizontalPanel.add(closeButton);
	    closeButton.setStyleName("button");
	    closeButton.setTitle("Close Dialog Box");
	    
	    ensureDebugId("validationDialogBox");
	}

}
