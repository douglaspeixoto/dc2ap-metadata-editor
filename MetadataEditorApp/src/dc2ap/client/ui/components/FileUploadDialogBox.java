package dc2ap.client.ui.components;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitEvent;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import dc2ap.client.services.DC2APService;

/**
 * Dialog box used to open the RDF file selected by 
 * the user to be opened.
 * 
 * @author Douglas A. Peixoto - 09/2012
 */
public class FileUploadDialogBox extends DialogBox {
	// servlet path name
	private static final String UPLOAD_ACTION_URL = GWT.getModuleBaseURL() + "uploadFile"; //"fileService";
	
	// Create a FormPanel to point it at a service.
	private final FormPanel form = new FormPanel();
	
	// the name and extension of the file opened
	private static String fullFileName = "";
	private static String fileExtension = "";
	
	public FileUploadDialogBox() {
		setGlassEnabled(true);
		setHTML("Open File");
		
        // point the FormPanel at a service.
		form.setAction(UPLOAD_ACTION_URL);
		// Because we're going to add a FileUpload widget, we'll need to set the
		// form to use the POST method, and multipart MIME encoding.
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);
		
		setWidget(form);
		
		VerticalPanel dialogContents = new VerticalPanel();
		dialogContents.setSize("400px", "73px");

		form.setWidget(dialogContents);

		HTML htmlSelectFile = new HTML("Select a RDF file to open:");
		htmlSelectFile.setStyleName("textBoldSmallBlue");
		htmlSelectFile.setHeight("30px");
		dialogContents.add(htmlSelectFile);
		
		final FileUpload fileUpload = new FileUpload();
		fileUpload.setName("uploadFormElement");
		fileUpload.setSize("100%", "50px");
		
		dialogContents.add(fileUpload);
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_BOTTOM);
		horizontalPanel.setStyleName("backgroundDarkTop");
		horizontalPanel.setSpacing(5);
		horizontalPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		dialogContents.add(horizontalPanel);
		horizontalPanel.setSize("100%", "50px");
		
		HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
		horizontalPanel.add(horizontalPanel_1);
		horizontalPanel_1.setWidth("200px");
		
		// Add open file command
		Button btnOpen = new Button("Open");
		btnOpen.setStyleName("button");
		horizontalPanel_1.add(btnOpen);
		btnOpen.setTitle("Open Selected File");
		btnOpen.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// get the file name and extension
		        fullFileName = fileUpload.getFilename();
		        int index = fullFileName.lastIndexOf(".");
		        fileExtension = fullFileName.substring(index);

				// open only .RDF files
				if(".rdf".equals(fileExtension)){
					form.submit();
				} else{
					Window.alert("Please, choose a RDF file.");
				}
			}
		});	
		
		// Add close box command
		Button btnClose = new Button("Close");
		btnClose.setStyleName("button");
		horizontalPanel_1.add(btnClose);
		btnClose.setTitle("Close Dialog Box");
		btnClose.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				hide();				
			}
		});
		
		// Add an submit event handler to the form.
		form.addSubmitHandler(new FormPanel.SubmitHandler() {
			@Override
			public void onSubmit(SubmitEvent event) {
				// This event is fired just before the form is submitted. 
				String filename = fileUpload.getFilename();
		        if (filename.length() == 0) {
		        	Window.alert("You must select a file to open!");
		        	event.cancel();
		        }
			}
		});

		// Add a complete handler to the form.
		// Event launched when the file is successfully submitted to the server
		form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(final SubmitCompleteEvent event) {
				// When the form submission is successfully completed, this
				// event is fired. Assuming the service returned a response of type
				// text/html, we can get the result text here (see the FormPanel
				// documentation for further explanation).
				
				// Get the file name created in the server
				final String result = event.getResults();
				int beginIndex = result.indexOf("uploadedFile");
				int endIndex = result.lastIndexOf("rdf");
				fullFileName = result.substring(beginIndex, endIndex+3);

				// if it is an error message
				if(fullFileName.length() > 50){
					Window.alert("SERVER ERROR MESSAGE: " + result);
					System.out.println("SERVER ERROR MESSAGE: " + result);
				}
				
				// Read the file information from the server
				DC2APService.readFileOpened(fullFileName);
				
				// Close the dialog box after read the file
				hide();
			}
		});
		
		this.ensureDebugId("fileUploadDialogBox");
	}
	
}
