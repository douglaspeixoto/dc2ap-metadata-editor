package dc2ap.client.ui.components;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;

import dc2ap.client.services.DC2APService;
import dc2ap.client.services.UIService;

/**
 * Class contains the UI buttons of the page and its events.
 * Save, Validate, Generate RDF, Clear.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public class ButtonPanel extends Composite {
	private Button buttonOpen = new Button("Open");
	private Button buttonValidate = new Button("Validate");
	private Button buttonRDF = new Button("RDF");
	private Button buttonClear = new Button("Clear");
	private Button buttonClearAll = new Button("Clear All");

	public ButtonPanel() {
		
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		horizontalPanel.setSize("980px", "50px");

		HorizontalPanel leftButtonsPanel = new HorizontalPanel();
		leftButtonsPanel.setSpacing(5);
		leftButtonsPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		
		horizontalPanel.add(leftButtonsPanel);
		leftButtonsPanel.setWidth("391px");
		
		leftButtonsPanel.add(buttonOpen);
		buttonOpen.setStyleName("button");
		buttonOpen.setSize("110px", "30px");
		buttonOpen.setTitle("Open RDF File");
		
		leftButtonsPanel.add(buttonClear);
		buttonClear.setStyleName("button");
		buttonClear.setSize("110px", "30px");
		buttonClear.setTitle("Clear Panel Fields");
		
		leftButtonsPanel.add(buttonClearAll);
		buttonClearAll.setTitle("Clear Fields of All Panels");
		buttonClearAll.setStyleName("button");
		buttonClearAll.setSize("120px", "30px");
		
		// add the event "save" to the button
		buttonOpen.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//DC2APService.saveProject();
				DC2APService.openFileRDF();
			}
		});
		
		// add the event "clear all fields" to the button
		buttonClearAll.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				UIService.clearAllFields(true);
			}
		});
		
		// add the event "clear" to the button
		buttonClear.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				UIService.clearCurrentPanel();				
			}
		});
		
		HorizontalPanel rightButtonsPanel = new HorizontalPanel();
		rightButtonsPanel.setSpacing(5);
		rightButtonsPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
		
		horizontalPanel.add(rightButtonsPanel);
		horizontalPanel.setCellHorizontalAlignment(rightButtonsPanel, HasHorizontalAlignment.ALIGN_RIGHT);
		rightButtonsPanel.setWidth("250px");
		rightButtonsPanel.add(buttonValidate);
		
		buttonValidate.setStyleName("button");
		buttonValidate.setSize("110px", "30px");
		buttonValidate.setTitle("Validate Metadata");
		rightButtonsPanel.add(buttonRDF);
		
		buttonRDF.setStyleName("button");
		buttonRDF.setSize("110px", "30px");
		buttonRDF.setTitle("Generate RDF Document");
		
		// add the event "validate metadata" to the button
		buttonValidate.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String errorMsg = "Metadata Validation Error! See error log for more details.";
				DC2APService.validateMetadata(errorMsg);
			}
		});
		
		// add the event "generate RDF document" to the button
		buttonRDF.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				DC2APService.generateRDF();
			}
		});
		
		this.ensureDebugId("buttonPanel");
		initWidget(horizontalPanel);
	}
}
