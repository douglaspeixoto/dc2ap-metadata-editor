package dc2ap.client.ui.components;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.MenuItemSeparator;
import dc2ap.client.Patternportal;
import dc2ap.client.services.DC2APService;
import dc2ap.client.services.UIService;

/**
 * This UI class implements the Menu of the main page.
 * 
 * @author Douglas A. Peixoto - 06/2012
 */
public class MenuPanel extends Composite {

	public MenuPanel() {
		VerticalPanel menuPanel = new VerticalPanel();
		initWidget(menuPanel);
		menuPanel.setSize("100%", "100%");
		
		MenuBar menuBar = new MenuBar(false);
		menuPanel.add(menuBar);
		menuBar.setSize("100%", "30px");
		
		MenuItem mntmHome = new MenuItem("Home", false, new Command() {			
			@Override
			public void execute() {
				if(Window.confirm("Are you sure you want to quit? Your progress will be lost.")){
					Patternportal.backToHomePage();	
				}
			}
		});
		
		menuBar.addItem(mntmHome);
		
		MenuItemSeparator separator = new MenuItemSeparator();
		menuBar.addSeparator(separator);
		MenuBar menuBar_1 = new MenuBar(true);
		
		MenuItem mntmFile = new MenuItem("File", false, menuBar_1);
		MenuBar menuBar_14 = new MenuBar(true);
		
		MenuItem mntmOpen = new MenuItem("Open", false, menuBar_14);
		menuBar_1.addItem(mntmOpen);
		
		// add the open RDF file command
		MenuItem mntmRdfFile = new MenuItem("RDF File", false, new Command() {			
			@Override
			public void execute() {
				DC2APService.openFileRDF();
			}
		});
		
		menuBar_14.addItem(mntmRdfFile);
		
		// add the save command
		MenuItem mntmSave = new MenuItem("Save", false, new Command() {			
			@Override
			public void execute() {
				//DC2APService.saveProject();
				DC2APService.sorryMessage();
			}
		});
		
		menuBar_1.addItem(mntmSave);
		
		MenuItemSeparator separator_3 = new MenuItemSeparator();
		menuBar_1.addSeparator(separator_3);
		
		// add the clear current panel command
		MenuItem mntmClear = new MenuItem("Clear", false, new Command() {			
			@Override
			public void execute() {
				UIService.clearCurrentPanel();
			}
		});
		
		menuBar_1.addItem(mntmClear);
		
		// add the clear all fields command
		MenuItem mntmClearAll = new MenuItem("Clear All", false, new Command() {			
			@Override
			public void execute() {
				UIService.clearAllFields(true);
			}
		});
		
		menuBar_1.addItem(mntmClearAll);
		menuBar.addItem(mntmFile);
		MenuBar menuBar_3 = new MenuBar(true);
		
		MenuItem mntmEdit = new MenuItem("Services", false, menuBar_3);
		
		// Add the validate command to the menu
		MenuItem mntmValidate = new MenuItem("Validate", false, new Command() {			
			@Override
			public void execute() {
				String errorMsg = "Metadata Validation Error! See error log for more details.";
				DC2APService.validateMetadata(errorMsg);
			}
		});
		
		menuBar_3.addItem(mntmValidate);
		
		// Add the Generate RDF command to the menu
		MenuItem mntmGenerateRdf = new MenuItem("Generate RDF", false, new Command() {			
			@Override
			public void execute() {
				DC2APService.generateRDF();
			}
		});
		
		menuBar_3.addItem(mntmGenerateRdf);
		menuBar.addItem(mntmEdit);
		
		MenuItemSeparator separator_1 = new MenuItemSeparator();
		menuBar.addSeparator(separator_1);
		MenuBar menuBar_2 = new MenuBar(true);
		
		MenuItem mntmHelp = new MenuItem("Help", false, menuBar_2);
		
		// add open help command
		MenuItem mntmEditorHelp = new MenuItem("Medatada Editor Help", false, new Command() {			
			@Override
			public void execute() {
				DC2APService.sorryMessage();
			}
		});
		
		menuBar_2.addItem(mntmEditorHelp);
		
		MenuItemSeparator separator_2 = new MenuItemSeparator();
		menuBar_2.addSeparator(separator_2);
		MenuBar menuBar_4 = new MenuBar(true);
		
		MenuItem mntmElementsInfo = new MenuItem("Elements Information", false, menuBar_4);

	    // show Identifier Info pop up event
	    MenuItem mntmIdentifier = new MenuItem("1. Identifier", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Identifier Information", 
			    		"INDENTIFIER", "580px");
			}
		});
		
		menuBar_4.addItem(mntmIdentifier);
		MenuBar menuBar_5 = new MenuBar(true);
		
		MenuItem mntmTitle = new MenuItem("2. Title", false, menuBar_5);

	    // show Title Info pop up event
	    MenuItem mntmTitle1 = new MenuItem("2. Title", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Title Information", 
			    		"TITLE", "560px");
			}
		});

		menuBar_5.addItem(mntmTitle1);

	    // show Alternative Title Info pop up event
		MenuItem mntmAltTitle = new MenuItem("2.1. Alternative Title", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Alternative Title Information", 
			    		"ALTERNATIVE", "590px");
			}
		});
		
		menuBar_5.addItem(mntmAltTitle);
		menuBar_4.addItem(mntmTitle);
	    
	    // show Creator pop up event
		MenuItem mntmCreator = new MenuItem("3. Creator", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Creator Information", 
			    		"CREATOR", "555px");
			}
		});		
		
		menuBar_4.addItem(mntmCreator);

	    // show Subject Info pop up event
		MenuItem mntmSubject = new MenuItem("4. Subject", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Subject Information", 
						"SUBJECT", "535px");
			}
		});	
		
		menuBar_4.addItem(mntmSubject);
		MenuBar menuBar_6 = new MenuBar(true);
		
		MenuItem mntmDescription = new MenuItem("5. Description", false, menuBar_6);
		
		// show Description Info pop up event
		MenuItem mntmDescription_1 = new MenuItem("5. Description", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Description Information", 
						"DESCRIPTION", "520px");
			}
		});		
		
		menuBar_6.addItem(mntmDescription_1);
	    
	    // show Problem Info pop up event
		MenuItem mntmProblem = new MenuItem("5.1. Problem", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Problem Information", 
						"PROBLEM", "550px");
			}
		});		
		
		menuBar_6.addItem(mntmProblem);

	    // show Motivation Info pop up event
		MenuItem mntmMotivation = new MenuItem("5.2. Motivation", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Motivation Information", 
						"MOTIVATION", "585px");
			}
		});		
		
		menuBar_6.addItem(mntmMotivation);
	    
	    // show Example Info pop up event
		MenuItem mntmExample = new MenuItem("5.2.1. Example", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Example Information", 
						"EXAMPLE", "585px");
			}
		});
		
		menuBar_6.addItem(mntmExample);
	    
	    // show Known Uses Info pop up event
		MenuItem mntmKnownUses = new MenuItem("5.2.2. Known Uses", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Known Uses Information", 
						"KNOWN", "600px");
			}
		});		
		
		menuBar_6.addItem(mntmKnownUses);

	    // show Context Info pop up event
		MenuItem mntmContext = new MenuItem("5.3. Context", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Context Information", 
						"CONTEXT", "580px");
			}
		});

		menuBar_6.addItem(mntmContext);
		menuBar_4.addItem(mntmDescription);

	    // show Publisher Info pop up event
		MenuItem mntmPublisher = new MenuItem("6. Publisher", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Publisher Information", 
						"PUBLISHER", "560px");
			}
		});	
		
		menuBar_4.addItem(mntmPublisher);
		
		// show Contributor Info pop up event
		MenuItem mntmContributor = new MenuItem("7. Contributor", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Contributor Information", 
						"CONTRIBUTOR", "585px");
			}
		});
		
		menuBar_4.addItem(mntmContributor);
		
		MenuBar menuBar_7 = new MenuBar(true);
		
		MenuItem mntmDate = new MenuItem("8. Date", false, menuBar_7);
		
		// show Date Info pop up event
		MenuItem mntmDate_1 = new MenuItem("8. Date", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Date Information", 
						"DATE", "520px");
			}
		});
		
		menuBar_7.addItem(mntmDate_1);
		
		// show Created Info pop up event
		MenuItem mntmCreated = new MenuItem("8.1. Created", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Date Created Information", 
						"CREATED", "550px");
			}
		});
		
		menuBar_7.addItem(mntmCreated);
		
		// show Modified Info pop up event
		MenuItem mntmModified = new MenuItem("8.2. Modified", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Date Modified Information", 
						"MODIFIED", "580px");
			}
		});
		
		menuBar_7.addItem(mntmModified);
		
		menuBar_4.addItem(mntmDate);
		MenuBar menuBar_8 = new MenuBar(true);
		
		MenuItem mntmType_1 = new MenuItem("9. Type", false, menuBar_8);
		
		// show Type Info pop up event
		MenuItem mntmType = new MenuItem("9. Type", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Type Information", 
						"TYPE", "585px");
			}
		});
			
		menuBar_8.addItem(mntmType);
		
		// show Notation Info pop up event
		MenuItem mntmNotation = new MenuItem("9.1. Notation", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Notation Information", 
						"NOTATION", "550px");
			}
		});
		
		menuBar_8.addItem(mntmNotation);
		menuBar_4.addItem(mntmType_1);
		
		// show Format Info pop up event
		MenuItem mntmFormat = new MenuItem("10. Format", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Format Information", 
						"FORMAT", "625px");
			}
		});
		
		menuBar_4.addItem(mntmFormat);
		
		// show Source Info pop up event
		MenuItem mntmSource = new MenuItem("11. Source", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Source Information", 
						"SOURCE", "555px");
			}
		});
		
		menuBar_4.addItem(mntmSource);
		
		// show Language Info pop up event
		MenuItem mntmLanguage = new MenuItem("12. Language", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Language Information", 
						"LANGUAGE", "575px");
			}
		});
		
		menuBar_4.addItem(mntmLanguage);
		MenuBar menuBar_9 = new MenuBar(true);
		
		MenuItem mntmRelation = new MenuItem("13. Relation", false, menuBar_9);
		
		// show Relation Info pop up event
		MenuItem mntmRelation_1 = new MenuItem("13. Relation", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Relation Information", 
						"RELATION", "540px");
			}
		});
		
		menuBar_9.addItem(mntmRelation_1);
		
		// show Is version Of Info pop up event
		MenuItem mntmIsVersionOf = new MenuItem("13.1. Is Version Of", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Is Version Of Information", 
						"IS_VERSION_OF", "550px");
			}
		});
		
		menuBar_9.addItem(mntmIsVersionOf);
		
		// show Is Replaced By Info pop up event
		MenuItem mntmIsReplacedBy = new MenuItem("13.2. Is Replaced By", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Is Replaced By Information", 
						"IS_REPLACED_BY", "560px");
			}
		});
		
		menuBar_9.addItem(mntmIsReplacedBy);
		
		// show Replaces Info pop up event
		MenuItem mntmReplaces = new MenuItem("13.3. Replaces", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Replaces Information", 
						"REPLACES", "560px");
			}
		});
		
		menuBar_9.addItem(mntmReplaces);
		
		// show Is Part Of Info pop up event
		MenuItem mntmIsPartOf = new MenuItem("13.4. Is Part Of", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Is Part Of Information", 
						"IS_PART_OF", "590px");
			}
		});
		
		menuBar_9.addItem(mntmIsPartOf);
		
		// show Has part Of Info pop up event
		MenuItem mntmHasPart = new MenuItem("13.5. Has Part", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Has Part Information", 
						"HAS_PART", "580px");
			}
		});
		
		menuBar_9.addItem(mntmHasPart);
		
		// show Is Designed With Info pop up event
		MenuItem mntmIsDesignedWith = new MenuItem("13.6. Is Designed With", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Is Designed With Information", 
						"IS_DESIGNED_WITH", "600px");
			}
		});
		
		menuBar_9.addItem(mntmIsDesignedWith);
		
		// show Should Avoid Info pop up event
		MenuItem mntmShouldAvoid = new MenuItem("13.7. Should Avoid", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Should Avoid Information", 
						"SHOULD_AVOID", "595px");
			}
		});
		
		menuBar_9.addItem(mntmShouldAvoid);
		
		// show Complemented By Info pop up event
		MenuItem mntmComplementedBy = new MenuItem("13.8. Complemented By", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Complemented By Information", 
						"COMPLEMENTED_BY", "615px");
			}
		});
		
		menuBar_9.addItem(mntmComplementedBy);
		
		// show About Info pop up event
		MenuItem mntmAbout = new MenuItem("13.9. About", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("About Information", 
						"ABOUT", "560px");
			}
		});
		
		menuBar_9.addItem(mntmAbout);
		
		menuBar_4.addItem(mntmRelation);
		
		// show Coverage Info pop up event
		MenuItem mntmCoverage = new MenuItem("14. Coverage", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Coverage Information", 
						"COVERAGE", "600px");
			}
		});
		
		menuBar_4.addItem(mntmCoverage);
		
		// show Rights Info pop up event
		MenuItem mntmRights = new MenuItem("15. Rights", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Rights Information", 
						"RIGHTS", "560px");
			}
		});
		
		menuBar_4.addItem(mntmRights);
		MenuBar menuBar_10 = new MenuBar(true);
		
		MenuItem mntmHistory = new MenuItem("16. History", false, menuBar_10);
		
		// show History Info pop up event
		MenuItem mntmHistory_1 = new MenuItem("16. History", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("History Information", 
						"HISTORY", "580px");
			}
		});
		
		menuBar_10.addItem(mntmHistory_1);
		
		// show Event Date Info pop up event
		MenuItem mntmEventDate = new MenuItem("16.1. Event Date", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Event Date Information", 
						"EVENT_DATE", "535px");
			}
		});
		
		menuBar_10.addItem(mntmEventDate);
		
		// show Author Info pop up event
		MenuItem mntmAuthor = new MenuItem("16.2. Author", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Author Information", 
						"AUTHOR", "570px");
			}
		});
		
		menuBar_10.addItem(mntmAuthor);
		
		// show Reason Info pop up event
		MenuItem mntmReason = new MenuItem("16.3. Reason", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Reason Information", 
						"REASON", "555px");
			}
		});
		
		menuBar_10.addItem(mntmReason);
		
		// show Changes Info pop up event
		MenuItem mntmChanges = new MenuItem("16.4. Changes", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Changes Information", 
						"CHANGES", "570px");
			}
		});
		
		menuBar_10.addItem(mntmChanges);
		menuBar_4.addItem(mntmHistory);
		MenuBar menuBar_11 = new MenuBar(true);
		
		MenuItem mntmRequirements = new MenuItem("17. Requirements", false, menuBar_11);
		
		MenuItem mntmRequirements_1 = new MenuItem("17. Requirements", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Requirements Information", 
						"REQUIREMENTS", "540px");
			}
		});
		
		menuBar_11.addItem(mntmRequirements_1);
		
		// show Functional Requirements Info pop up event
		MenuItem mntmFunctionalRequirements = new MenuItem("17.1. Functional Requirements", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Functional Requirements Information", 
						"FUNCTIONAL", "570px");
			}
		});
		
		menuBar_11.addItem(mntmFunctionalRequirements);
		
		// show Non-functional Requirements Info pop up event
		MenuItem mntmNonfunctionalReq = new MenuItem("17.2. Non-functional Requirements", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Non-Functional Information", 
						"NON_FUNCTIONAL", "580px");
			}
		});
		
		menuBar_11.addItem(mntmNonfunctionalReq);
		
		// show Dependencies and Contributions Info pop up event
		MenuItem mntmDependenciesAndContributions = new MenuItem("17.3. Dependencies and Contributions", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Dependencies and Contributions Information", 
						"DEPENDENCIES", "570px");
			}
		});
		
		menuBar_11.addItem(mntmDependenciesAndContributions);
		
		// show Dependency Graph Info pop up event
		MenuItem mntmDependencyGraph = new MenuItem("17.3.1. Dependency Graph", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Dependency Graph Information", 
						"DEP_GRAPH", "570px");
			}
		});
		
		menuBar_11.addItem(mntmDependencyGraph);
		
		// show Contribution Graph Info pop up event
		MenuItem mntmContributionGraph = new MenuItem("17.3.2. Contribution Graph", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Contribution Graph Information", 
						"CONTRI_GRAPH", "600px");
			}
		});
		
		menuBar_11.addItem(mntmContributionGraph);
		
		// show Conflict and Resolution Info pop up event
		MenuItem mntmConflictAndResolution = new MenuItem("17.4. Conflict and Resolution", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Conflict and Resolution Information", 
						"CONFLICT", "600px");
			}
		});
		
		menuBar_11.addItem(mntmConflictAndResolution);
		
		// show Priorities Diagram Info pop up event
		MenuItem mntmPrioritiesDiagram = new MenuItem("17.5. Priorities Diagram", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Priorities Diagram Information", 
						"PRIORITIES_DIAGRAM", "600px");
			}
		});
		
		menuBar_11.addItem(mntmPrioritiesDiagram);
		
		// show Participants Info pop up event
		MenuItem mntmParticipants = new MenuItem("17.6. Participants", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Participants Information", 
						"PARTICIPANTS", "580px");
			}
		});
		
		menuBar_11.addItem(mntmParticipants);
		menuBar_4.addItem(mntmRequirements);
		MenuBar menuBar_12 = new MenuBar(true);
		
		MenuItem mntmModelling = new MenuItem("18. Modelling", false, menuBar_12);
		
		// show Modelling Info pop up event
		MenuItem mntmModelling_1 = new MenuItem("18. Modelling", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Modelling Information", 
						"MODELLING", "520px");
			}
		});
		
		menuBar_12.addItem(mntmModelling_1);
		
		// show Behaviour Info pop up event
		MenuItem mntmBehaviour = new MenuItem("18.1. Behaviour", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Behaviour Information", 
						"BEHAVIOUR", "560px");
			}
		});
		
		menuBar_12.addItem(mntmBehaviour);
		
		// show Use Case Diagram Info pop up event
		MenuItem mntmUseCaseDiagram = new MenuItem("18.1.1. Use Case Diagram", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Use Case Diagram Information", 
						"USE_CASE", "565px");
			}
		});
		
		menuBar_12.addItem(mntmUseCaseDiagram);
		
		// show Colaboration/Sequence Diagrams Info pop up event
		MenuItem mntmColaborationsequenceDiagrams = new MenuItem("18.1.2. Colaboration/Sequence Diagrams", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Collaboration/Sequence Diagrams Information", 
						"COLLABORATION", "620px");
			}
		});
		
		menuBar_12.addItem(mntmColaborationsequenceDiagrams);
		
		// show Activity/State Diagrams Info pop up event
		MenuItem mntmActivitystateDiagrams = new MenuItem("18.1.3. Activity/State Diagrams", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Activity/State Diagrams Information", 
						"ACTIVITY", "595px");
			}
		});
		
		menuBar_12.addItem(mntmActivitystateDiagrams);
		
		// show Structure Info pop up event
		MenuItem mntmStructure = new MenuItem("18.2. Structure", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Structure Information", 
						"STRUCTURE", "550px");
			}
		});
		
		menuBar_12.addItem(mntmStructure);
		
		// show Class Diagram Info pop up event
		MenuItem mntmClassDiagram = new MenuItem("18.2.1. Class Diagram", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Class Diagram Information", 
						"CLASS_DIAGRAM", "570px");
			}
		});
		
		menuBar_12.addItem(mntmClassDiagram);
		
		// show Class descriptions Info pop up event
		MenuItem mntmClassDescriptions = new MenuItem("18.2.2. Class Descriptions", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Class Descriptions Information", 
						"CLASS_DESCRIPTIONS", "580px");
			}
		});
		
		menuBar_12.addItem(mntmClassDescriptions);
		
		// show Relationship descriptions Info pop up event
		MenuItem mntmRelationshipDescriptions = new MenuItem("18.2.3. Relationship Descriptions", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Relationship Descriptions Information", 
						"RELATIONSHIP_DESCRIPTIONS", "580px");
			}
		});
		
		menuBar_12.addItem(mntmRelationshipDescriptions);
		
		// show Solution Variants Info pop up event
		MenuItem mntmSolutionVariants = new MenuItem("18.3. Solution Variants", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Solution Variants Information", 
						"SOLUTION_VARIANTS", "635px");
			}
		});
		
		menuBar_12.addItem(mntmSolutionVariants);
		menuBar_4.addItem(mntmModelling);
		
		// show Resulting Context Info pop up event
		MenuItem mntmResultingContext = new MenuItem("19. Resulting Context", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Resulting Context Information", 
						"RESULTING_CONTEXT", "580px");
			}
		});
		
		menuBar_4.addItem(mntmResultingContext);
		
		// show Design Guidelines Info pop up event
		MenuItem mntmDesignGuidelines = new MenuItem("20. Design Guidelines", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Design Guidelines Information", 
						"DESIGN_GUIDE", "580px");
			}
		});
		
		menuBar_4.addItem(mntmDesignGuidelines);
		MenuBar menuBar_13 = new MenuBar(true);
		
		MenuItem mntmConsequences = new MenuItem("21. Consequences", false, menuBar_13);
		
		// show Consequences Info pop up event
		MenuItem mntmConsequences_1 = new MenuItem("21. Consequences", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Consequences Information", 
						"CONSEQUENCES", "550px");
			}
		});
		
		menuBar_13.addItem(mntmConsequences_1);
		
		// show Positive Consequences Info pop up event
		MenuItem mntmPositive = new MenuItem("21.1. Positive", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Positive Consequences Information", 
						"POSITIVE", "570px");
			}
		});
		
		menuBar_13.addItem(mntmPositive);
		
		// show Negative Consequences Info pop up event
		MenuItem mntmNegative = new MenuItem("21.2. Negative", false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup("Negative Consequences Information", 
						"NEGATIVE", "570px");
			}
		});
		
		menuBar_13.addItem(mntmNegative);
		menuBar_4.addItem(mntmConsequences);
		menuBar_2.addItem(mntmElementsInfo);
		
		menuBar.addItem(mntmHelp);
		
		this.ensureDebugId("menuPanel");
	}

	/**
	 * Create the Menu Items that shows a pop up with 
	 * the element information.
	 * 
	 * @param label The label of the Menu Item
	 * @param name The URL name of the element
	 * @param size The size of the pop up window
	 * @return Menu Item
	 *//*
	private MenuItem InfoMenuItem(final String label, final String name, 
		final String size){
		
		MenuItem item = new MenuItem(label, false, new Command() {			
			@Override
			public void execute() {
				UIService.showInfoPopup(label + " Information", name, size);
			}
		});

		return item;
	}*/
}
