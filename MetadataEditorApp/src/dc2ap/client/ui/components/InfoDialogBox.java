package dc2ap.client.ui.components;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Create and show the element information pop up. 
 * Event of the (?) Icon.
 * 
 * @author Douglas A. Peixoto - 07/2012
 */
public class InfoDialogBox extends DialogBox {

	/**
	 * Create a dialog box and set the caption text
	 * 
	 * @param headerText Header text of the pop up
	 * @param name Element Tag name
	 * @param height Page height
	 */
	public InfoDialogBox(String headerText, String name, String height) {
		setAutoHideEnabled(true);
		setGlassEnabled(true);
		final String path = "http://www.dpi.ufv.br/projetos/apri/?page_id=316#";
	    
		setText(headerText);
	    ensureDebugId("infoPopup");

	    // Create a panel to layout the content
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setSpacing(4);
	    setWidget(dialogContents);

		// Create the image and pop up with the information
		Frame frame = new Frame(path + name);
		frame.setSize("1000px", height);
		
		// Add the image to the dialog
	    dialogContents.add(frame);
	    dialogContents.setCellHorizontalAlignment(
	    		frame, HasHorizontalAlignment.ALIGN_CENTER);
		
	    // Add a close button at the bottom of the dialog
	    Button closeButton = new Button(
	        "Close", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	  hide();
	          }
	    });
	    closeButton.setStyleName("button");
	    closeButton.setTitle("Close Info Box");
	    
	    dialogContents.add(closeButton);
	    
	    // configure the button position
	    dialogContents.setCellHorizontalAlignment(
		          closeButton, HasHorizontalAlignment.ALIGN_RIGHT);
	    
	    // setPopupPosition(30, 30);
	}

}
