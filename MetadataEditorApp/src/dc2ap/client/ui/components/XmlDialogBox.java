package dc2ap.client.ui.components;

import java.util.Date;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import dc2ap.client.services.DC2APService;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.HasVerticalAlignment;

public class XmlDialogBox extends DialogBox {

	public XmlDialogBox(final String rdfScript) {
		setGlassEnabled(true);
		setHTML("RDF Script for These Metadata");
		
		VerticalPanel dialogContents = new VerticalPanel();
		setWidget(dialogContents);
		dialogContents.setSize("600px", "660px");
		
		ScrollPanel mainScrollPanel = new ScrollPanel();
		dialogContents.add(mainScrollPanel);
		mainScrollPanel.setSize("800px", "600px");
		
		// receive XML script
		Label scriptRDF = new Label(rdfScript);
		scriptRDF.setStyleName("xmlLabel");
		scriptRDF.setSize("100%", "100%");
		
		mainScrollPanel.setWidget(scriptRDF);

		HorizontalPanel horizontalPanel = new HorizontalPanel();
	    horizontalPanel.setStyleName("backgroundDarkTop");
	    dialogContents.add(horizontalPanel);
	    horizontalPanel.setSize("100%", "50px");
	    
	    HorizontalPanel horizontalPanel_1 = new HorizontalPanel();
	    horizontalPanel_1.setSpacing(5);
	    horizontalPanel_1.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
	    horizontalPanel.add(horizontalPanel_1);
	    horizontalPanel_1.setWidth("207px");
	    horizontalPanel.setCellVerticalAlignment(horizontalPanel_1, HasVerticalAlignment.ALIGN_MIDDLE);
	    
	    Image validatedIimage = new Image("images/confirm.gif");
	    horizontalPanel_1.add(validatedIimage);
	    
	    Label lblValidated = new Label("Metadata Validation OK!");
	    lblValidated.setStyleName("textBoldSmallBlue");
	    horizontalPanel_1.add(lblValidated);
	    
	    HorizontalPanel buttonPanel = new HorizontalPanel();
	    buttonPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
	    horizontalPanel.add(buttonPanel);
	    horizontalPanel.setCellVerticalAlignment(buttonPanel, HasVerticalAlignment.ALIGN_MIDDLE);
	    horizontalPanel.setCellHorizontalAlignment(buttonPanel, HasHorizontalAlignment.ALIGN_RIGHT);
	    buttonPanel.setStyleName("gwt-HorizontalPanel");
	    buttonPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_RIGHT);
	    buttonPanel.setSpacing(5);
	    dialogContents.setCellHorizontalAlignment(buttonPanel, HasHorizontalAlignment.ALIGN_RIGHT);
	    buttonPanel.setSize("224px", "100%");
	    		
		// Add the save button at the bottom of the dialog
	    Button saveButton = new Button(
	        "Save", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	  // create the file name plus the current date to avoid duplicates
	        	  Date now = new Date();
	        	  final String fileName = "rdfFile_" + now.getTime();
	      
	        	  // save the file (call RPC method)
	        	  DC2APService.saveFileRDF(rdfScript, fileName);
	          }
	    });
	    saveButton.setStyleName("button");
	    saveButton.setTitle("Save RDF File");
	    	    
		// Add the close button at the bottom of the dialog
	    Button closeButton = new Button(
	        "Close", new ClickHandler() {
	          public void onClick(ClickEvent event) {
	        	  hide();
	          }
	    });
	    closeButton.setStyleName("button");
	    closeButton.setTitle("Close Dialog Box");
	    
	    buttonPanel.add(saveButton);
	    buttonPanel.add(closeButton);
	}

}
