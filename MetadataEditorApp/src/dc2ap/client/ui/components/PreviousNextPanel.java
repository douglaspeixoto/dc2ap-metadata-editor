package dc2ap.client.ui.components;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import dc2ap.client.services.PanelService;

/**
 * Class contain the UI images Previous Page and Next Page
 * of the bottom panel.
 * 
 * @author Douglas A. Peixoto - 06/2012
 *
 */
public class PreviousNextPanel extends Composite {
	private static Image previousImage = new Image("images/previous.png");
	private static Image nextImage = new Image("images/next.png");

	public PreviousNextPanel() {
		HorizontalPanel horizontalPanel = new HorizontalPanel();
		horizontalPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		initWidget(horizontalPanel);
		horizontalPanel.setSize("970px", "65px");

		previousImage.setTitle("Previous Panel");
		previousImage.setStyleName("imageLink");
		horizontalPanel.add(previousImage);
		
		// add the "Previous Panel" event to the image Previous
		previousImage.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				PanelService.setCurrentPanel(
						PanelService.getCurrentPanel().getPreviousPanel());
				PanelService.setCurrentPanelFocus(true);
			}
		});

		nextImage.setTitle("Next Panel");
		nextImage.setStyleName("imageLink");
		horizontalPanel.add(nextImage);
		
		// add the "Next Panel" event to the image Next
		nextImage.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				PanelService.setCurrentPanel(
						PanelService.getCurrentPanel().getNextPanel());
				PanelService.setCurrentPanelFocus(true);
			}
		});
		
		horizontalPanel.setCellHorizontalAlignment(nextImage, HasHorizontalAlignment.ALIGN_RIGHT);
		
		this.ensureDebugId("previousNextPanel");
	}

}
