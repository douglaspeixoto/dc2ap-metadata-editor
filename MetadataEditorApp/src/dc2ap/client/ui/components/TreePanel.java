package dc2ap.client.ui.components;

import java.util.Iterator;
import java.util.NoSuchElementException;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.VerticalPanel;
import dc2ap.client.services.PanelService;
import dc2ap.client.ui.panels.ConsequencesPanel;
import dc2ap.client.ui.panels.CoveragePanel;
import dc2ap.client.ui.panels.CreatorPanel;
import dc2ap.client.ui.panels.DatePanel;
import dc2ap.client.ui.panels.DescriptionPanel;
import dc2ap.client.ui.panels.FormatPanel;
import dc2ap.client.ui.panels.HistoryPanel;
import dc2ap.client.ui.panels.IdentifierPanel;
import dc2ap.client.ui.panels.ModelingPanel;
import dc2ap.client.ui.panels.PublisherPanel;
import dc2ap.client.ui.panels.RelationPanel;
import dc2ap.client.ui.panels.RequirementsPanel;
import dc2ap.client.ui.panels.ResultingPanel;
import com.google.gwt.widget.client.TextButton;

/**
 * This UI class implements the Tree of the main page, whit the items to be
 * selected.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public class TreePanel extends Composite {
	private Tree tree = new Tree();
	private TextButton btnExpandCollapse = new TextButton("+ Expand All");
	private boolean stateExpand = true;
	
	// panel instances
	private IdentifierPanel idPanel = PanelService.getIdentifierPanel();
	private CreatorPanel creatorPanel = PanelService.getCreatorPanel();
	private DescriptionPanel descrPanel = PanelService.getDescriptionPanel();
	private PublisherPanel publisherPanel = PanelService.getPublisherPanel();
	private DatePanel datePanel = PanelService.getDatePanel();
	private FormatPanel formatPanel = PanelService.getFormatPanel();
	private RelationPanel relationPanel = PanelService.getRelationPanel();
	private CoveragePanel coveragePanel = PanelService.getCoveragePanel();
	private HistoryPanel historyPanel = PanelService.getHistoryPanel();
	private RequirementsPanel requirementPanel = PanelService.getRequirementsPanel();
	private ModelingPanel modelingPanel = PanelService.getModelingPanel();
	private ResultingPanel resultingPanel = PanelService.getResultingPanel();
	private ConsequencesPanel conseqPanel = PanelService.getConsequencesPanel();

	/**
	 * Constructor.
	 */
	public TreePanel(){				
		String indexImg = "images/index.png";
		String non_indexImg = "images/non_index.png";
		
		VerticalPanel treePanel = new VerticalPanel();
		treePanel.setSize("250px", "730px");
		treePanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_LEFT);
		treePanel.add(btnExpandCollapse);
		
		initWidget(treePanel);
		
		btnExpandCollapse.setCollapseLeft(true);
		btnExpandCollapse.setCollapseRight(true);
		btnExpandCollapse.setTitle("Expand/Collapse Tree Items");
		btnExpandCollapse.setWidth("250px");
		
		ScrollPanel mainPanelTree = new ScrollPanel();
		mainPanelTree.setAlwaysShowScrollBars(true);
		treePanel.add(mainPanelTree);
		mainPanelTree.setSize("250px", "710px");
		mainPanelTree.add(tree);
		tree.setSize("250px", "150px");
		
		// create the event for the tree panel (open the panel selected)
		tree.addSelectionHandler(new SelectionHandler<TreeItem>() {
			@Override
			public void onSelection(SelectionEvent<TreeItem> event) {
				openSelectedPanel(event);	
			}
		});
		
		/*for each TreeItem we set the related panel and related 
		field with this item*/
		
		final MyTreeItem trtmDcap = new MyTreeItem("DC2AP");
		trtmDcap.setRelatedPanel(idPanel);
		trtmDcap.setRelatedField(idPanel.getIdentifierBox());
		trtmDcap.setStyleName("gwt-MyTreeItem");
		trtmDcap.setSize("220px", "437px");
		
		tree.addItem(trtmDcap);

		//** Identifier Panel
		MyTreeItem trtmIdentifier = createMyTreeItem(indexImg, "Identifier"); 
		trtmIdentifier.setStyleName("backgroundDarkTop");
		trtmIdentifier.setRelatedPanel(idPanel);
		trtmIdentifier.setRelatedField(idPanel.getIdentifierBox());
		trtmDcap.addItem(trtmIdentifier);
		
		final MyTreeItem trtmTitle = createMyTreeItem(indexImg, "Title"); 
		trtmTitle.setStyleName("backgroundDarkBottom");
		trtmTitle.setRelatedPanel(idPanel);
		trtmTitle.setRelatedField(idPanel.getTitleBox());
		trtmDcap.addItem(trtmTitle);
		
		MyTreeItem trtmAlttitle = createMyTreeItem(indexImg, "Alternative Title"); 
		trtmAlttitle.setRelatedPanel(idPanel);
		trtmAlttitle.setRelatedField(idPanel.getAltTitleBox());
		trtmTitle.addItem(trtmAlttitle);
		
		//** Creator Panel
		MyTreeItem trtmCreator = createMyTreeItem(indexImg, "Creator"); 
		trtmCreator.setStyleName("backgroundLight");
		trtmCreator.setRelatedPanel(creatorPanel);
		trtmCreator.setRelatedField(creatorPanel.getCreatorBox());
		trtmDcap.addItem(trtmCreator);
		
		MyTreeItem trtmSubject = createMyTreeItem(indexImg, "Subject"); 
		trtmSubject.setStyleName("backgroundLight");
		trtmSubject.setRelatedPanel(creatorPanel);
		trtmSubject.setRelatedField(creatorPanel.getSubjectBox());
		trtmDcap.addItem(trtmSubject);
		
		//** Description Panel
		final MyTreeItem trtmDescription = createMyTreeItem(non_indexImg, "Description"); 
		trtmDescription.setStyleName("backgroundDarkBox");
		trtmDescription.setRelatedPanel(descrPanel);
		trtmDescription.setRelatedField(descrPanel.getProblemBox());
		trtmDcap.addItem(trtmDescription);
		
		MyTreeItem trtmProblem = createMyTreeItem(indexImg, "Problem"); 
		trtmProblem.setRelatedPanel(descrPanel);
		trtmProblem.setRelatedField(descrPanel.getProblemBox());
		trtmDescription.addItem(trtmProblem);
		
		final MyTreeItem trtmMotivation = createMyTreeItem(indexImg, "Motivation"); 
		trtmMotivation.setRelatedPanel(descrPanel);
		trtmMotivation.setRelatedField(descrPanel.getMotivationBox());
		trtmDescription.addItem(trtmMotivation);
		
		MyTreeItem trtmExample = createMyTreeItem(indexImg, "Example"); 
		trtmExample.setRelatedPanel(descrPanel);
		trtmExample.setRelatedField(descrPanel.getExampleBox());
		trtmMotivation.addItem(trtmExample);
		
		MyTreeItem trtmKnownuses = createMyTreeItem(indexImg, "Known Uses"); 
		trtmKnownuses.setRelatedPanel(descrPanel);
		trtmKnownuses.setRelatedField(descrPanel.getKnownUsesBox());
		trtmMotivation.addItem(trtmKnownuses);
		
		MyTreeItem trtmContext = createMyTreeItem(indexImg, "Context"); 
		trtmContext.setRelatedPanel(descrPanel);
		trtmContext.setRelatedField(descrPanel.getContextBox());
		trtmDescription.addItem(trtmContext);
		
		//** Publisher Panel
		MyTreeItem trtmPublisher = createMyTreeItem(indexImg, "Publisher"); 
		trtmPublisher.setStyleName("backgroundLight");
		trtmPublisher.setRelatedPanel(publisherPanel);
		trtmPublisher.setRelatedField(publisherPanel.getPublisherBox());
		trtmDcap.addItem(trtmPublisher);
		
		MyTreeItem trtmContributor = createMyTreeItem(indexImg, "Contributor"); 
		trtmContributor.setStyleName("backgroundLightBottom");
		trtmContributor.setRelatedPanel(publisherPanel);
		trtmContributor.setRelatedField(publisherPanel.getContributorBox());
		trtmDcap.addItem(trtmContributor);
		
		//** Date Panel
		final MyTreeItem trtmDate = createMyTreeItem(non_indexImg, "Date"); 
		trtmDate.setStyleName("backgroundDark");
		trtmDate.setRelatedPanel(datePanel);
		trtmDate.setRelatedField(datePanel.getCreatedBox());
		trtmDcap.addItem(trtmDate);
		
		MyTreeItem trtmCreated = createMyTreeItem(indexImg, "Created"); 
		trtmCreated.setRelatedPanel(datePanel);
		trtmCreated.setRelatedField(datePanel.getCreatedBox());
		trtmDate.addItem(trtmCreated);
		
		MyTreeItem trtmModified = createMyTreeItem(indexImg, "Modified"); 
		trtmModified.setRelatedPanel(datePanel);
		trtmModified.setRelatedField(datePanel.getModifiedBox());
		trtmDate.addItem(trtmModified);
		
		final MyTreeItem trtmType = createMyTreeItem(indexImg, "Type"); 
		trtmType.setStyleName("backgroundDarkBottom");
		trtmType.setRelatedPanel(datePanel);
		trtmType.setRelatedField(datePanel.getTypeBox());
		trtmDcap.addItem(trtmType);
		
		MyTreeItem trtmNotation = createMyTreeItem(indexImg, "Notation"); 
		trtmNotation.setRelatedPanel(datePanel);
		trtmNotation.setRelatedField(datePanel.getNotationBox());
		trtmType.addItem(trtmNotation);
		
		//** Format Panel
		MyTreeItem trtmFormat = createMyTreeItem(indexImg, "Format"); 
		trtmFormat.setStyleName("backgroundLight");
		trtmFormat.setRelatedPanel(formatPanel);
		trtmFormat.setRelatedField(formatPanel.getXmiBox());
		trtmDcap.addItem(trtmFormat);
		
		MyTreeItem trtmSource = createMyTreeItem(indexImg, "Source"); 
		trtmSource.setStyleName("backgroundLight");
		trtmSource.setRelatedPanel(formatPanel);
		trtmSource.setRelatedField(formatPanel.getSourceBox());
		trtmDcap.addItem(trtmSource);
		
		MyTreeItem trtmLanguage = createMyTreeItem(indexImg, "Language"); 
		trtmLanguage.setStyleName("backgroundLightBottom");
		trtmLanguage.setRelatedPanel(formatPanel);
		trtmLanguage.setRelatedField(formatPanel.getLanguageLabelBox());
		trtmDcap.addItem(trtmLanguage);
		
		//** Relation Panel
		final MyTreeItem trtmRelation = createMyTreeItem(non_indexImg, "Relation"); 
		trtmRelation.setStyleName("backgroundDarkBottom");
		trtmRelation.setRelatedPanel(relationPanel);
		trtmRelation.setRelatedField(relationPanel.getIsVersionOfBox());
		trtmDcap.addItem(trtmRelation);
		
		MyTreeItem trtmIsVersionOf = createMyTreeItem(indexImg, "Is Version Of"); 
		trtmIsVersionOf.setRelatedPanel(relationPanel);
		trtmIsVersionOf.setRelatedField(relationPanel.getIsVersionOfBox());
		trtmRelation.addItem(trtmIsVersionOf);
		
		MyTreeItem trtmIsReplacedBy = createMyTreeItem(indexImg, "Is Replaced By"); 
		trtmIsReplacedBy.setRelatedPanel(relationPanel);
		trtmIsReplacedBy.setRelatedField(relationPanel.getIsReplacedByBox());
		trtmRelation.addItem(trtmIsReplacedBy);
		
		MyTreeItem trtmReplaces = createMyTreeItem(indexImg, "Replaces"); 
		trtmReplaces.setRelatedPanel(relationPanel);
		trtmReplaces.setRelatedField(relationPanel.getReplacesBox());
		trtmRelation.addItem(trtmReplaces);
		
		MyTreeItem trtmIsPartOf = createMyTreeItem(indexImg, "Is Part Of"); 
		trtmIsPartOf.setRelatedPanel(relationPanel);
		trtmIsPartOf.setRelatedField(relationPanel.getIsPartOfBox());
		trtmRelation.addItem(trtmIsPartOf);
		
		MyTreeItem trtmHasPart = createMyTreeItem(indexImg, "Has Part"); 
		trtmHasPart.setRelatedPanel(relationPanel);
		trtmHasPart.setRelatedField(relationPanel.getHasPartBox());
		trtmRelation.addItem(trtmHasPart);
		
		MyTreeItem trtmIsDesignedWith = createMyTreeItem(indexImg, "Is Designed With"); 
		trtmIsDesignedWith.setRelatedPanel(relationPanel);
		trtmIsDesignedWith.setRelatedField(relationPanel.getIsDesignedWithBox());
		trtmRelation.addItem(trtmIsDesignedWith);
		
		MyTreeItem trtmShouldAvoid = createMyTreeItem(indexImg, "Should Avoid"); 
		trtmShouldAvoid.setRelatedPanel(relationPanel);
		trtmShouldAvoid.setRelatedField(relationPanel.getShouldAvoidBox());
		trtmRelation.addItem(trtmShouldAvoid);
		
		MyTreeItem trtmComplementedBy = createMyTreeItem(indexImg, "Complemented By"); 
		trtmComplementedBy.setRelatedPanel(relationPanel);
		trtmComplementedBy.setRelatedField(relationPanel.getComplementedByBox());
		trtmRelation.addItem(trtmComplementedBy);
		
		MyTreeItem trtmAbout = createMyTreeItem(indexImg, "About"); 
		trtmAbout.setRelatedPanel(relationPanel);
		trtmAbout.setRelatedField(relationPanel.getAboutBox());
		trtmRelation.addItem(trtmAbout);
		
		//** Coverage Panel
		MyTreeItem trtmCoverage = createMyTreeItem(indexImg, "Coverage"); 
		trtmCoverage.setStyleName("backgroundLight");
		trtmCoverage.setRelatedPanel(coveragePanel);
		trtmCoverage.setRelatedField(coveragePanel.getCoverageBox());
		trtmDcap.addItem(trtmCoverage);
		
		MyTreeItem trtmRights = createMyTreeItem(indexImg, "Rights"); 
		trtmRights.setStyleName("backgroundLightBottom");
		trtmRights.setRelatedPanel(coveragePanel);
		trtmRights.setRelatedField(coveragePanel.getRightsBox());
		trtmDcap.addItem(trtmRights);
		
		//** History Panel
		final MyTreeItem trtmHistory = createMyTreeItem(non_indexImg, "History"); 
		trtmHistory.setStyleName("backgroundDarkBottom");
		trtmHistory.setRelatedPanel(historyPanel);
		trtmHistory.setRelatedField(historyPanel.getEventDateBox());
		trtmDcap.addItem(trtmHistory);
		
		MyTreeItem trtmEventDate = createMyTreeItem(indexImg, "Event Date"); 
		trtmEventDate.setRelatedPanel(historyPanel);
		trtmEventDate.setRelatedField(historyPanel.getEventDateBox());
		trtmHistory.addItem(trtmEventDate);
		
		MyTreeItem trtmAuthor = createMyTreeItem(indexImg, "Author"); 
		trtmAuthor.setRelatedPanel(historyPanel);
		trtmAuthor.setRelatedField(historyPanel.getAuthorBox());
		trtmHistory.addItem(trtmAuthor);
		
		MyTreeItem trtmReason = createMyTreeItem(indexImg, "Reason"); 
		trtmReason.setRelatedPanel(historyPanel);
		trtmReason.setRelatedField(historyPanel.getReasonBox());
		trtmHistory.addItem(trtmReason);
				
		MyTreeItem trtmChanges = createMyTreeItem(indexImg, "Changes"); 
		trtmChanges.setRelatedPanel(historyPanel);
		trtmChanges.setRelatedField(historyPanel.getChangesBox());
		trtmHistory.addItem(trtmChanges);

		//** Requirement Panel
		final MyTreeItem trtmRequirements = createMyTreeItem(non_indexImg, "Requirements"); 
		trtmRequirements.setRelatedPanel(requirementPanel);
		trtmRequirements.setRelatedField(requirementPanel.getFunctionalRequirementsBox());
		trtmRequirements.setStyleName("backgroundLightBottom");
		trtmDcap.addItem(trtmRequirements);
		
		SafeHtmlBuilder sb = new SafeHtmlBuilder();
		sb.appendHtmlConstant("Functional<br>Requirements");//&nbsp;&nbsp;&nbsp;
		MyTreeItem trtmFunctionalRequirements = createMyTreeItem(indexImg, sb); 
		trtmFunctionalRequirements.setRelatedPanel(requirementPanel);
		trtmFunctionalRequirements.setRelatedField(requirementPanel.getFunctionalRequirementsBox());
		trtmRequirements.addItem(trtmFunctionalRequirements);

		SafeHtmlBuilder sb1 = new SafeHtmlBuilder();
		sb1.appendHtmlConstant("Non-Functional<br>Requirements");
		MyTreeItem trtmNonfunctionalReq = createMyTreeItem(indexImg, sb1); 
		trtmNonfunctionalReq.setRelatedPanel(requirementPanel);
		trtmNonfunctionalReq.setRelatedField(requirementPanel.getNonFunctionalRequirementsBox());
		trtmRequirements.addItem(trtmNonfunctionalReq);
		
		SafeHtmlBuilder sb2 = new SafeHtmlBuilder();
		sb2.appendHtmlConstant("Dependencies and<br>Contributions");
		final MyTreeItem trtmDepencesAndContributions = createMyTreeItem(indexImg, sb2); 
		trtmDepencesAndContributions.setRelatedPanel(requirementPanel);
		trtmDepencesAndContributions.setRelatedField(requirementPanel.getDependenciesAndContributionsBox());
		trtmRequirements.addItem(trtmDepencesAndContributions);
		
		MyTreeItem trtmDependencyGraph = createMyTreeItem(indexImg, "Dependency Graph");
		trtmDependencyGraph.setRelatedPanel(requirementPanel);
		trtmDependencyGraph.setRelatedField(requirementPanel.getDependencyGraphBox());
		trtmDepencesAndContributions.addItem(trtmDependencyGraph);
		
		MyTreeItem trtmContributionGraph = createMyTreeItem(indexImg, "Contribution Graph"); 
		trtmContributionGraph.setRelatedPanel(requirementPanel);
		trtmContributionGraph.setRelatedField(requirementPanel.getContributionGraphBox());
		trtmDepencesAndContributions.addItem(trtmContributionGraph);
		
		SafeHtmlBuilder sb3 = new SafeHtmlBuilder();
		sb3.appendHtmlConstant("Conflict and<br>Resolution");
		MyTreeItem trtmConflictResolution = createMyTreeItem(indexImg, sb3); 
		trtmConflictResolution.setRelatedPanel(requirementPanel);
		trtmConflictResolution.setRelatedField(requirementPanel.getConflictAndResolutionBox());
		trtmRequirements.addItem(trtmConflictResolution);
		
		MyTreeItem trtmPrioritiesDiagram = createMyTreeItem(indexImg, "Priorities Diagram"); 
		trtmPrioritiesDiagram.setRelatedPanel(requirementPanel);
		trtmPrioritiesDiagram.setRelatedField(requirementPanel.getPrioritiesDiagramBox());
		trtmRequirements.addItem(trtmPrioritiesDiagram);
		
		MyTreeItem trtmParticipants = createMyTreeItem(indexImg, "Participants"); 
		trtmParticipants.setRelatedPanel(requirementPanel);
		trtmParticipants.setRelatedField(requirementPanel.getParticipantsBox());
		trtmRequirements.addItem(trtmParticipants);
		
		//** Modeling Panel
		final MyTreeItem trtmModeling = createMyTreeItem(non_indexImg, "Modeling"); 
		trtmModeling.setStyleName("backgroundDarkBottom");
		trtmModeling.setRelatedPanel(modelingPanel);
		trtmModeling.setRelatedField(modelingPanel.getUseCaseDiagramBox());
		trtmDcap.addItem(trtmModeling);
		
		final MyTreeItem trtmBehavior = createMyTreeItem(non_indexImg, "Behavior");//new MyTreeItem("Behavior");
		trtmBehavior.setRelatedPanel(modelingPanel);
		trtmBehavior.setRelatedField(modelingPanel.getUseCaseDiagramBox());
		trtmModeling.addItem(trtmBehavior);
		
		MyTreeItem trtmUseCaseDiagram = createMyTreeItem(indexImg, "Use Case Diagram"); 
		trtmUseCaseDiagram.setRelatedPanel(modelingPanel);
		trtmUseCaseDiagram.setRelatedField(modelingPanel.getUseCaseDiagramBox());
		trtmBehavior.addItem(trtmUseCaseDiagram);
		
		SafeHtmlBuilder sb5 = new SafeHtmlBuilder();
		sb5.appendHtmlConstant("Collaboration /<br>Sequence Diagram");
		MyTreeItem trtmCollabseqDiagram = createMyTreeItem(indexImg, sb5); 
		trtmCollabseqDiagram.setRelatedPanel(modelingPanel);
		trtmCollabseqDiagram.setRelatedField(modelingPanel.getColaborationSequenceDiagramBox());
		trtmBehavior.addItem(trtmCollabseqDiagram);
		
		SafeHtmlBuilder sb6 = new SafeHtmlBuilder();
		sb6.appendHtmlConstant("Activity / State<br>Diagram");
		MyTreeItem trtmActstateDiagrams = createMyTreeItem(indexImg, sb6); 
		trtmActstateDiagrams.setRelatedPanel(modelingPanel);
		trtmActstateDiagrams.setRelatedField(modelingPanel.getActivityStateBox());
		trtmBehavior.addItem(trtmActstateDiagrams);
		
		final MyTreeItem trtmStructure = createMyTreeItem(non_indexImg, "Structure"); 
		trtmStructure.setRelatedPanel(modelingPanel);
		trtmStructure.setRelatedField(modelingPanel.getClassDiagramBox());
		trtmModeling.addItem(trtmStructure);
		
		MyTreeItem trtmClassDiagram = createMyTreeItem(indexImg, "Class Diagram"); 
		trtmClassDiagram.setRelatedPanel(modelingPanel);
		trtmClassDiagram.setRelatedField(modelingPanel.getClassDiagramBox());
		trtmStructure.addItem(trtmClassDiagram);
		
		MyTreeItem trtmClassDescription = createMyTreeItem(indexImg, "Class Description"); 
		trtmClassDescription.setRelatedPanel(modelingPanel);
		trtmClassDescription.setRelatedField(modelingPanel.getClassDescriptionBox());
		trtmStructure.addItem(trtmClassDescription);
		
		SafeHtmlBuilder sb7 = new SafeHtmlBuilder();
		sb7.appendHtmlConstant("Relationship<br>Description");
		MyTreeItem trtmRelationshipDescr = createMyTreeItem(indexImg, sb7); 
		trtmRelationshipDescr.setRelatedPanel(modelingPanel);
		trtmRelationshipDescr.setRelatedField(modelingPanel.getRelationshipDescriptionBox());
		trtmStructure.addItem(trtmRelationshipDescr);
		
		MyTreeItem trtmSolutionVariants = createMyTreeItem(indexImg, "Solution Variants"); 
		trtmSolutionVariants.setRelatedPanel(modelingPanel);
		trtmSolutionVariants.setRelatedField(modelingPanel.getSolutionVariantsBox());
		trtmModeling.addItem(trtmSolutionVariants);
		
		//** Resulting panel
		MyTreeItem trtmResultingContext = createMyTreeItem(indexImg, "Resulting Context"); 
		trtmResultingContext.setStyleName("backgroundLight");
		trtmResultingContext.setRelatedPanel(resultingPanel);
		trtmResultingContext.setRelatedField(resultingPanel.getResultingContextBox());
		trtmDcap.addItem(trtmResultingContext);
		
		MyTreeItem trtmDesignGuidelines = createMyTreeItem(indexImg, "Design Guidelines"); 
		trtmDesignGuidelines.setStyleName("backgroundLightBottom");
		trtmDesignGuidelines.setRelatedPanel(resultingPanel);
		trtmDesignGuidelines.setRelatedField(resultingPanel.getDesignGuidelinesBox());
		trtmDcap.addItem(trtmDesignGuidelines);
		
		//** Consequences panel
		final MyTreeItem trtmConsequences = createMyTreeItem(non_indexImg, "Consequences"); 
		trtmConsequences.setStyleName("backgroundDarkBottom");
		trtmConsequences.setRelatedPanel(conseqPanel);
		trtmConsequences.setRelatedField(conseqPanel.getPositiveBox());
		trtmDcap.addItem(trtmConsequences);
		
		MyTreeItem trtmPositive = createMyTreeItem(indexImg, "Positive"); 
		trtmPositive.setRelatedPanel(conseqPanel);
		trtmPositive.setRelatedField(conseqPanel.getPositiveBox());
		trtmConsequences.addItem(trtmPositive);
		
		MyTreeItem trtmNegative = createMyTreeItem(indexImg, "Negative"); 
		trtmNegative.setRelatedPanel(conseqPanel);
		trtmNegative.setRelatedField(conseqPanel.getNegativeBox());
		trtmConsequences.addItem(trtmNegative);

		// add the event that expand or collapse the Tree Items
		btnExpandCollapse.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if(stateExpand){
					btnExpandCollapse.setText("- Collapse All");
					expandAll();
					stateExpand = false;
				} else{
					btnExpandCollapse.setText("+ Expand All");
					collapseAll();
					stateExpand = true;
				}
			}
		});

		trtmDcap.setState(true);
		
		this.ensureDebugId("treePanel");
	}

	/**
	 * Tell which panel will be shown in the center panel according to the Tree
	 * Item selected. Also set focus on the element selected.
	 * 
	 * @param event The TreeItem click event
	 */
	private void openSelectedPanel(SelectionEvent<TreeItem> event) {
		// get the panel related with the TreeItem selected
		final MyTreeItem item = (MyTreeItem) event.getSelectedItem();
		// If there is some Tree Item selected
		if (item != null) {
			PanelService.setCurrentPanel(item.getRelatedPanel());
			
			item.setRelatedFieldFocus(true);
			
			// use Scheduler to fix the set focus problem.
			/*Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
				public void execute() {
					item.setRelatedFieldFocus(true);
				}
			});*/
		}
	}

	/**
	 * Create the tree item, which is an icon and the label
	 * 
	 * @param imgPath Path of the image
	 * @param label Label of the tree item
	 * @return Tree Item
	 */
	private final MyTreeItem createMyTreeItem(String imgPath, String label) {
		SafeHtmlBuilder sb = new SafeHtmlBuilder();
		sb.appendHtmlConstant("<tr><td><img src='" + imgPath + "'></td>");
		sb.appendHtmlConstant("<td>" + label + "</td></tr>");

		return new MyTreeItem(sb.toSafeHtml());
	}

	/**
	 * Create the tree item, which is an icon and the label
	 * 
	 * @param imgPath Path of the image
	 * @param sb The SafeHmlBuilder with the label of the tree item
	 * @return Tree Item
	 */
	private final MyTreeItem createMyTreeItem(String imgPath,
			SafeHtmlBuilder sbHtml) {
		SafeHtmlBuilder sb = new SafeHtmlBuilder();
		sb.appendHtmlConstant("<tr><td><img src='" + imgPath + "'></td>");
		sb.appendHtmlConstant("<td>" + sbHtml.toSafeHtml().asString()
				+ "</td></tr>");

		return new MyTreeItem(sb.toSafeHtml());
	}

	/**
	 * Expand all TreeItems
	 */
	private void expandAll() {
		Iterator<TreeItem> it = tree.treeItemIterator();
		TreeItem trtm;
		try {
			while ((trtm = it.next()) != null) {
				if (trtm.getChildCount() > 0) {
					trtm.setState(true);
				}
			}
		} catch (NoSuchElementException e) {
			return;
		}
	}

	/**
	 * Collapse all TreeItems
	 */
	private void collapseAll() {
		Iterator<TreeItem> it = tree.treeItemIterator();
		TreeItem trtm = it.next(); // jump the first item (DC2AP)
		try {
			while ((trtm = it.next()) != null) {
				if (trtm.getChildCount() > 0) {
					trtm.setState(false);
				}
			}
		} catch (NoSuchElementException e) {
			return;
		}
	}

}
