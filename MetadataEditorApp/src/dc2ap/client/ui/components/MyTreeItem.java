package dc2ap.client.ui.components;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TreeItem;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import dc2ap.client.ui.panels.GenericPanel;

/**
 * This class implements a customized TreeItem,
 * with the Panel and the Field of the panel related 
 * with the TreeItem. Extends the GWT widget TreeItem.
 * 
 * @author Douglas A. Peixoto - 07/2012
 *
 */
public class MyTreeItem extends TreeItem {
	private GenericPanel relatedPanel;
	private Widget relatedField;
	private Integer relatedFieldType;
	
	// constants
	private static final Integer 
		TEXT_BOX = 1, 
		MULTIPLE = 2, 
		DATE_BOX = 3, 
		SUGGEST_BOX = 4, 
		CHECK_BOX = 5; 
	
	// constructors
	public MyTreeItem() {
		super();
	}
	public MyTreeItem(SafeHtml html) {
		super(html);
	}
	public MyTreeItem(String html) {
		super(html);
	}
	public MyTreeItem(Widget widget) {
		super(widget);
	}
	
	/**
	 * Get which panel this TreeItem is related with.
	 * 
	 * @return Related panel in the panel
	 */
	public GenericPanel getRelatedPanel() {
		return relatedPanel;
	}
	/**
	 * Set the panel related with this TreeItem.
	 * 
	 * @return relatedPanel Panel related with this TreItem
	 */
	public void setRelatedPanel(GenericPanel relatedPanel) {
		this.relatedPanel = relatedPanel;
	}
	
	/**
	 * Get the field of the panel related with this TreeItem.
	 * 
	 * @return Related field in the panel
	 */
	public Widget getRelatedField() {
		return relatedField;
	}
	/**
	 * Set the field of the panel related with this TreeItem.
	 * 
	 * @return relatedField Field related with this TreItem
	 */
	public void setRelatedField(Widget relatedField) {
		this.relatedField = relatedField;
	}

	/**
	 * Explicitly focus/unfocus the element field
	 * related with this MyTreeItem.
	 * 
	 * @param focused Whether the field should 
	 * take focus or release it.
	 */
	public void setRelatedFieldFocus(final boolean focused){
		// use Scheduler to fix the set focus problem.
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			@Override
			public void execute() {
				if(TEXT_BOX == relatedFieldType){
					((TextBox)relatedField).setFocus(focused);
				} else if(DATE_BOX == relatedFieldType){
					((DateBox)relatedField).setFocus(focused);
				} else if(SUGGEST_BOX == relatedFieldType){
					((SuggestBox)relatedField).setFocus(focused);
				} else if(CHECK_BOX == relatedFieldType){
					((CheckBox)relatedField).setFocus(focused);
				} else if(MULTIPLE == relatedFieldType){
					((MultipleTextField)relatedField).setFocus(focused, 0);
				}
			}
		});
	}
	
	/**
	 * Set the TextBox field of the panel related with this TreeItem.
	 * 
	 * @return relatedField TextBox Field related with this TreItem
	 */
	public void setRelatedField(TextBox relatedField) {
		this.relatedField = relatedField;
		relatedFieldType = TEXT_BOX;
	}
	/**
	 * Set the MultipleTextField of the panel related with this TreeItem.
	 * 
	 * @return relatedField MultipleTextField related with this TreItem
	 */
	public void setRelatedField(MultipleTextField relatedField) {
		this.relatedField = relatedField;
		relatedFieldType = MULTIPLE;
	}
	/**
	 * Set the SuggestBox field of the panel related with this TreeItem.
	 * 
	 * @return relatedField SuggestBox Field related with this TreItem
	 */
	public void setRelatedField(SuggestBox relatedField) {
		this.relatedField = relatedField;
		relatedFieldType = SUGGEST_BOX;
	}
	/**
	 * Set the CheckBox field of the panel related with this TreeItem.
	 * 
	 * @return relatedField CheckBox Field related with this TreItem
	 */
	public void setRelatedField(CheckBox relatedField) {
		this.relatedField = relatedField;
		relatedFieldType = CHECK_BOX;
	}
	/**
	 * Set the DateBox field of the panel related with this TreeItem.
	 * 
	 * @return relatedField DateBox Field related with this TreItem
	 */
	public void setRelatedField(DateBox relatedField) {
		this.relatedField = relatedField;
		relatedFieldType = DATE_BOX;
	}
	
}
