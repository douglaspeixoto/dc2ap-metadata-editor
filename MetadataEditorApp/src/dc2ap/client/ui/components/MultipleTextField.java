package dc2ap.client.ui.components;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * This composite contains the UI of the multiple
 * text fields with its buttons: Add, Remove, 
 * move element Up and move element Down.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public class MultipleTextField extends Composite {
	// list of TextBoxes
	private List<TextBox> boxList = new ArrayList<TextBox>();
	// the table contents
	private final FlexTable table = new FlexTable(); 
	// the width of the Text Boxes
	private final Integer txtWidth;
	// index of the text box in the table
	private final static int TEXBOX_INDEX = 1;
	
	public MultipleTextField(Integer panelWidth) {
		ScrollPanel scrollPanel = new ScrollPanel();
		scrollPanel.setStyleName("backgroundDarkBox");
		initWidget(scrollPanel);
		scrollPanel.setSize(panelWidth.toString() + "px", "200px");
		
		table.setCellSpacing(5);
		scrollPanel.setWidget(table);
		
		final Integer tbWidth = (panelWidth-20);
		table.setSize(tbWidth.toString() + "px", "60px");
		
		// calculate the size of the text box
		txtWidth = (panelWidth-160);

		// add the first item
		//UIService.addItemToTable(table, boxList, null, txtWidth);
		addItemToMultipleTextField(0, null);
		
		// cells align
		CellFormatter format = table.getCellFormatter();
		format.setVerticalAlignment(0, 1, HasVerticalAlignment.ALIGN_MIDDLE);
		format.setHorizontalAlignment(0, 1, HasHorizontalAlignment.ALIGN_CENTER);
	}

	/**
	 * Return the String list with the Strings of the TextBoxes of the table.
	 * 
	 * @return Fields String List
	 */
	public List<String> getBoxStringList() {
		List<String> list = new ArrayList<String>();  
		
		for(TextBox txtBox : boxList){
			if(!"".equals(txtBox.getText())){
				list.add(txtBox.getText());
			}
		}
		
		return list;
	}
	
	/**
	 * Set a list of String into the fields of this 
	 * {@link MultipleTextField} object (Open a String 
	 * list into this MultipleTextField).
	 * 
	 * @param list The list of strings to be set.
	 */
	public void setBoxStringList(List<String> list){
		// clear the TexBox list and the table.
    	boxList.clear();
    	table.removeAllRows();
    	
    	// if there is no item then add one empty field
    	if(list.size() == 0){
    		addItemToMultipleTextField(0, "");
    	}
    	
    	// Add each item of the string list into a row of the table.
    	for(int index=0; index<list.size(); index++){
    		addItemToMultipleTextField(index, list.get(index));
    	}
	}
	
	/**
	 * Return the TexBox of index "index" from the {@link MultipleTextField}.
	 * (The first index is the index = 0).
	 * 
	 * @param index The index of the TexBox
	 * @return TextBox
	 */
	public TextBox getTextBoxByIndex(int index){
		/*if(index < table.getRowCount() && index > -1){
			return (TextBox)table.getWidget(index, TEXBOX_INDEX);*/
		if(index < boxList.size() && index > -1){
			return boxList.get(index);
		} else{
			Window.alert("Error in method 'getTextBoxByIndex'\n" +
					"in 'MultipleTextField' class!\n " +
					"You are trying to acces an invalid Table position.");
		}
		return null;
	}

	/** 
	 * Clear, and also remove, all the fields of this element.
	 * Clear the TextBox list and remove the table rows.
	 */
	public void clear(){
		// clear the TextBox list
		boxList.clear();
		// remove all rows from the table
		table.removeAllRows();
		// add the first item to the table.
		//UIService.addItemToTable(table, boxList, null, txtWidth);
		addItemToMultipleTextField(0, null);
	}
	
	/**
	 * Explicitly focus/unfocus this widget.
	 * 
	 * @param focused Whether this widget should 
	 * take focus or release it.
	 * @param rowIndex The row index to be focused.
	 */
	public void setFocus(final boolean focused, final int rowIndex){		
		// use Scheduler to fix the set focus problem.
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			public void execute() {
				if(rowIndex < boxList.size() && rowIndex > -1){
					getTextBoxByIndex(rowIndex).setFocus(focused);
				} else if(!boxList.isEmpty()){
					getTextBoxByIndex(0).setFocus(focused);
				}
			}
		});
	}

	/**
	 * Add an Text box to the table of the {@link MultipleTextField}
	 * and to the Box list with the buttons: add, remove, up, down.
	 * 
	 * @param rowClickedIndex The index of the row to add 
	 * the TextBox.
	 * @param rowContent The content of the row.
	 */
	private void addItemToMultipleTextField(final int rowIndex, String rowContent) {
		// create the new text box
		TextBox textBox = new TextBox();
		textBox.setStyleName("textBox");
	    textBox.setSize(txtWidth.toString() + "px", "40px");
	    
	    // set focus on this Box
	    setFocus(true, rowIndex);
        
	    // set the content of the row (can be null)
	    textBox.setText(rowContent);

	    // add the TextBox to the list in the specified position
	    boxList.add(rowIndex, textBox);	    
	    
		// Create the images that appears ahead the box (add, remove, up, down)
		Image addImg = new Image("images/add.png");
		addImg.setTitle("Add Item");
		addImg.setStyleName("imageLink");
		
		Image removeImg = new Image("images/remove.png");
		removeImg.setTitle("Remove Item");
		removeImg.setStyleName("imageLink");
		
		Image upImg = new Image("images/up.png");
		upImg.setTitle("Move Up");
		upImg.setStyleName("imageLink");
		
		Image downImg = new Image("images/down.png");
		downImg.setTitle("Move Down");
		downImg.setStyleName("imageLink");
	
		// add item event
		addImg.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
		        int index = table.getCellForEvent(event).getRowIndex();
		        // add the TextBox to the box list, 
		        // one position below to the row clicked
				addItemToMultipleTextField(index+1, null);
			}
		});
		
		// remove item event
		removeImg.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				removeItemFromMultipleTextField(event);
			}
		});

		// move item up event
		upImg.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				moveMultipleTextFieldItemUp(event);
			}
		});
		
		// move item down event
		downImg.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				moveMultipleTextFieldItemDown(event);
			}
		});

		int numRows = table.getRowCount();
		
		// creates the row number label
		Label label = new Label(numRows+1 + ":");
		label.setStyleName("textSmall");
		table.setWidget(numRows, 0, label);
		
	    // add icons to the new row
	    table.setWidget(numRows, 2, addImg);
	    table.setWidget(numRows, 3, removeImg);
	    table.setWidget(numRows, 4, upImg);
	    table.setWidget(numRows, 5, downImg);
	    
		// text box cell align
		CellFormatter format = table.getCellFormatter();
		format.setVerticalAlignment(numRows, TEXBOX_INDEX, 
				HasVerticalAlignment.ALIGN_MIDDLE);
		format.setHorizontalAlignment(numRows, TEXBOX_INDEX, 
				HasHorizontalAlignment.ALIGN_CENTER);
		
	    // move the row bellow, rows from the rowIndex
	    for(int i=numRows; i>rowIndex; i--){
	        Widget current = new Widget();
	        
	        // move the text one row bellow in the table
	        current = table.getWidget(i-1, 1);
	        table.setWidget(i, TEXBOX_INDEX, current);
	    }
	    
	    // add the new text box to the table, bellow the row clicked
	    table.setWidget(rowIndex, TEXBOX_INDEX, textBox);
	}
	
	/**
	 * Remove an item off the text box list and  off to the 
	 * table of the {@link MultipleTextField} after a click event.
	 * 
	 * @param click event
	 */
	private void removeItemFromMultipleTextField(ClickEvent event){
		// get the index of the row clicked
        int removedIndex = table.getCellForEvent(event).getRowIndex();
        
	    if(table.getRowCount() > 1){
	        // remove from the list
	        boxList.remove(removedIndex);
	        // remove from the table
	        table.removeRow(removedIndex);	
	        
	        // increment the row number label
	        for(int i=removedIndex; i<table.getRowCount(); i++){
	    		((Label)(table.getWidget(i, 0))).setText(i+1 + ":");
	        }
        }
	    else{
	    	// clean the text box
	    	((TextBox)(table.getWidget(0, TEXBOX_INDEX))).setText("");
	    }
	}
	
	/**
	 * Move an item up to the text box list and to the table
	 * of the {@link MultipleTextField} after a click event.
	 * 
	 * @param click event
	 */
	private void moveMultipleTextFieldItemUp(ClickEvent event){
		// get the index of the row clicked
        int index = table.getCellForEvent(event).getRowIndex();
		
        Widget current, above;

        // add up
        if(index > 0){ 
        	// move the item one position behind
        	boxList.add(index-1, boxList.get(index));
        	// remove from the list (now the index increased +1)
        	boxList.remove(index+1);
	
        	// move the text one row above in the table
        	current = table.getWidget(index, TEXBOX_INDEX);
        	above = table.getWidget(index-1, TEXBOX_INDEX);
        	table.setWidget(index-1, TEXBOX_INDEX, current);
        	table.setWidget(index, TEXBOX_INDEX, above);
        }
	}
	
	/**
	 * Move an item down to the text box list and to the table
	 * of the {@link MultipleTextField} after a click event.
	 * 
	 * @param click event
	 */
	private void moveMultipleTextFieldItemDown(ClickEvent event){
		// get the index of the row clicked
        int index = table.getCellForEvent(event).getRowIndex();
		
        Widget current, bellow;

        // add down
        if(index < table.getRowCount()-1){ 
        	// move the item one position ahead
        	boxList.add(index+2, boxList.get(index));
        	// remove from the list
        	boxList.remove(index);
        	
        	// move the text one row bellow in the table
        	current = table.getWidget(index, TEXBOX_INDEX);
        	bellow = table.getWidget(index+1, TEXBOX_INDEX);
        	table.setWidget(index+1, TEXBOX_INDEX, current);
        	table.setWidget(index, TEXBOX_INDEX, bellow);
        }
	}
}
