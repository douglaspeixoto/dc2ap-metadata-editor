package dc2ap.client.ui.components;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import dc2ap.client.services.UIService;


/**
 * This UI composite the element label menu, shown on the bottom
 * of each element of the main page. Containing its obligatoriness,
 * occurrence and value type.
 * 
 * @author Douglas A. Peixoto - 05/2012
 *
 */
public class ElementLabel extends Composite {
	private Label label = new Label("[] [] []");
	
	public ElementLabel(final String obligatoriness, final String occurrence, 
			final String valueType, final String size) {
		label.setStyleName("textLabelMenu");
		initWidget(label);
		
		// set the properties
		label.setWidth(size);
		label.setText(obligatoriness + " " + 
				 occurrence + " " + valueType);
		
		// add pop up event to the element label menu
		label.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				UIService.showLabelMenuPopup(
						obligatoriness, occurrence, valueType, event);
			}
		});
	}

}
