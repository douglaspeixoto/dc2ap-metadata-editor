package dc2ap.client.ui;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import dc2ap.client.services.PanelService;
import dc2ap.client.services.UIService;
import dc2ap.client.ui.components.ButtonPanel;
import dc2ap.client.ui.components.CenterPanel;
import dc2ap.client.ui.components.MenuPanel;
import dc2ap.client.ui.components.PreviousNextPanel;
import dc2ap.client.ui.components.TreePanel;

/**
 * This is the main UI class of this project.
 * Contain a main panel DockLayoutPanel that fits 
 * all the components of the page.
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
public class MetadataEditor extends Composite {
	// Components
	private CenterPanel centerPanel = new CenterPanel();
	private TreePanel treePanel = new TreePanel();
	private MenuPanel menuPanel = new MenuPanel();
	private ButtonPanel buttonPanel = new ButtonPanel();
	private PreviousNextPanel previousNextPanel = new PreviousNextPanel();
	
	public MetadataEditor() {
		// Initialize the element Panels
		PanelService.initPanels();
		
		final DockLayoutPanel mainPanel = new DockLayoutPanel(Unit.PX);
		mainPanel.setStyleName("gwt-DockLayoutPanel");
		mainPanel.setSize("1230px", "750px");
		
		// Add the Menu Panel to the top panel
		final DockLayoutPanel topPanel = new DockLayoutPanel(Unit.EM);
		topPanel.setStyleName("gwt-DockLayoutPanel");
		mainPanel.addNorth(topPanel, 30.0);
		topPanel.add(menuPanel);

		// Add the Tree Panel to the left panel
		final DockLayoutPanel leftPanel = new DockLayoutPanel(Unit.PX);
		leftPanel.setStyleName("gwt-DockLayoutPanel");
		mainPanel.addWest(leftPanel, 252.0);
		leftPanel.add(treePanel);
	
		// Add the Buttons Panel to the main panel
		final DockLayoutPanel bottomPanel1 = new DockLayoutPanel(Unit.EM);
		bottomPanel1.setStyleName("gwt-DockLayoutPanelUp");
		mainPanel.addSouth(bottomPanel1, 50.0);
		bottomPanel1.add(buttonPanel);

		// Add the Previous/Next Menu Panel to the main panel
		final DockLayoutPanel bottomPanel3 = new DockLayoutPanel(Unit.EM);
		bottomPanel3.setStyleName("dialogVPanel");
		mainPanel.addSouth(bottomPanel3, 65.0);
		bottomPanel3.add(previousNextPanel);
		
		// Add the center panel
		mainPanel.add(centerPanel);
		
		this.ensureDebugId("metadataEditor");
		initWidget(mainPanel);
	}
	
	/**
	 * Clear new editor (only clean all panels), 
	 * called when create a new project.
	 */
	public void createNewEditor(){
		UIService.clearAllFields(false);
		UIService.setDefaultFields();
		PanelService.resetCurrentPanel();
	}

}
