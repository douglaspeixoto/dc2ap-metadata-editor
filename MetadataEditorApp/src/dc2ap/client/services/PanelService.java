package dc2ap.client.services;

import com.google.gwt.core.client.Scheduler;

import dc2ap.client.ui.components.CenterPanel;
import dc2ap.client.ui.panels.ConsequencesPanel;
import dc2ap.client.ui.panels.CoveragePanel;
import dc2ap.client.ui.panels.CreatorPanel;
import dc2ap.client.ui.panels.DatePanel;
import dc2ap.client.ui.panels.DescriptionPanel;
import dc2ap.client.ui.panels.FormatPanel;
import dc2ap.client.ui.panels.GenericPanel;
import dc2ap.client.ui.panels.HistoryPanel;
import dc2ap.client.ui.panels.IdentifierPanel;
import dc2ap.client.ui.panels.ModelingPanel;
import dc2ap.client.ui.panels.PublisherPanel;
import dc2ap.client.ui.panels.RelationPanel;
import dc2ap.client.ui.panels.RequirementsPanel;
import dc2ap.client.ui.panels.ResultingPanel;

/**
 * This  service contains all the elements Panels of this application, 
 * in order to keep only one instance of each panel "Singleton". 
 * Always access panels through this service. Call the method InitPanel() 
 * in the onModuleLoad() method in order to set some panels properties.
 * 
 * @author Douglas A. Peixoto - 07/2012
 */
public class PanelService {
	// provides unique access to the panels
	private static GenericPanel currentPanel = null;
	private final static IdentifierPanel identifierPanel = new IdentifierPanel();
	private final static CreatorPanel creatorPanel = new CreatorPanel();
	private final static DescriptionPanel descriptionPanel = new DescriptionPanel();
	private final static PublisherPanel publisherPanel = new PublisherPanel();
	private final static DatePanel datePanel = new DatePanel();
	private final static FormatPanel formatPanel = new FormatPanel();
	private final static RelationPanel relationPanel = new RelationPanel();
	private final static CoveragePanel coveragePanel = new CoveragePanel();
	private final static HistoryPanel historyPanel = new HistoryPanel();
	private final static RequirementsPanel requirementsPanel = new RequirementsPanel();
	private final static ModelingPanel modelingPanel = new ModelingPanel();
	private final static ResultingPanel resultingPanel = new ResultingPanel();
	private final static ConsequencesPanel consequencesPanel = new ConsequencesPanel();
	
	/**
	 * Get the Current Panel of the main page
	 * 
	 * @return Current Panel
	 */
	public static synchronized GenericPanel getCurrentPanel() {
		if(currentPanel == null){
			resetCurrentPanel();
		}
		return currentPanel;
	}
	/**
	 * Set the Current Panel of the main page and fits
	 * the current panel into the center panel.
	 * 
	 * @param currentPanel
	 */
	public static synchronized void setCurrentPanel(GenericPanel currentPanel) {
		if(currentPanel != null){
			PanelService.currentPanel = currentPanel;
			// fits the panel into the center panel
			CenterPanel.setPanel(currentPanel);			
		}
	}
	/**
	 * Reset the Current Panel of the main page, 
	 * set the Identifier panel as default and fits
	 * it into the center panel.
	 * 
	 * @param currentPanel
	 */
	public static synchronized void resetCurrentPanel() {
		setCurrentPanel(identifierPanel);
		setCurrentPanelFocus(true);
	}
	/**
	 * Get the Identifier Panel of the main page
	 * 
	 * @return Identifier Panel
	 */
	public static synchronized IdentifierPanel getIdentifierPanel() {
		return identifierPanel;
	}
	/**
	 * Get the Creator Panel of the main page
	 * 
	 * @return Creator Panel
	 */
	public static synchronized CreatorPanel getCreatorPanel() {
		return creatorPanel;
	}
	/**
	 * Get the Description Panel of the main page
	 * 
	 * @return Description Panel
	 */
	public static synchronized DescriptionPanel getDescriptionPanel() {
		return descriptionPanel;
	}
	/**
	 * Get the Publisher Panel of the main page
	 * 
	 * @return Publisher Panel
	 */
	public static synchronized PublisherPanel getPublisherPanel() {
		return publisherPanel;
	}
	/**
	 * Get the Date Panel of the main page
	 * 
	 * @return Date Panel
	 */
	public static synchronized DatePanel getDatePanel() {
		return datePanel;
	}
	/**
	 * Get the Format Panel of the main page
	 * 
	 * @return Format Panel
	 */
	public static synchronized FormatPanel getFormatPanel() {
		return formatPanel;
	}
	/**
	 * Get the Relational Panel of the main page
	 * 
	 * @return Relational Panel
	 */
	public static synchronized RelationPanel getRelationPanel() {
		return relationPanel;
	}
	/**
	 * Get the Coverage Panel of the main page
	 * 
	 * @return Coverage Panel
	 */
	public static synchronized CoveragePanel getCoveragePanel() {
		return coveragePanel;
	}
	/**
	 * Get the History Panel of the main page
	 * 
	 * @return History Panel
	 */
	public static synchronized HistoryPanel getHistoryPanel() {
		return historyPanel;
	}
	/**
	 * Get the Requirements Panel of the main page
	 * 
	 * @return Requirements Panel
	 */
	public static synchronized RequirementsPanel getRequirementsPanel() {
		return requirementsPanel;
	}
	/**
	 * Get the Modeling Panel of the main page
	 * 
	 * @return Modeling Panel
	 */
	public static synchronized ModelingPanel getModelingPanel() {
		return modelingPanel;
	}
	/**
	 * Get the Resulting Panel of the main page
	 * 
	 * @return Resulting Panel
	 */
	public static synchronized ResultingPanel getResultingPanel() {
		return resultingPanel;
	}
	/**
	 * Get the Consequences Panel of the main page
	 * 
	 * @return Consequences Panel
	 */
	public static synchronized ConsequencesPanel getConsequencesPanel() {
		return consequencesPanel;
	}
	
	/**
	 * Initialize all element panels used here.
	 * Set the previous and the next panels of each one.
	 * Also initialize the center panel, fit the current
	 * panel into it.
	 */
	public static void initPanels(){
		// Set the current panel
		currentPanel = identifierPanel;
		CenterPanel.setPanel(currentPanel);
		
		identifierPanel.setPreviousPanel(null);
		identifierPanel.setNextPanel(creatorPanel);
		
		creatorPanel.setPreviousPanel(identifierPanel);
		creatorPanel.setNextPanel(descriptionPanel);
		
		descriptionPanel.setPreviousPanel(creatorPanel);
		descriptionPanel.setNextPanel(publisherPanel);
		
		publisherPanel.setPreviousPanel(descriptionPanel);
		publisherPanel.setNextPanel(datePanel);
		
		datePanel.setPreviousPanel(publisherPanel);
		datePanel.setNextPanel(formatPanel);
		
		formatPanel.setPreviousPanel(datePanel);
		formatPanel.setNextPanel(relationPanel);
		
		relationPanel.setPreviousPanel(formatPanel);
		relationPanel.setNextPanel(coveragePanel);
		
		coveragePanel.setPreviousPanel(relationPanel);
		coveragePanel.setNextPanel(historyPanel);
		
		historyPanel.setPreviousPanel(coveragePanel);
		historyPanel.setNextPanel(requirementsPanel);
		
		requirementsPanel.setPreviousPanel(historyPanel);
		requirementsPanel.setNextPanel(modelingPanel);
		
		modelingPanel.setPreviousPanel(requirementsPanel);
		modelingPanel.setNextPanel(resultingPanel);
		
		resultingPanel.setPreviousPanel(modelingPanel);
		resultingPanel.setNextPanel(consequencesPanel);
		
		consequencesPanel.setPreviousPanel(resultingPanel);
		consequencesPanel.setNextPanel(null);
	}
	
	/**
	 * Set focus on the main field of the current panel.
	 * 
	 * @param focused Whether the field should take focus or release it
	 */
	public static void setCurrentPanelFocus(final boolean focused){
		// use Scheduler to fix the set focus problem.
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
			public void execute() {
				currentPanel.setMainFieldFocus(focused);
			}
		});
	}
}
