package dc2ap.client.services;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;;

/**
 * Service that embed text files into the application by  
 * specifying an associated TextSource within a ClientBundle.
 * 
 * @author Douglas A. Peixoto - 06/2012
 */
public interface TextSource extends ClientBundle {
	/**
	 * An instance of this class
	 */
	public final TextSource INSTANCE = GWT.create(TextSource.class);
	
	/**
	 * Languages source.
	 * Each token is separated by a 'tab' character
	 */
	@Source("languages")
	TextResource languagesSource();
}
