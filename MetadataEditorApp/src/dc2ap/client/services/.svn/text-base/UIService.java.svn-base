package dc2ap.client.services;

import java.util.List;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HTMLTable.CellFormatter;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import dc2ap.client.entities.Languages;
import dc2ap.client.ui.components.InfoDialogBox;

/**
 * Implements the common methods used in the UI panels and components.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public class UIService {
	// index of the text box in the table
	private static final int TEXBOX_INDEX = 1;
	// number of days in each month
	private static int[] monthDays = 
		{0,31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	// unique instance of the Languages, to avoid read this more than once from the file
//	private static final Languages languages = readLanguages();
	
	/**
	 * Add an Text box to the flex table and to the list.
	 * with the buttons: add, remove, up, down.
	 * 
	 * @param flexTable
	 * @param boxList
	 * @param eventCell
	 * @param txtWidth - width of the Text Box
	 */
	public static void addItemToTable(final FlexTable flexTable, final List<TextBox> boxList
			, final ClickEvent eventCell, final Integer txtWidth) {
		// create the new text box
		TextBox textBox = new TextBox();
		textBox.setStyleName("textBox");
	    textBox.setSize(txtWidth.toString() + "px", "40px");
	    textBox.setFocus(true);
		
	    // get the index of the row clicked
	    int index;
        if(eventCell != null){
        	index = flexTable.getCellForEvent(eventCell).getRowIndex();
        } else{ // for the clear or add first field case
        	index = -1;
        }
        
	    // add to the list, one position ahead to the row clicked
	    boxList.add(index+1, textBox);		
		
		// Create the images that appears after the box (add, remove, up, down)
		Image addImg = new Image("images/add.png");
		addImg.setTitle("Add Item");
		addImg.setStyleName("imageLink");
		
		Image removeImg = new Image("images/remove.png");
		removeImg.setTitle("Remove Item");
		removeImg.setStyleName("imageLink");
		
		Image upImg = new Image("images/up.png");
		upImg.setTitle("Move Up");
		upImg.setStyleName("imageLink");
		
		Image downImg = new Image("images/down.png");
		downImg.setTitle("Move Down");
		downImg.setStyleName("imageLink");
		
		// add item event
		addImg.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				addItemToTable(flexTable, boxList, event, txtWidth);
			}
		});
		
		// remove item event
		removeImg.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				removeItemFromTable(flexTable, boxList, event);
			}
		});

		// move item up event
		upImg.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				moveTableItemUp(flexTable, boxList, event);
			}
		});
		
		// move item down event
		downImg.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				moveTableItemDown(flexTable, boxList, event);
			}
		});

		int numRows = flexTable.getRowCount();
		
		// creates the row number label
		Label label = new Label(numRows+1 + ":");
		label.setStyleName("textSmall");
		flexTable.setWidget(numRows, 0, label);
		
	    // add icons to the new row
	    flexTable.setWidget(numRows, 2, addImg);
	    flexTable.setWidget(numRows, 3, removeImg);
	    flexTable.setWidget(numRows, 4, upImg);
	    flexTable.setWidget(numRows, 5, downImg);
	    
		// text box cell align
		CellFormatter format = flexTable.getCellFormatter();
		format.setVerticalAlignment(numRows, TEXBOX_INDEX, 
				HasVerticalAlignment.ALIGN_MIDDLE);
		format.setHorizontalAlignment(numRows, TEXBOX_INDEX, 
				HasHorizontalAlignment.ALIGN_CENTER);
		
	    // move the row bellow, rows from the row clicked+1
	    for(int i=numRows; i>index+1; i--){
	        Widget current = new Widget();
	        
	        // move the text one row bellow in the table
	        current = flexTable.getWidget(i-1, 1);
	        flexTable.setWidget(i, TEXBOX_INDEX, current);
	    }
	    
	    // add the new text box to the table, bellow the row clicked
	    flexTable.setWidget(index+1, TEXBOX_INDEX, textBox);
	}
	
	/**
	 * Remove an item off the text box list and 
	 * off to the flex table.
	 * 
	 * @param flexTable
	 * @param boxList
	 * @param click event
	 */
	public static void removeItemFromTable(final FlexTable flexTable, 
		final List<TextBox> boxList, ClickEvent event){
		// get the index of the row clicked
        int removedIndex = flexTable.getCellForEvent(event).getRowIndex();
        
	    if(flexTable.getRowCount() > 1){
	        // remove from the list
	        boxList.remove(removedIndex);
	        // remove from the table
	        flexTable.removeRow(removedIndex);	
	        
	        // increment the row number label
	        for(int i=removedIndex; i<flexTable.getRowCount(); i++){
	    		((Label)(flexTable.getWidget(i, 0))).setText(i+1 + ":");
	        }
        }
	    else{
	    	// clean the text box
	    	((TextBox)(flexTable.getWidget(0, TEXBOX_INDEX))).setText("");
	    }
	}
	
	/**
	 * Move an item up to the text box list and to the flex table.
	 * 
	 * @param flexTable
	 * @param boxList
	 * @param click event
	 */
	public static void moveTableItemUp(final FlexTable flexTable, 
		final List<TextBox> boxList, ClickEvent event){
		// get the index of the row clicked
        int index = flexTable.getCellForEvent(event).getRowIndex();
		
        Widget current, above;

        // add up
        if(index > 0){ 
        	// move the item one position behind
        	boxList.add(index-1, boxList.get(index));
        	// remove from the list (now the index increased +1)
        	boxList.remove(index+1);
	
        	// move the text one row above in the table
        	current = flexTable.getWidget(index, TEXBOX_INDEX);
        	above = flexTable.getWidget(index-1, TEXBOX_INDEX);
        	flexTable.setWidget(index-1, TEXBOX_INDEX, current);
        	flexTable.setWidget(index, TEXBOX_INDEX, above);
        }
	}
	
	/**
	 * Move an item down to the text box list and to the flex table.
	 * 
	 * @param flexTable
	 * @param boxList
	 * @param click event
	 */
	public static void moveTableItemDown(final FlexTable flexTable, 
			final List<TextBox> boxList, ClickEvent event){
		// get the index of the row clicked
        int index = flexTable.getCellForEvent(event).getRowIndex();
		
        Widget current, bellow;

        // add down
        if(index < flexTable.getRowCount()-1){ 
        	// move the item one position ahead
        	boxList.add(index+2, boxList.get(index));
        	// remove from the list
        	boxList.remove(index);
        	
        	// move the text one row bellow in the table
        	current = flexTable.getWidget(index, TEXBOX_INDEX);
        	bellow = flexTable.getWidget(index+1, TEXBOX_INDEX);
        	flexTable.setWidget(index+1, TEXBOX_INDEX, current);
        	flexTable.setWidget(index, TEXBOX_INDEX, bellow);
        }
	}
	
	/**
	 * Create and show the element information pop up. 
	 * Event of the (?) Icon.
	 * 
	 * @param headerText Header text of the pop up
	 * @param name Element Tag name
	 * @param height Page height
	 */
	public static void showInfoPopup(String headerText, String name, String height){
		// create a dialog box to fit the info
		final InfoDialogBox infoDialogBox = 
			new InfoDialogBox(headerText, name, height);
		
		infoDialogBox.show();
	}
	
	/**
	 * Create and show the pop up shown on the bottom of the element name,
	 * with the element properties.
	 * 
	 * @param obligatoriness
	 * @param occurance
	 * @param valueType
	 * @param event Click Event
	 */
	public static void showLabelMenuPopup(String obligatoriness, String occurrence, 
			String valueType, ClickEvent event){
		// Create a basic pop up widget
	    final DecoratedPopupPanel simplePopup = new DecoratedPopupPanel(true);
	    simplePopup.ensureDebugId("labelMenuPopup");
	    
 		String oblig = "", occur = "", valt = "";
 		
		// test obligatoriness
		if("[M]".equals(obligatoriness)){
			oblig = "Mandatory";
		} else if("[O]".equals(obligatoriness)){
			oblig = "Optional"; 
		} else if("[Cd]".equals(obligatoriness)){
			oblig = "Conditional"; 
		}
		// test occurrence
		if("[S]".equals(occurrence)){
			occur = "Single";
		} else if("[Mu]".equals(occurrence)){
			occur = "Multiple"; 
		}
		// test value type
		if("[St]".equals(valueType)){
			valt = "String";
		} else if("[D]".equals(valueType)){
			valt = "Date"; 
		} else if("[U]".equals(valueType)){
			valt = "URI"; 
		} else if("[N]".equals(valueType)){
			valt = "Null"; 
		} else if("[UNS]".equals(valueType)){
			valt = "URI, Number or String"; 
		} else if("[US]".equals(valueType)){
			valt = "URI or String"; 
		}
		
	    // Create a table grid to layout the content
		FlexTable table = new FlexTable();
		table.setCellSpacing(5);

		// add the labels to the grid
		Label label1 = new Label("Obligatoriness: ");
		Label labelOb = new Label(obligatoriness + " - " + oblig);
		label1.setStyleName("textBoldSmall");
		labelOb.setStyleName("textSmall");
		table.setWidget(0, 0, label1);
		table.setWidget(0, 1, labelOb);
		
		Label label2 = new Label("Occurance: ");
		Label labelOc = new Label(occurrence + " - " + occur);
		label2.setStyleName("textBoldSmall");
		labelOc.setStyleName("textSmall");
		table.setWidget(1, 0, label2);
		table.setWidget(1, 1, labelOc);
		
		Label label3 = new Label("Value Type: ");
		Label labelVT = new Label(valueType + " - " + valt);
		label3.setStyleName("textBoldSmall");
		labelVT.setStyleName("textSmall");
		table.setWidget(2, 0, label3);
		table.setWidget(2, 1, labelVT);
		
		// set the table into the pop up
		simplePopup.add(table);

		// Reposition the pop up relative to the label menu
	    Widget source = (Widget) event.getSource();
	    int left = source.getAbsoluteLeft() + 10;
	    int top = source.getAbsoluteTop() + 10;
	    simplePopup.setPopupPosition(left, top);
	    
	    // Show the pop up
	    simplePopup.show();
	}
	
	/**
	 * Read from the file and return a Languages object 
	 * with the languages Id and languages Print Name.
	 * 
	 * @return Languages list with its Id and Name
	 */
/*	private static Languages readLanguages(){
		Languages languages = new Languages(); 
		
		try {
			// read the languages from the file using the ClientBundle
			final TextSource textSource = TextSource.INSTANCE;
			String text = textSource.languagesSource().getText();
			
			// peeeeensa numa gambiarra :P
			text = text.replace('\n', '\t');
			String[] tokens = text.split("\t");

			for(int i=0; i<tokens.length; i+=3){
				languages.getIdsOracle().add(tokens[i]);
				languages.getNamesOracle().add(tokens[i+1]);
				
				languages.getIdsStringList().add(tokens[i]);
				languages.getNamesStringList().add(tokens[i+1]);
			}
		} catch (Exception e) {
			Window.alert("Error opening the languages file.");
		}
		
		return languages;
	}
*/	
	/**
	 * Return a Languages object with the languages Id and Print Name.
	 * See more in: http://www.sil.org/iso639-3/codes.asp
	 * 
	 * @return Languages with languages Id and languages Name
	 */
/*	public static final Languages getLanguages(){
		return languages;
	}
*/	
	/**
	 * Return the Value related with a language Label (String size=3). 
	 * 
	 * @param languageLabel The name of the language to be searched.
	 * @return The language Value.
	 */
/*	public static String getLanguageValueByLanguageLabel(String languageLabel){
		for(String aux : languages.getNamesStringList()){
			if(aux.equals(languageLabel)){
				int index = languages.getNamesStringList().indexOf(aux);
				return languages.getIdsStringList().get(index);
			}
		}
		
		return "";
	}
*/
	/**
	 * Return the Label related with a language Value. 
	 * 
	 * @param languageName The Value of the language to be searched.
	 * @return The language Label.
	 */
/*	public static String getLanguageLabelByLanguageValue(String languageValue){
		for(String aux : languages.getIdsStringList()){
			if(aux.equals(languageValue)){
				int index = languages.getIdsStringList().indexOf(aux);
				return languages.getNamesStringList().get(index);
			}
		}
		
		return "";
	}
*/	
	/**
	 * Clear the fields of the current panel.
	 */
	public static void clearCurrentPanel(){
		PanelService.getCurrentPanel().clearFields();
		PanelService.setCurrentPanelFocus(true);
	}
	
	/**
	 * Clear all fields of all panels.
	 */
	public static void clearAllFields(){
		if(Window.confirm("Are you sure to want to clear all fields?")){
			PanelService.getIdentifierPanel().clearFields(); 
			PanelService.getCreatorPanel().clearFields();
			PanelService.getDescriptionPanel().clearFields();
			PanelService.getPublisherPanel().clearFields();
			PanelService.getDatePanel().clearFields();
			PanelService.getFormatPanel().clearFields();
			PanelService.getRelationPanel().clearFields();
			PanelService.getCoveragePanel().clearFields();
			PanelService.getHistoryPanel().clearFields();
			PanelService.getRequirementsPanel().clearFields();
			PanelService.getModelingPanel().clearFields();
			PanelService.getResultingPanel().clearFields();
			PanelService.getConsequencesPanel().clearFields();
			
			PanelService.setCurrentPanelFocus(true);
		}
	}
	
	/**
	 * Verify if the String is a valid date.
	 * Obs: Date format must be (yyyy-MM-dd)
	 * 
	 * @return True if the string is a valid date,
	 * otherwise returns false.
	 */
	public static boolean verifyIfStringIsADate(String date){
		String[] tokens = date.split("-");

		if(tokens.length < 3){
			return false;
		} 
		
		String y = tokens[0];
		String m = tokens[1];
		String d = tokens[2];
		
		int year, month, day;
		
		try {
			year = Integer.parseInt(y);
			month = Integer.parseInt(m);
			day = Integer.parseInt(d);
		} catch (NumberFormatException e) {
			return false;
		}
		
		// leap year
		if((year%4==0 && year%100!=0) || year%400==0){
			monthDays[2] = 29;
		}
		
		// Check the year (4 digits)
		if(y.length() != 4 || year < 0){
			return false;
		}
		// Check the month (2 digits)
		if(m.length() != 2 || month < 1 || month > 12){
			return false;
		}
		// Check the day (2 digits)
		if(d.length() != 2 || day < 0 || day > monthDays[month]){
			return false;
		}
		
		return true;
	}
}
