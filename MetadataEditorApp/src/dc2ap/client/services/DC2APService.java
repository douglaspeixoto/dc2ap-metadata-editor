package dc2ap.client.services;

import java.util.Date;
import java.util.List;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.rpc.AsyncCallback;
import dc2ap.client.entities.DC2AP;
import dc2ap.client.rpc.RPCService;
import dc2ap.client.rpc.RPCServiceAsync;
import dc2ap.client.ui.components.FileUploadDialogBox;
import dc2ap.client.ui.components.MultipleTextField;
import dc2ap.client.ui.components.XmlDialogBox;
import dc2ap.client.ui.components.ValidationDialogBox;
import dc2ap.client.ui.panels.ConsequencesPanel;
import dc2ap.client.ui.panels.CoveragePanel;
import dc2ap.client.ui.panels.CreatorPanel;
import dc2ap.client.ui.panels.DatePanel;
import dc2ap.client.ui.panels.DescriptionPanel;
import dc2ap.client.ui.panels.FormatPanel;
import dc2ap.client.ui.panels.HistoryPanel;
import dc2ap.client.ui.panels.IdentifierPanel;
import dc2ap.client.ui.panels.ModelingPanel;
import dc2ap.client.ui.panels.PublisherPanel;
import dc2ap.client.ui.panels.RelationPanel;
import dc2ap.client.ui.panels.RequirementsPanel;
import dc2ap.client.ui.panels.ResultingPanel;

/**
 * Implement the main services used in this application, such as:
 * Validate metadata. Generate RDF File. Save project. Open project.
 * Some of these services make a RPC (Remote Procedure Call) to the server.
 * 
 * @author Douglas A. Peixoto - 07/2012
 */
public class DC2APService {
	private static String errorLog;
	private static String warningLog;
	private static Integer numErrors;
	private static Integer numWarnings;
	
	// instances of the panels
	private static IdentifierPanel idPanel = PanelService.getIdentifierPanel();
	private static CreatorPanel creatorPanel = PanelService.getCreatorPanel();
	private static DescriptionPanel descrPanel = PanelService.getDescriptionPanel();
	private static PublisherPanel publisherPanel = PanelService.getPublisherPanel(); 
	private static DatePanel datePanel = PanelService.getDatePanel();
	private static FormatPanel formatPanel = PanelService.getFormatPanel(); 
	private static RelationPanel relationPanel = PanelService.getRelationPanel();
	private static CoveragePanel coveragePanel = PanelService.getCoveragePanel();
	private static HistoryPanel historyPanel = PanelService.getHistoryPanel();
	private static RequirementsPanel reqPanel = PanelService.getRequirementsPanel();
	private static ModelingPanel modelPanel = PanelService.getModelingPanel();
	private static ResultingPanel resultingPanel = PanelService.getResultingPanel();
	private static ConsequencesPanel conseqPanel = PanelService.getConsequencesPanel();
	
	// initialize the service proxy which communicate with the server-side.
	private static final RPCServiceAsync RPC_SERVICE = 
		GWT.create(RPCService.class);
	
	/**
	 * Validate the metadata according with the DC2AP specification. 
	 * This function also can be access through WebService.
	 * 
	 * @param errorMessage an error message to be shown in the case of 
	 * non-validation of the metadata.
	 */
	public static void validateMetadata(final String errorMessage){
		// create the object with the current fields from the UI
		DC2AP dc2apObject = createDC2APObject();
		
		// Set up the callback object, to receive the answer from the server
	    AsyncCallback<List<String>> callback = new AsyncCallback<List<String>>() {
	    	  @Override
	    	  public void onFailure(Throwable caught) {
		    	  	Window.alert("Failed to call the service 'ValidationMetadata'  from the server.");
		      }
	    	  
	    	  @Override
		      public void onSuccess(List<String> result) {
		    	  	errorLog = "<br/>";
			  		warningLog = "Please, check the conditions of the following fields:<br/><br/>";
			  		numErrors = 0;
			  		numWarnings = 0;

			  		// create the error log with the answer coming from the server
		    	  	createErrorLog(result);
		    	  	
		    	  	// change the background of the fields (white, yellow or red)
		    	  	changeFieldsBackground();
		    	  	
		    	  	// validation of the conditional fields (it is not done in the server)
			  		validateConditionalMultipleField(publisherPanel.getContributorBox(), "7. Contributor");
			  		validateConditionalSingleField(datePanel.getModifiedBox().getTextBox(), "8.2. Modified");
			  		validateConditionalSingleField(relationPanel.getIsVersionOfBox(), "13.1. Is Version Of");
			  		validateConditionalMultipleField(relationPanel.getIsReplacedByBox(), "13.2. Is Replaced By");
			  		validateConditionalMultipleField(relationPanel.getReplacesBox(), "13.3. Replaces");
			  		validateConditionalSingleField(relationPanel.getAboutBox(), "13.9. About");
			  		validateConditionalMultipleField(coveragePanel.getRightsBox(), "15. Rights");
			  		validateConditionalSingleField(historyPanel.getChangesBox(), "16.4. Changes");
			  		validateConditionalSingleField(reqPanel.getContributionGraphBox(), "17.3.2. Contribution Graph");
			  		validateConditionalMultipleField(reqPanel.getConflictAndResolutionBox(), "17.4. Conflict and Resolution");
			  		
			  		//** Optional fields don't need to be validated **//
			  				
			  		// create a dialog box to show a log error or confirm validation
			  		ValidationDialogBox valDialogBox = 
			  			new ValidationDialogBox(errorMessage, numErrors, numWarnings, errorLog, warningLog);
			  		
			  		// Show the pop up
			  		valDialogBox.center();
		      }
	    };		
	    
	    // make the call to the RPC service to return a error list
	    RPC_SERVICE.validateMetadata(dc2apObject, callback);
	}
	
	/**
	 * Creates the log with the errors found in the 
	 * metadata validation.
	 * 
	 * @param result list of errors found, from the RPC service
	 */
	private static void createErrorLog(List<String> result){
		for(String error : result){
			errorLog += "+ " + error + "<br/>";
		}
		numErrors = result.size();
	}
	
	/**
	 * Verify a single field of the resource, used
	 * on conditional fields, warns the user if this 
	 * field is empty.
	 * 
	 * @param box The textBox of the element
	 * @param fieldName The name of the field
	 */
	private static void validateConditionalSingleField(TextBox box, String fieldName){
		String text = box.getText();
		
		if("".equals(text) || text == null){
			warningLog += "+ Field (" + fieldName + ") is not filled.<br/>";
			numWarnings++;
		}
	}
	
	/**
	 * Verify a multiple field of the resource, used
	 * on conditional fields, warns the user if this 
	 * field is empty.
	 * 
	 * @param multipleField The MultipleTextFild of the element
	 * @param fieldName The name of the field
	 */
	private static void validateConditionalMultipleField(
			MultipleTextField multipleField, String fieldName){
		List<String> list = multipleField.getBoxStringList();
		
		if(list.size() == 0 || list == null){
			warningLog += "+ Field (" + fieldName + ") is not filled.<br/>";
			numWarnings++;
		} 
	}
	
	/**
	 * Generate the RDF script according with the panel fields, call the server, 
	 * and set it into a dialog box (call the method to validate the metadata first).
	 */
	public static void generateRDF(){
		// create the object with the current fields from the UI
		DC2AP dc2apObject = createDC2APObject();
		
		// Set up the callback object, to receive the answer from the server
	    AsyncCallback<String> callback = new AsyncCallback<String>() {
	    	  @Override
	    	  public void onFailure(Throwable caught) {
		    	  	Window.alert("Failed to call the service 'GenerateScriptRDF' from the server.");
		      }

	    	  @Override
	    	  public void onSuccess(String result) {
	    		   if("ValidationException".equals(result)){
	    			   String errorMsg = result + ": It was not possible to create the .rdf file because there are some errors " +
	    			   		"with your metadata. Please, see the error log for more details.";
	    			   validateMetadata(errorMsg);
	    		   } else if("ParserConfigurationException".equals(result) || 
	    				   "TransformerException".equals(result)){
	    		  		Window.alert(result + ": Exception calling the method 'generateScriptRDF' in the server-side.");
	    		   } else{
		    		   result = result
		    		  		.replace("http://www.w3.org/1999/02/22-rdf-syntax-ns#\"", "http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n")
		    		  		.replace("http://www.w3.org/2000/01/rdf-schema#\"", "http://www.w3.org/2000/01/rdf-schema#\"")
		    		  		.replace("http://purl.org/dc/elements/1.1/\"", "http://purl.org/dc/elements/1.1/\"\n")
		    		  		.replace("http://purl.org/dc2ap/elements/\"", "http://purl.org/dc2ap/elements/\"\n")
		    		  		.replace("http://purl.org/dc2ap/type/\"", "http://purl.org/dc2ap/type/\"\n")
		    		  		.replace("http://purl.org/dc2ap/format/\"", "http://purl.org/dc2ap/format/\"\n")
		    		  		.replace("http://purl.org/dc/terms/\"", "http://purl.org/dc/terms/\"\n")
		    		  		.replace("http://purl.org/dc/dcmitype/\"", "http://purl.org/dc/dcmitype/\"\n")
		    		  		.replace("http://rdvocab.info/Elements/\"", "http://rdvocab.info/Elements/\"\n")
		    		  		.replace("http://rdvocab.info/roles/\"", "http://rdvocab.info/roles/\"\n")
		    		  		.replace("http://purl.org/dc2ap/ves/\"", "http://purl.org/dc2ap/ves/\"\n");
		    		  	
		    		   	// change the background of the fields (white, yellow or red)
		    		    changeFieldsBackground();
		    		    
		    		  	// create a dialog box to show the RDF script
				  		XmlDialogBox rdfBox = new XmlDialogBox(result);
				  		
				  		// Show the pop up
				  		rdfBox.center();
	    		   }
	    	  }
	    };
	    
		RPC_SERVICE.generateScriptRDF(dc2apObject, callback);
	}
	
	/**
	 * Change the background of the fields, if the field
	 * is not validated then turn it into red or yellow.
	 */
	private static void changeFieldsBackground(){
		UIService.changeFieldsBackground(idPanel, creatorPanel,
				descrPanel, publisherPanel, datePanel, formatPanel, 
				relationPanel, coveragePanel,historyPanel, reqPanel,
				modelPanel, conseqPanel);
	}
	
	/**
	 * Generate a RDF file and save it to the client disk.
	 * 
	 * @param rdfScript the script of the RDF file
	 * @param fileName the name of the file to be saved (without extension, only the name)
	 * 
	 * @return true if the save was completed
	 */
	public static void saveFileRDF(final String rdfScript, final String fileName){
		
		// Set up the callback object, to receive the answer from the server
	    AsyncCallback<String> callback = new AsyncCallback<String>() {
	    	  @Override
	    	  public void onFailure(Throwable caught) {
		    	  	Window.alert("Failed to call the service 'SaveFileRDF' from the server.");
		      }

	    	  @Override
	    	  public void onSuccess(String result) {
	    		  	if("IOException".equals(result)){
	    		  		Window.alert(result + ": It was not possible to create the file in the server-side.");
	    		  	} else{
	    		  		// file name plus rdf extension
	    		  		final String fullfileName = fileName  + ".rdf";
	    		  		
	    		  		// get the full path of the file in the server
	    		  		final String fileURL = GWT.getHostPageBaseURL() + "temporaryFiles/" + fullfileName;

	    		  		// open the file (download)
	    		  		Window.open(fileURL, "Download File", "");
	    		  		
	    		  		// delete the temporary file created after closes the application
	    		  		Window.addCloseHandler(new CloseHandler<Window>() {
							@Override
							public void onClose(CloseEvent<Window> event) {
								deleteTemporaryFileFromTheServer(fullfileName);
							}
						});
	    		  	}
	    	  }
	    };
		
		RPC_SERVICE.saveFileRDF(rdfScript, fileName, callback);
	}
	
	/**
	 * Open a RDF file chosen by the user and set its fields 
	 * into the application fields.
	 */
	public static void openFileRDF(){
		// Create a dialog box to open the file
		final FileUploadDialogBox fileUploadBox = new FileUploadDialogBox();
		
		/*
		  Call the method 'readFileOpened' in the 'FileUploadDialogBox' class
		*/
		
		// open the dialog box to upload file
		fileUploadBox.center();
	}
	
	/**
	 * Read a RDF file from the server and process its information by
	 * setting its fields into the application fields.
	 * 
	 * This method must be called after the file has been saved to the server.
	 * 
	 * @param fileName Name of the file in the server to be read.
	 */
	public static void readFileOpened(final String fileName){
		// Set up the callback object, to receive the answer from the server
		AsyncCallback<DC2AP> callback = new AsyncCallback<DC2AP>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to call the service 'readFileRDF' from the server.");
			}

			@Override
			public void onSuccess(DC2AP result) {
				if(result == null){
					Window.alert("NullPointerException: Was not possible to read the DC2AP object in the server-side.");
				} else{
					// get the DC2AP element read from the file in the server
    			  	// and open it into the UI fields
    			  	openDC2APOject(result);    		  
    		  }
			}
		};
		
		RPC_SERVICE.readFileRDF(fileName, callback);
	}

	/**
	 * Save the project.
	 */
    public static void saveProject(){
    	// create the object with the current fields from the UI
		DC2AP dc2apObject = createDC2APObject();

		// create the file name plus the current date to avoid duplicates
		Date now = new Date();
		final String fileName = "projectFile_" + now.getTime();
		
		// Set up the callback object, to receive the answer from the server
	    AsyncCallback<String> callback = new AsyncCallback<String>() {
	    	  @Override
	    	  public void onFailure(Throwable caught) {
		    	  	Window.alert("Failed to call the service 'SaveProject' from the server.");
		      }

	    	  @Override
	    	  public void onSuccess(final String result) {
	    		  	if("IOException".equals(result)){
	    		  		Window.alert(result + ": It was not possible to create the file in the server-side.");
	    		  	} else if("TransformerException".equals(result) || "ParserConfigurationException".equals(result)){
	    		  		Window.alert(result + ": It was not possible to create the project file in the server-side.");
	    		  	} else{    		  		
	    		  		// file name plus mde extension
	    		  		final String fullfileName = fileName  + ".mde";
	    		  		
	    		  		// get the full path of the file in the server
	    		  		final String fileURL = GWT.getHostPageBaseURL() + "temporaryFiles/" + fullfileName;

	    		  		// open the file (download)
	    		  		Window.open(fileURL, "Download File", "");
	    		  		
	    		  		// delete the temporary file created after closes the application
	    		  		Window.addCloseHandler(new CloseHandler<Window>() {
							@Override
							public void onClose(CloseEvent<Window> event) {
								deleteTemporaryFileFromTheServer(fullfileName);
							}
						});
	    		  	}
	    	  }
	    };

		RPC_SERVICE.saveProject(dc2apObject, fileName, callback);
    }
    
	/**
	 * Delete a file from the server (called after the user
	 * save closes the application).
	 * 
	 * @param fullFileName the name of the file to be deleted, with its extension.
	 */
	public static void deleteTemporaryFileFromTheServer(final String fullFileName){

		// Set up the callback object, to receive the answer from the server
	    AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
	    	  @Override
	    	  public void onFailure(Throwable caught) {
	    		  GWT.log("Failed to call the service 'DeleteFileRDF' from the server.");
		      }

	    	  @Override
	    	  public void onSuccess(Boolean result) {
	    		  	if(!result){
	    		  		GWT.log("DeleteException: It was not possible to delete the file '" + fullFileName + "' at the server-side.");
	    		  	} 
	    	  }
	    };
	    
		RPC_SERVICE.deleteTemporaryFile(fullFileName, callback);
	}
	
	/**
	 * Create and return a DC2AP object. Set the current 
	 * fields of the UI panels into a DC2AP object.
	 * (Save the fields of the panels into a DC2AP object)
	 * 
	 * @return DC2AP object
	 */
	public static DC2AP createDC2APObject(){
		DC2AP object = new DC2AP();
		
		object.setIdentifier(idPanel.getIdentifierBox().getText());
		object.setTitle(idPanel.getTitleBox().getText());
		object.setAlternativeTitle(idPanel.getAltTitleBox().getBoxStringList());
		object.setCreator(creatorPanel.getCreatorBox().getBoxStringList());
		object.setSubject(creatorPanel.getSubjectBox().getBoxStringList());
		object.setProblem(descrPanel.getProblemBox().getText());
		object.setMotivation(descrPanel.getMotivationBox().getBoxStringList());
		object.setExample(descrPanel.getExampleBox().getBoxStringList());
		object.setKnownUses(descrPanel.getKnownUsesBox().getBoxStringList());
		object.setContext(descrPanel.getContextBox().getText());
		object.setPublisher(publisherPanel.getPublisherBox().getBoxStringList());
		object.setContributor(publisherPanel.getContributorBox().getBoxStringList());
		object.setCreated(datePanel.getCreatedBox().getTextBox().getText());
		object.setModified(datePanel.getModifiedBox().getTextBox().getText());
		object.setType(datePanel.getTypeBox().getText());
		object.setNotation(datePanel.getNotationBox().getText());
		object.setFormat(formatPanel.getFormatList());
		object.setSource(formatPanel.getSourceBox().getText());
		object.setLanguageValue(formatPanel.getLanguageValueBox().getText().toLowerCase());
		object.setLanguageLabel(formatPanel.getLanguageLabelBox().getText().toLowerCase());
		object.setIsVersionOf(relationPanel.getIsVersionOfBox().getText());
		object.setIsReplacedBy(relationPanel.getIsReplacedByBox().getBoxStringList());
		object.setReplaces(relationPanel.getReplacesBox().getBoxStringList());
		object.setIsPartOf(relationPanel.getIsPartOfBox().getBoxStringList());
		object.setHasPart(relationPanel.getHasPartBox().getBoxStringList());
		object.setIsDesignedWith(relationPanel.getIsDesignedWithBox().getBoxStringList());
		object.setShouldAvoid(relationPanel.getShouldAvoidBox().getBoxStringList());
		object.setComplementedBy(relationPanel.getComplementedByBox().getBoxStringList());
		object.setAbout(relationPanel.getAboutBox().getText());
		object.setCoverage(coveragePanel.getCoverageBox().getBoxStringList());
		object.setRights(coveragePanel.getRightsBox().getBoxStringList());
		object.setEventDate(historyPanel.getEventDateBox().getTextBox().getText());
		object.setAuthor(historyPanel.getAuthorBox().getBoxStringList());
		object.setReason(historyPanel.getReasonBox().getText());
		object.setChanges(historyPanel.getChangesBox().getText());
		object.setFunctionalRequirements(reqPanel.getFunctionalRequirementsBox().getBoxStringList());
		object.setNonFunctionalRequirements(reqPanel.getNonFunctionalRequirementsBox().getBoxStringList());
		object.setDependencesAndContributions(reqPanel.getDependenciesAndContributionsBox().getText());
		object.setDependencyGraph(reqPanel.getDependencyGraphBox().getText());
		object.setContributionGraph(reqPanel.getContributionGraphBox().getText());
		object.setConflictAndResolution(reqPanel.getConflictAndResolutionBox().getBoxStringList());
		object.setPrioritiesDiagram(reqPanel.getPrioritiesDiagramBox().getText());
		object.setParticipants(reqPanel.getParticipantsBox().getBoxStringList());		
		object.setUseCaseDiagram(modelPanel.getUseCaseDiagramBox().getText());
		object.setCollaborationSequenceDiagrams(modelPanel.getColaborationSequenceDiagramBox().getBoxStringList());
		object.setActivityStateDiagrams(modelPanel.getActivityStateBox().getBoxStringList());
		object.setClassDiagram(modelPanel.getClassDiagramBox().getText());
		object.setClassDescriptions(modelPanel.getClassDescriptionBox().getBoxStringList());
		object.setRelationshipDescriptions(modelPanel.getRelationshipDescriptionBox().getBoxStringList());
		object.setSolutionVariants(modelPanel.getSolutionVariantsBox().getBoxStringList());
		object.setResultingContext(resultingPanel.getResultingContextBox().getBoxStringList());
		object.setDesignGuidelines(resultingPanel.getDesignGuidelinesBox().getBoxStringList());
		object.setPositive(conseqPanel.getPositiveBox().getBoxStringList());
		object.setNegative(conseqPanel.getNegativeBox().getBoxStringList());
		
		return object;
	}

	/**
	 * This method receive a DC2AP object and set its fields into 
	 * each respective field of the UI panels.
	 * 
	 * @param object The DC2AP object to be opened.
	 */
	public static void openDC2APOject(DC2AP object){
		idPanel.getIdentifierBox().setText(object.getIdentifier());
		idPanel.getTitleBox().setText(object.getTitle());
		idPanel.getAltTitleBox().setBoxStringList(object.getAlternativeTitle());
		creatorPanel.getCreatorBox().setBoxStringList(object.getCreator());
		creatorPanel.getSubjectBox().setBoxStringList(object.getSubject());
		descrPanel.getProblemBox().setText(object.getProblem());
		descrPanel.getMotivationBox().setBoxStringList(object.getMotivation());
		descrPanel.getExampleBox().setBoxStringList(object.getExample());
		descrPanel.getKnownUsesBox().setBoxStringList(object.getKnownUses());
		descrPanel.getContextBox().setText(object.getContext());
		publisherPanel.getPublisherBox().setBoxStringList(object.getPublisher());
		publisherPanel.getContributorBox().setBoxStringList(object.getContributor());
		datePanel.getCreatedBox().getTextBox().setText(object.getCreated());
		datePanel.getModifiedBox().getTextBox().setText(object.getModified());
		datePanel.getTypeBox().setText(object.getType());
		datePanel.getNotationBox().setText(object.getNotation());
		formatPanel.setFormatList(object.getFormat());
		formatPanel.getSourceBox().setText(object.getSource());
		formatPanel.getLanguageValueBox().setText(object.getLanguageValue());
		formatPanel.getLanguageLabelBox().setText(object.getLanguageLabel());
		relationPanel.getIsVersionOfBox().setText(object.getIsVersionOf());
		relationPanel.getIsReplacedByBox().setBoxStringList(object.getIsReplacedBy());
		relationPanel.getReplacesBox().setBoxStringList(object.getReplaces());
		relationPanel.getIsPartOfBox().setBoxStringList(object.getIsPartOf());
		relationPanel.getHasPartBox().setBoxStringList(object.getHasPart());
		relationPanel.getIsDesignedWithBox().setBoxStringList(object.getIsDesignedWith());
		relationPanel.getShouldAvoidBox().setBoxStringList(object.getShouldAvoid());
		relationPanel.getComplementedByBox().setBoxStringList(object.getComplementedBy());
		relationPanel.getAboutBox().setText(object.getAbout());
		coveragePanel.getCoverageBox().setBoxStringList(object.getCoverage());
		coveragePanel.getRightsBox().setBoxStringList(object.getRights());
		historyPanel.getEventDateBox().getTextBox().setText(object.getEventDate());
		historyPanel.getAuthorBox().setBoxStringList(object.getAuthor());
		historyPanel.getReasonBox().setText(object.getReason());
		historyPanel.getChangesBox().setText(object.getChanges());
		reqPanel.getFunctionalRequirementsBox().setBoxStringList(object.getFunctionalRequirements());
		reqPanel.getNonFunctionalRequirementsBox().setBoxStringList(object.getNonFunctionalRequirements());
		reqPanel.getDependenciesAndContributionsBox().setText(object.getDependencesAndContributions());
		reqPanel.getDependencyGraphBox().setText(object.getDependencyGraph());
		reqPanel.getContributionGraphBox().setText(object.getContributionGraph());
		reqPanel.getConflictAndResolutionBox().setBoxStringList(object.getConflictAndResolution());
		reqPanel.getPrioritiesDiagramBox().setText(object.getPrioritiesDiagram());
		reqPanel.getParticipantsBox().setBoxStringList(object.getParticipants());
		modelPanel.getUseCaseDiagramBox().setText(object.getUseCaseDiagram());
		modelPanel.getColaborationSequenceDiagramBox().setBoxStringList(object.getCollaborationSequenceDiagrams());
		modelPanel.getActivityStateBox().setBoxStringList(object.getActivityStateDiagrams());
		modelPanel.getClassDiagramBox().setText(object.getClassDiagram());
		modelPanel.getClassDescriptionBox().setBoxStringList(object.getClassDescriptions());
		modelPanel.getRelationshipDescriptionBox().setBoxStringList(object.getRelationshipDescriptions());
		modelPanel.getSolutionVariantsBox().setBoxStringList(object.getSolutionVariants());
		resultingPanel.getResultingContextBox().setBoxStringList(object.getResultingContext());
		resultingPanel.getDesignGuidelinesBox().setBoxStringList(object.getDesignGuidelines());
		conseqPanel.getPositiveBox().setBoxStringList(object.getPositive());
		conseqPanel.getNegativeBox().setBoxStringList(object.getNegative());
		
		changeFieldsBackground();
	}
	
	/**
	 * Sorry message to be shown in not finished methods :(
	 */
	public static void sorryMessage(){
		Window.alert("Sorry! This is not working yet. :(");
	}
}
