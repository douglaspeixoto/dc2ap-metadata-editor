package dc2ap.client.services;

import java.util.Date;
import java.util.List;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.TextBoxBase;
import com.google.gwt.user.client.ui.Widget;
import dc2ap.client.entities.Languages;
import dc2ap.client.ui.components.InfoDialogBox;
import dc2ap.client.ui.components.MultipleTextField;
import dc2ap.client.ui.panels.ConsequencesPanel;
import dc2ap.client.ui.panels.CoveragePanel;
import dc2ap.client.ui.panels.CreatorPanel;
import dc2ap.client.ui.panels.DatePanel;
import dc2ap.client.ui.panels.DescriptionPanel;
import dc2ap.client.ui.panels.FormatPanel;
import dc2ap.client.ui.panels.HistoryPanel;
import dc2ap.client.ui.panels.IdentifierPanel;
import dc2ap.client.ui.panels.ModelingPanel;
import dc2ap.client.ui.panels.PublisherPanel;
import dc2ap.client.ui.panels.RelationPanel;
import dc2ap.client.ui.panels.RequirementsPanel;

/**
 * Implements the common methods used in the UI panels and components.
 * 
 * @author Douglas A. Peixoto - 05/2012
 */
public class UIService {
	// unique instance of the Languages, to avoid read more than once from the file
    private final static Languages LANGUAGES = readLanguages();
    
	/**
	 * Create and show the element information pop up. 
	 * Event of the (?) Icon.
	 * 
	 * @param headerText Header text of the pop up
	 * @param name Element Tag name
	 * @param height Page height
	 */
	public static void showInfoPopup(String headerText, String name, String height){
		// create a dialog box to fit the info
		final InfoDialogBox infoDialogBox = 
			new InfoDialogBox(headerText, name, height);
		
		infoDialogBox.center();
	}
	
	/**
	 * Create and show the pop up shown on the bottom of the element name,
	 * with the element properties.
	 * 
	 * @param obligatoriness
	 * @param occurance
	 * @param valueType
	 * @param event Click Event
	 */
	public static void showLabelMenuPopup(String obligatoriness, String occurrence, 
			String valueType, ClickEvent event){
		// Create a basic pop up widget
	    final DecoratedPopupPanel simplePopup = new DecoratedPopupPanel(true);
	    simplePopup.ensureDebugId("labelMenuPopup");
	    
 		String oblig = "", occur = "", valt = "";
 		
		// test obligatoriness
		if("[M]".equals(obligatoriness)){
			oblig = "Mandatory";
		} else if("[O]".equals(obligatoriness)){
			oblig = "Optional"; 
		} else if("[Cd]".equals(obligatoriness)){
			oblig = "Conditional"; 
		}
		// test occurrence
		if("[S]".equals(occurrence)){
			occur = "Single";
		} else if("[Mu]".equals(occurrence)){
			occur = "Multiple"; 
		}
		// test value type
		if("[St]".equals(valueType)){
			valt = "String";
		} else if("[D]".equals(valueType)){
			valt = "Date"; 
		} else if("[U]".equals(valueType)){
			valt = "URI"; 
		} else if("[N]".equals(valueType)){
			valt = "Null"; 
		} else if("[UNS]".equals(valueType)){
			valt = "URI, Number or String"; 
		} else if("[US]".equals(valueType)){
			valt = "URI or String"; 
		}
		
	    // Create a table grid to layout the content
		FlexTable table = new FlexTable();
		table.setCellSpacing(5);

		// add the labels to the grid
		Label label1 = new Label("Obligatoriness: ");
		Label labelOb = new Label(obligatoriness + " - " + oblig);
		label1.setStyleName("textBoldSmall");
		labelOb.setStyleName("textSmall");
		table.setWidget(0, 0, label1);
		table.setWidget(0, 1, labelOb);
		
		Label label2 = new Label("Occurance: ");
		Label labelOc = new Label(occurrence + " - " + occur);
		label2.setStyleName("textBoldSmall");
		labelOc.setStyleName("textSmall");
		table.setWidget(1, 0, label2);
		table.setWidget(1, 1, labelOc);
		
		Label label3 = new Label("Value Type: ");
		Label labelVT = new Label(valueType + " - " + valt);
		label3.setStyleName("textBoldSmall");
		labelVT.setStyleName("textSmall");
		table.setWidget(2, 0, label3);
		table.setWidget(2, 1, labelVT);
		
		// set the table into the pop up
		simplePopup.add(table);

		// Reposition the pop up relative to the label menu
	    Widget source = (Widget) event.getSource();
	    int left = source.getAbsoluteLeft() + 10;
	    int top = source.getAbsoluteTop() + 10;
	    simplePopup.setPopupPosition(left, top);
	    
	    // Show the pop up
	    simplePopup.show();
	}

	/**
	 * Read from the file and return a Languages object 
	 * with the languages Id and languages Print Name.
	 * 
	 * @return Languages list with its Id and Name
	 */
	private static Languages readLanguages(){
		Languages languages = new Languages(); 
		
		try {
			// read the languages from the file using the ClientBundle
			final TextSource textSource = TextSource.INSTANCE;
			String text = textSource.languagesSource().getText();

			String[] tokens = text.split("\t");
		
			for(int i=0; i<tokens.length; i+=2){
				languages.getValuesOracle().add(tokens[i]);
				languages.getLabelsOracle().add(tokens[i+1]);
				
				languages.getValuesStringList().add(tokens[i]);
				languages.getLabelsStringList().add(tokens[i+1]);
			}
		} catch (Exception e) {
			GWT.log("Error opening the languages file.");
		}
		
		return languages;
	}
	
	/**
	 * Return a Languages object with the languages Id and Print Name.
	 * See more in: http://www.sil.org/iso639-3/codes.asp
	 * 
	 * @return Languages with languages Id and languages Name
	 */
	public static final Languages getLanguages(){
		return LANGUAGES;
	}
	
	/**
	 * Return the Value related with a language Label (String size=3). 
	 * 
	 * @param languageLabel The name of the language to be searched.
	 * @return The language Value.
	 */
	public static String getLanguageValueByLanguageLabel(String languageLabel){	
		for(String aux : LANGUAGES.getLabelsStringList()){
			if((aux.toLowerCase()).equals(languageLabel.toLowerCase())){
				int index = LANGUAGES.getLabelsStringList().indexOf(aux);	
				return LANGUAGES.getValuesStringList().get(index);
			}
		}
	
		return "";
	}

	/**
	 * Return the Label related with a language Value. 
	 * 
	 * @param languageName The Value of the language to be searched.
	 * @return The language Label.
	 */
	public static String getLanguageLabelByLanguageValue(String languageValue){
		for(String aux : LANGUAGES.getValuesStringList()){
			if((aux.toLowerCase()).equals(languageValue.toLowerCase())){
				int index = LANGUAGES.getValuesStringList().indexOf(aux);		
				return LANGUAGES.getLabelsStringList().get(index);
			}
		}
		
		return "";
	}

	/**
	 * Clear the fields of the current panel.
	 */
	public static void clearCurrentPanel(){
		PanelService.getCurrentPanel().clearFields();
		PanelService.setCurrentPanelFocus(true);
	}
	
	/**
	 * Clear all fields of all panels.
	 * 
	 * @param confirmCleanMsg True if you want to show a confirm message.
	 */
	public static void clearAllFields(boolean confirmCleanMsg){
		if(confirmCleanMsg){
			if(Window.confirm("Are you sure you want to clear all fields?")){
				clearPanels();
			}
		} else{
			clearPanels();
		}
	}
	
	/**
	 * Clear the fields of the panels.
	 */
	private static void clearPanels(){
		PanelService.getIdentifierPanel().clearFields(); 
		PanelService.getCreatorPanel().clearFields();
		PanelService.getDescriptionPanel().clearFields();
		PanelService.getPublisherPanel().clearFields();
		PanelService.getDatePanel().clearFields();
		PanelService.getFormatPanel().clearFields();
		PanelService.getRelationPanel().clearFields();
		PanelService.getCoveragePanel().clearFields();
		PanelService.getHistoryPanel().clearFields();
		PanelService.getRequirementsPanel().clearFields();
		PanelService.getModelingPanel().clearFields();
		PanelService.getResultingPanel().clearFields();
		PanelService.getConsequencesPanel().clearFields();
		
		PanelService.setCurrentPanelFocus(true);
	}
	
	/**
	 * Set the default value of some fields of the panels.
	 */
	public static void setDefaultFields(){
		DatePanel datePanel = PanelService.getDatePanel();
		FormatPanel formatPanel = PanelService.getFormatPanel();
		
		// default - current date
		datePanel.getCreatedBox().setValue(new Date());
		// default - Analysis Pattern
		datePanel.getTypeBox().setText("Analysis Pattern");
		// default - English
		formatPanel.getLanguageLabelBox().setText("English");
		// default - eng
		formatPanel.getLanguageValueBox().setText("eng");
	}
	
	/**
	 * Change the background of the mandatory fields, if the
	 * field is not validated then turn it into red or yellow.
	 */
	public static void changeFieldsBackground(
			IdentifierPanel idPanel, CreatorPanel creatorPanel,
			DescriptionPanel descrPanel, PublisherPanel publisherPanel, 
			DatePanel datePanel, FormatPanel formatPanel, 
			RelationPanel relationPanel, CoveragePanel coveragePanel,
			HistoryPanel historyPanel, RequirementsPanel reqPanel,
			ModelingPanel modelPanel, ConsequencesPanel conseqPanel){
	  	
		// mandatory fields
		changeSingleFieldBackground(idPanel.getIdentifierBox(), true);
		changeSingleFieldBackground(idPanel.getTitleBox(), true);
		changeMultipleFieldBackground(creatorPanel.getCreatorBox(), true);
		changeMultipleFieldBackground(creatorPanel.getSubjectBox(), true);
		changeSingleFieldBackground(descrPanel.getProblemBox(), true);
		changeMultipleFieldBackground(descrPanel.getMotivationBox(), true);
		changeMultipleFieldBackground(descrPanel.getExampleBox(), true);
		changeSingleFieldBackground(descrPanel.getContextBox(), true);
		changeSingleFieldBackground(datePanel.getCreatedBox().getTextBox(), true);
		changeSingleFieldBackground(datePanel.getTypeBox(), true);
		changeSingleFieldBackground(datePanel.getNotationBox(), true);
		changeSingleFieldBackground(formatPanel.getSourceBox(), true);
		changeSingleFieldBackground(formatPanel.getLanguageValueBox().getTextBox(), true);
		changeSingleFieldBackground(formatPanel.getLanguageLabelBox().getTextBox(), true);
		changeSingleFieldBackground(historyPanel.getEventDateBox().getTextBox(), true);
		changeMultipleFieldBackground(historyPanel.getAuthorBox(), true);
		changeSingleFieldBackground(historyPanel.getReasonBox(), true);
		changeMultipleFieldBackground(reqPanel.getFunctionalRequirementsBox(), true);
		changeSingleFieldBackground(reqPanel.getDependenciesAndContributionsBox(), true);
		changeSingleFieldBackground(reqPanel.getDependencyGraphBox(), true);
		changeSingleFieldBackground(reqPanel.getPrioritiesDiagramBox(), true);
		changeMultipleFieldBackground(reqPanel.getParticipantsBox(), true);
		changeSingleFieldBackground(modelPanel.getUseCaseDiagramBox(), true);
		changeMultipleFieldBackground(modelPanel.getColaborationSequenceDiagramBox(), true);
		changeSingleFieldBackground(modelPanel.getClassDiagramBox(), true);
		changeMultipleFieldBackground(modelPanel.getClassDescriptionBox(), true);
		changeMultipleFieldBackground(modelPanel.getRelationshipDescriptionBox(), true);
		changeMultipleFieldBackground(conseqPanel.getPositiveBox(), true);
		changeMultipleFieldBackground(conseqPanel.getNegativeBox(), true);
		
		// Conditional fields
		changeMultipleFieldBackground(publisherPanel.getContributorBox(), false);
		changeSingleFieldBackground(datePanel.getModifiedBox().getTextBox(), false);
		changeSingleFieldBackground(relationPanel.getIsVersionOfBox(), false);
		changeMultipleFieldBackground(relationPanel.getIsReplacedByBox(), false);
		changeMultipleFieldBackground(relationPanel.getReplacesBox(), false);
  		changeSingleFieldBackground(relationPanel.getAboutBox(), false);
  		changeMultipleFieldBackground(coveragePanel.getRightsBox(), false);
  		changeSingleFieldBackground(historyPanel.getChangesBox(), false);
  		changeSingleFieldBackground(reqPanel.getContributionGraphBox(), false);
  		changeMultipleFieldBackground(reqPanel.getConflictAndResolutionBox(), false);
	}
		
	/**
	 * Verify a single field of the resource, change the 
	 * Box background if this box is empty.
	 * 
	 * @param box The textBox of the element
	 * @param mandatory True if the field is mandatory, false if it is conditional
	 */
	private static void changeSingleFieldBackground(TextBoxBase box, boolean mandatory){
		String text = box.getText();
		
		if(mandatory){
			if("".equals(text) || text == null){
				box.addStyleName("errorTextBox");
			} else{
				box.removeStyleName("errorTextBox");
			}
		} else{
			if("".equals(text) || text == null){
				box.addStyleName("warningTextBox");
			} else{
				box.removeStyleName("warningTextBox");
			}
		}
	}
	
	/**
	 * Verify a multiple field of the resource, change the 
	 * Box background if this box is empty.
	 * 
	 * @param multipleField The MultipleTextFild of the element
	 * @param mandatory True if the field is mandatory, false if it is conditional
	 */
	private static void changeMultipleFieldBackground(MultipleTextField multipleField, boolean mandatory){
		TextBox box = multipleField.getTextBoxByIndex(0);
		List<String> list = multipleField.getBoxStringList();
		
		if(mandatory){
			if(list.size() == 0 || list == null){
				box.addStyleName("errorTextBox");
			} else{
				box.removeStyleName("errorTextBox");
			}	
		} else {
			if(list.size() == 0 || list == null){
				box.addStyleName("warningTextBox");
			} else{
				box.removeStyleName("warningTextBox");
			}
		}
	}

}
