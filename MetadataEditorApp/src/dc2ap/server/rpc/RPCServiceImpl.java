package dc2ap.server.rpc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import dc2ap.client.entities.DC2AP;
import dc2ap.client.rpc.RPCService;
import dc2ap.server.services.ValidateService;
import dc2ap.server.services.XmlService;

/**
 * The server-side implementation of the RPC service (Servlet).
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
@SuppressWarnings("serial")
public class RPCServiceImpl extends RemoteServiceServlet implements RPCService{
	private ValidateService validateService = new ValidateService();
	private XmlService xmlService = new XmlService();
		
	@Override
	public List<String> validateMetadata(final DC2AP dc2apObject) {
		return validateService.validateMetadata(dc2apObject.getIdentifier(), dc2apObject.getTitle(), 
				dc2apObject.getCreator(), dc2apObject.getSubject(), dc2apObject.getProblem(), 
				dc2apObject.getMotivation(), dc2apObject.getExample(), dc2apObject.getContext(), 
				dc2apObject.getCreated(), dc2apObject.getModified(), dc2apObject.getType(), 
				dc2apObject.getNotation(), dc2apObject.getFormat(), dc2apObject.getSource(), 
				dc2apObject.getLanguageValue(), dc2apObject.getLanguageLabel(),
				dc2apObject.getEventDate(), dc2apObject.getAuthor(), dc2apObject.getReason(), 
				dc2apObject.getFunctionalRequirements(), dc2apObject.getDependencesAndContributions(), 
				dc2apObject.getDependencyGraph(), dc2apObject.getPrioritiesDiagram(), 
				dc2apObject.getParticipants(), dc2apObject.getUseCaseDiagram(), 
				dc2apObject.getCollaborationSequenceDiagrams(), dc2apObject.getClassDiagram(), 
				dc2apObject.getClassDescriptions(), dc2apObject.getRelationshipDescriptions(), 
				dc2apObject.getPositive(), dc2apObject.getNegative());
	}
	
	@Override
	public String generateScriptRDF(final DC2AP dc2apObject) {
		// validate the resource before create file
		if(!validateMetadata(dc2apObject).isEmpty()){
			return "ValidationException";
		}

		try {
			return xmlService.generateRdfScript(dc2apObject.getIdentifier(), dc2apObject.getTitle(), 
					dc2apObject.getAlternativeTitle(), dc2apObject.getCreator(), 
					dc2apObject.getSubject(), dc2apObject.getProblem(), dc2apObject.getMotivation(), 
					dc2apObject.getExample(), dc2apObject.getKnownUses(),dc2apObject.getContext(), 
					dc2apObject.getPublisher(), dc2apObject.getContributor(), dc2apObject.getCreated(), 
					dc2apObject.getModified(), dc2apObject.getType(), dc2apObject.getNotation(), 
					dc2apObject.getFormat(), dc2apObject.getSource(), dc2apObject.getLanguageValue(), 
					dc2apObject.getLanguageLabel(),dc2apObject.getIsVersionOf(), 
					dc2apObject.getIsReplacedBy(), dc2apObject.getReplaces(), dc2apObject.getIsPartOf(), 
					dc2apObject.getHasPart(), dc2apObject.getIsDesignedWith(), dc2apObject.getShouldAvoid(), 
					dc2apObject.getComplementedBy(), dc2apObject.getAbout(), dc2apObject.getCoverage(), 
					dc2apObject.getRights(), dc2apObject.getEventDate(), dc2apObject.getAuthor(), 
					dc2apObject.getReason(), dc2apObject.getChanges(), dc2apObject.getFunctionalRequirements(), 
					dc2apObject.getNonFunctionalRequirements(), dc2apObject.getDependencesAndContributions(), 
					dc2apObject.getDependencyGraph(), dc2apObject.getContributionGraph(), 
					dc2apObject.getConflictAndResolution(), dc2apObject.getPrioritiesDiagram(), 
					dc2apObject.getParticipants(), dc2apObject.getUseCaseDiagram(), 
					dc2apObject.getCollaborationSequenceDiagrams(), dc2apObject.getActivityStateDiagrams(), 
					dc2apObject.getClassDiagram(), dc2apObject.getClassDescriptions(), 
					dc2apObject.getRelationshipDescriptions(), dc2apObject.getSolutionVariants(), 
					dc2apObject.getResultingContext(), dc2apObject.getDesignGuidelines(), 
					dc2apObject.getPositive(), dc2apObject.getNegative());
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			return "ParserConfigurationException";
		} catch (TransformerException e) {
			e.printStackTrace();
			return "TransformerException";
		}
	}

	@Override
	public String saveFileRDF(final String rdfScript, final String fileName) {
		String fullFilename = fileName + ".rdf";
		try {
			saveTemporaryFile(rdfScript, fullFilename);
		} catch (IOException e) {
			System.out.println("IOException! in the method " +
					"'saveFileRDF' in the server-side.\n\n");
			e.printStackTrace();
			return "IOException";
		}
		return "";
	}
	
	@Override
	public DC2AP readFileRDF(String fileName){
		// get the path of the repository files in the server
		ServletContext context = getServletConfig().getServletContext();
		final String temporaryFilesPath = context.getRealPath("temporaryFiles");
	
		// the full file path name on the server
		String fullFilePath = temporaryFilesPath + "\\" + fileName;

		try {
    		// Open RDF file into a DC2AP object
    		DC2AP object = xmlService.readRdfFile(fullFilePath);
    		return object;
		} catch (Exception e) {
			e.printStackTrace();
			return null;			
		} finally {
			// delete the temporary file from the server after reading
			deleteTemporaryFile(fileName);
		}
	}
	
	@Override
	public String saveProject(DC2AP dc2apObject, String fileName) {
		// save as .mde (metadata editor)
		String fullFileName = fileName + ".mde";

		String projectXmlScript = "";
		try {
			projectXmlScript = xmlService.generateProjectXmlScript(dc2apObject.getIdentifier(), dc2apObject.getTitle(), 
					dc2apObject.getAlternativeTitle(), dc2apObject.getCreator(), 
					dc2apObject.getSubject(), dc2apObject.getProblem(), dc2apObject.getMotivation(), 
					dc2apObject.getExample(), dc2apObject.getKnownUses(),dc2apObject.getContext(), 
					dc2apObject.getPublisher(), dc2apObject.getContributor(), dc2apObject.getCreated(), 
					dc2apObject.getModified(), dc2apObject.getType(), dc2apObject.getNotation(), 
					dc2apObject.getFormat(), dc2apObject.getSource(), dc2apObject.getLanguageValue(), 
					dc2apObject.getLanguageLabel(),dc2apObject.getIsVersionOf(), 
					dc2apObject.getIsReplacedBy(), dc2apObject.getReplaces(), dc2apObject.getIsPartOf(), 
					dc2apObject.getHasPart(), dc2apObject.getIsDesignedWith(), dc2apObject.getShouldAvoid(), 
					dc2apObject.getComplementedBy(), dc2apObject.getAbout(), dc2apObject.getCoverage(), 
					dc2apObject.getRights(), dc2apObject.getEventDate(), dc2apObject.getAuthor(), 
					dc2apObject.getReason(), dc2apObject.getChanges(), dc2apObject.getFunctionalRequirements(), 
					dc2apObject.getNonFunctionalRequirements(), dc2apObject.getDependencesAndContributions(), 
					dc2apObject.getDependencyGraph(), dc2apObject.getContributionGraph(), 
					dc2apObject.getConflictAndResolution(), dc2apObject.getPrioritiesDiagram(), 
					dc2apObject.getParticipants(), dc2apObject.getUseCaseDiagram(), 
					dc2apObject.getCollaborationSequenceDiagrams(), dc2apObject.getActivityStateDiagrams(), 
					dc2apObject.getClassDiagram(), dc2apObject.getClassDescriptions(), 
					dc2apObject.getRelationshipDescriptions(), dc2apObject.getSolutionVariants(), 
					dc2apObject.getResultingContext(), dc2apObject.getDesignGuidelines(), 
					dc2apObject.getPositive(), dc2apObject.getNegative());
		} catch (ParserConfigurationException e) {
			System.out.println("ParserConfigurationException! in the method " +
					"'saveProject' in the server-side.\n\n");
			e.printStackTrace();
			return "ParserConfigurationException";
		} catch (TransformerException e) {
			System.out.println("TransformerException! in the method " +
					"'saveProject' in the server-side.\n\n");
			e.printStackTrace();
			return "TransformerException";
		}
		
		try {
			saveTemporaryFile(projectXmlScript, fullFileName);
		} catch (IOException e) {
			System.out.println("IOException! in the method " +
					"'saveProject' in the server-side.\n\n");
			e.printStackTrace();
			return "IOException";
		}
		return "";
	}

	@Override
	public Boolean deleteTemporaryFile(String fileName) {
		// get the path of the repository files in the server
		ServletContext context = getServletConfig().getServletContext();
		final String temporaryFilesPath = context.getRealPath("temporaryFiles");

		String fullFilePath = temporaryFilesPath +  "\\" + fileName;
		
		File file = new File(fullFilePath);
		
        return file.delete();
	}
	
	/**
	 * Save file in the temporary folder in the server.
	 * 
	 * @param fileScript script of the file to be saved.
	 * @param fullFileName the full name of the file, with the file extension.
	 * @throws IOException
	 */
	private void saveTemporaryFile(String fileScript, String fullFileName) 
			throws IOException{
		
		// get the path of the repository files in the server
		ServletContext context = getServletConfig().getServletContext();
		final String temporaryFilesPath = context.getRealPath("temporaryFiles");

		BufferedWriter buffer = 
			new BufferedWriter(new PrintWriter(temporaryFilesPath + "\\" + fullFileName));
		buffer.write(fileScript); 
		buffer.flush();  
	    buffer.close(); 
	}
}
