package dc2ap.server.rpc;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.FileCleaningTracker;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

/**
 * Servlet to process .rdf files opened by the user.
 * This Servlet receive a Form with a File from the client and save  
 * the file in the temporary files server folder.
 * 
 * TUTORIALS:
 * http://commons.apache.org/fileupload/using.html (main) 
 * http://www.jroller.com/hasant/entry/fileupload_with_gwt
 * http://stackoverflow.com/questions/9160535/is-it-possible-to-upload-a-file-via-gwt-rpc-servlets
 * http://www.tutorialspoint.com/gwt/gwt_fileupload_widget.htm
 * http://blog.ropardo.ro/2011/04/19/gwt-2-1-uploading-a-file-using-the-rpc-mechanism/
 */
@SuppressWarnings("serial")
public class FileUploadServlet extends HttpServlet {
	private FileItem uploadedFileItem;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		super.doGet(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		// get the path of the repository files in the server
		ServletContext context = getServletConfig().getServletContext();
		final String TEMPORARY_FILES_PATH = context.getRealPath("temporaryFiles");
		
		boolean isMultiPart = ServletFileUpload
			.isMultipartContent(new ServletRequestContext(request));

		// Check that we have a file upload request
		// process only multipart requests
		if(isMultiPart) {
			// Create a factory for disk-based file items
			//DiskFileItemFactory factory = new DiskFileItemFactory();
			//factory.setRepository(new File(TEMPORARY_FILES_PATH));
			DiskFileItemFactory factory = newDiskFileItemFactory(TEMPORARY_FILES_PATH);
			
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);
			
			// Parse the request
			try {
				List<FileItem> items = upload.parseRequest(request);
				uploadedFileItem = items.get(0); // we only upload one file

				if(uploadedFileItem == null) {
					super.service(request, response); 
					return;
				} else if(uploadedFileItem.getFieldName()
						.equalsIgnoreCase("uploadFormElement")) {				
					// upload the file to the server and add the file name 
        			// to the server response
			        processUploadedFile(response, TEMPORARY_FILES_PATH);
				}

			} catch(Exception e) {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                        "An error occurred while creating the file: " + e.getMessage());
			}
		} else {
			super.service(request, response);
			return;
		}
	}
	
	/**
	 * Process the file from the client, save it in the 
	 * temporaryFiles folder. Add to the server response 
	 * the name of the file created.
	 * 
	 * @param fileName the file name to be saved in the server
	 * @param response The HttpServletResponse to the client
	 *
	 * @throws Exception if it was not possible to create the 
	 * file in the server 
	 */
	private void processUploadedFile(HttpServletResponse response,
			final String TEMPORARY_FILES_PATH) 
			throws Exception {

		// file name in the server
		Date now = new Date();
        String fileName = "uploadedFile_" + now.getTime() + ".rdf";
        
		// create a file in the server
        File uploadedFile = new File(TEMPORARY_FILES_PATH, fileName);

        if (uploadedFile.createNewFile()) {
        	// write the file to the server folder
        	uploadedFileItem.write(uploadedFile);
      	
        	// response to the client, return the file name
        	response.setStatus(HttpServletResponse.SC_CREATED);
        	response.getWriter().print(fileName);
        	response.flushBuffer();
        }
	}
	
	/**
	 * Create a factory for disk-based file items.
	 * Set a FileCleaningTracker to the factor in order
	 * to delete the temporary files from the temporary folder
	 * if they are no longer used.
	 * 
	 * @param TEMPORARY_FILES_PATH Path of the temporary folder
	 * @return a DiskFileItemFactory
	 */
	private DiskFileItemFactory newDiskFileItemFactory(final String TEMPORARY_FILES_PATH) {
		FileCleaningTracker fileCleaningTracker = new FileCleaningTracker();
		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setRepository(new File(TEMPORARY_FILES_PATH));
		factory.setFileCleaningTracker(fileCleaningTracker);
		
		return factory;
	}
}
