package dc2ap.server.entities;

import java.io.Serializable;

/**
 * Entity the contains the languages definitions.
 * See more in: http://www.sil.org/iso639-3/codes.asp
 * 
 * @author Douglas A. Peixoto - 08/2012
 *
 */
@SuppressWarnings("serial")
public class Languages implements Serializable {		
	private String value;
	private String label;
	
	/**
	 * Get the language value.
	 * 
	 * @return String with the languages value.
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the language value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * Get the language label.
	 * 
	 * @return String with the languages label
	 */
	public String getLabel() {
		return label;
	}
	/**
	 * @param label the language label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
}
