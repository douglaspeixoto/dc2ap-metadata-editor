package dc2ap.server.services;

import java.io.StringWriter;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Service to manipulate XML files.
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
public class XMLService {
	private Document doc;
	
	// constants
	private static final String bag = "rdf:Bag";
	private static final String seq = "rdf:Seq";
	private static final String alt = "rdf:Alt";
	private static final String rsc = "rdf:resource";
	private static final String lang = "xml:lang";

	/**
	 * Create the string of the DC2AP RDF document.
	 * 
	 * @throws ParserConfigurationException 
	 * @throws TransformerException 
	 */
	public String generateRdfScript(
			String identifier, String title, List<String> alternativeTitle,
			List<String> creator, List<String> subject, String problem,
			List<String> motivation, List<String> examples, List<String> knownUses,
			String context, List<String> publisher, List<String> contributor,
			String created, String modified, String type, String notation, 
			List<String> format, String source, String languageValue, 
			String languageLabel, String isVersionOf, List<String> isReplacedBy,
			List<String> replaces, List<String> isPartOf, List<String> hasPart, 
			List<String> isDesignedWith, List<String> shouldAvoid,
			List<String> complementedBy, String about, List<String> coverage, 
			List<String> rights, String eventDate, List<String> author,
			String reason, String changes, List<String> functionalRequirements,
			List<String> nonFunctionalRequirements, String dependencesAndContributions,
			String dependencyGraph, String contributionGraph, List<String> conflictAndResolution,
			String prioritiesDiagram, List<String> participants, String useCaseDiagram,
			List<String> collaborationSequenceDiagrams, List<String> activityStateDiagrams,
			String classDiagram, List<String> classDescriptions, List<String> relationshipDescriptions,
			List<String> solutionVariants, List<String> resultingContext, List<String> designGuidelines,
			List<String> positive, List<String> negative	
		) throws ParserConfigurationException, TransformerException  {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();
		
		/* doc = docBuilder.parse("src/files/rdfTemplate.xml");  
		Element root = doc.getDocumentElement(); */
		
		// get the language used in this document
		String langAttr = languageValue;
		
		// creates a new document
		doc = docBuilder.newDocument();
		
		// create root element <rdf:RDF>
		Element rootNode = doc.createElement("rdf:RDF"); 
		setRootAttributes(rootNode);
		doc.appendChild(rootNode);

		// create the Description node, which will append the DC2AP element nodes
		Element descriptionNode = createSingleElement("rdf:Description", null);
				descriptionNode.setAttribute("rdf:about", "");
		
		//*** create DC2AP XML node elements ***//
		Element indetifierNode = createSingleElement("dc:identifier", identifier);
		Element titleNode = createSingleElement("dc:title", title);
		Element altTitleNode = createCompositeElement("dcterms:alternative", bag, alternativeTitle);
		Element creatorNode = createCompositeElement("dc:creator", seq, creator);
		Element subjectNode = createCompositeElement("dc:subject", bag, subject);
				subjectNode.setAttribute(lang, langAttr);
		Element descrNode = createSingleElement("dc:description", null);
				descrNode.setAttribute(lang, langAttr);
			Element problemNode = createSingleElement("dc2ap:problem", problem);
					problemNode.setAttribute(lang, langAttr);
			Element motivationNode = createCompositeElement("dc2ap:motivation", bag, motivation);
					motivationNode.setAttribute(lang, langAttr);
			Element exampleNode = createCompositeElement("dc2ap:example", bag, examples);
					exampleNode.setAttribute(lang, langAttr);
			Element knownUsesNode = createCompositeElement("dc2ap:knownUses", bag, knownUses);
					knownUsesNode.setAttribute(lang, langAttr);
			Element contextNode = createSingleElement("dc2ap:context", context);
					contextNode.setAttribute(lang, langAttr);
			descrNode.appendChild(problemNode);	
			descrNode.appendChild(motivationNode);
			descrNode.appendChild(exampleNode);
			descrNode.appendChild(knownUsesNode);
			descrNode.appendChild(contextNode);
		Element publisherNode = createCompositeElement("dc:publisher", seq, publisher);
		Element contributorNode = createCompositeElement("dc:contributor", seq, contributor);
		Element dateNode = createSingleElement("dc:date", null);
			Element createdNode = createDateElement("dcterms:created", created);
			Element modifiedNode = createDateElement("dcterms:modified", modified);
			dateNode.appendChild(createdNode);
			dateNode.appendChild(modifiedNode);
		Element analysisPatternNode = createSingleElement("dc2aptp:analysisPattern", null);
			Element typeNode = createSingleElement("rdfs:label", type);
					typeNode.setAttribute(lang, langAttr);
			Element notationNode = createSingleElement("rdaelem:formOfNotation", notation);
					notationNode.setAttribute(lang, langAttr);
			analysisPatternNode.appendChild(typeNode);
			analysisPatternNode.appendChild(notationNode);
		Element formatNode = createFormatElement("dc:format", format);
		Element sourceNode = createSingleElement("dc:source", source);
		Element languageNode = createSingleElement("dc:language", null);
			Element langTermNode = createSingleElement("dcterms:ISO639-3", null);
				Element langValueNode = createSingleElement("rdf:value", languageValue);
				Element langLabelNode = createSingleElement("rdfs:label", languageLabel);
				langTermNode.appendChild(langValueNode);
				langTermNode.appendChild(langLabelNode);
			languageNode.appendChild(langTermNode);
		Element relationNode = createSingleElement("dc:relation", null);
			Element isVersionOfNode = createSingleElement("dcterms:isVersionOf", isVersionOf);
			Element isReplacedByNode = createCompositeElement("dcterms:isReplacedBy", alt, isReplacedBy);
			Element replacesNode = createCompositeElement("dcterms:replaces", alt, replaces);
			Element isPartOfNode = createCompositeElement("dcterms:isPartOf", bag, isPartOf);
			Element isDesignedWithNode = createCompositeElement("dc2ap:isDesignedWith", bag, isDesignedWith);
			Element shouldAvoidNode = createCompositeElement("dc2ap:shouldAvoid", bag, shouldAvoid);
			Element complementedByNode = createCompositeElement("dc2ap:complementedBy", bag, complementedBy);
			Element aboutNode = createSingleElement("rdaelem:note", about);
					aboutNode.setAttribute(lang, langAttr);
			relationNode.appendChild(isVersionOfNode);
			relationNode.appendChild(isReplacedByNode);
			relationNode.appendChild(replacesNode);
			relationNode.appendChild(isPartOfNode);
			relationNode.appendChild(isDesignedWithNode);
			relationNode.appendChild(shouldAvoidNode);
			relationNode.appendChild(complementedByNode);
			relationNode.appendChild(aboutNode);
		Element coverageNode = createCompositeElement("dc:coverage", bag, coverage);
				coverageNode.setAttribute(lang, langAttr);
		Element rightsNode = createCompositeElement("dc:rights", bag, rights);
				rightsNode.setAttribute(lang, langAttr);
		Element historyNode = createSingleElement("dc2ap:history", null);
			Element historySeqNode = doc.createElement(seq);
				Element historyLiNode = doc.createElement("rdf:li");
					Element eventDateNode = createDateElement("dc:date", eventDate);
					Element authorNode = createCompositeElement("rdaroles:author", seq, author); 
					Element reasonNode = createSingleElement("dc2ap:reason", reason);
							reasonNode.setAttribute(lang, langAttr);
					Element changesNode = createSingleElement("dc2ap:changes", changes);
							changesNode.setAttribute(lang, langAttr);
					historyLiNode.appendChild(eventDateNode);
					historyLiNode.appendChild(authorNode);
					historyLiNode.appendChild(reasonNode);
					historyLiNode.appendChild(changesNode);
			historySeqNode.appendChild(historyLiNode);
		historyNode.appendChild(historySeqNode);
		Element requirementsNode = createSingleElement("dc2ap:requirements", null);
			Element funcReqNode = createCompositeElement("dc2ap:functionalRequirements", seq, functionalRequirements);
					funcReqNode.setAttribute(lang, langAttr);
			Element nonFuncreqNode = createCompositeElement("dc2ap:nonFunctionalRequirements", seq, nonFunctionalRequirements);
					nonFuncreqNode.setAttribute(lang, langAttr);
			Element depAndContribNode = createSingleElement("dc2ap:dependenciesAndContributions", dependencesAndContributions);
					depAndContribNode.setAttribute(lang, langAttr);
			Element dependencyGraphNode = createSingleElementAttr("dc2ap:dependencyGraph", rsc, dependencyGraph);
			Element contributionGraphNode = createSingleElementAttr("dc2ap:contributionGraph", rsc, contributionGraph);
			Element conflictAndResolNode = createCompositeElement("dc2ap:conflictAndResolution", bag, conflictAndResolution);
					conflictAndResolNode.setAttribute(lang, langAttr);
			Element prioritiesNode = createSingleElementAttr("dc2ap:priorities", rsc, prioritiesDiagram);
			Element participantsNode = createCompositeElement("dc2ap:participants", bag, participants);
					participantsNode.setAttribute(lang, langAttr);
			requirementsNode.appendChild(funcReqNode);
			requirementsNode.appendChild(nonFuncreqNode);
			requirementsNode.appendChild(depAndContribNode);
			requirementsNode.appendChild(dependencyGraphNode);
			requirementsNode.appendChild(contributionGraphNode);
			requirementsNode.appendChild(conflictAndResolNode);
			requirementsNode.appendChild(prioritiesNode);
			requirementsNode.appendChild(participantsNode);
		Element modelingNode = createSingleElement("dc2ap:modelling", null);
			Element behaviourNode = createSingleElement("dc2ap:behaviour", null);
				Element useCaseDiagNode = createSingleElementAttr("dc2ap:useCaseDiagram", rsc, useCaseDiagram);
				Element collabSeqDiagNode = createCompositeElementAttr("dc2ap:collaborationSequenceDiagrams", bag, rsc, collaborationSequenceDiagrams);
				Element actStateDiagNode = createCompositeElementAttr("dc2ap:activityStateDiagrams", bag, rsc, activityStateDiagrams);
				behaviourNode.appendChild(useCaseDiagNode);
				behaviourNode.appendChild(collabSeqDiagNode);
				behaviourNode.appendChild(actStateDiagNode);
			Element structureNode = createSingleElement("dc2ap:structure", null);
				Element classDiagNode = createSingleElementAttr("dc2ap:classDiagram", rsc, null);
				Element classDescrNode = createCompositeElement("dc2ap:classDescriptions", bag, classDescriptions);
						classDescrNode.setAttribute(lang, langAttr);
				Element relatDescrNode = createCompositeElement("dc2ap:relationshipDescriptions", bag, relationshipDescriptions);
						relatDescrNode.setAttribute(lang, langAttr);
				structureNode.appendChild(classDiagNode);
				structureNode.appendChild(classDescrNode);
				structureNode.appendChild(relatDescrNode);
			Element solVariantsNode = createCompositeElementAttr("dc2ap:solutionVariants", bag, rsc, solutionVariants);
			modelingNode.appendChild(behaviourNode);
			modelingNode.appendChild(structureNode);
			modelingNode.appendChild(solVariantsNode);
		Element resultContextNode = createCompositeElement("dc2ap:resultingContext", bag, resultingContext);
				resultContextNode.setAttribute(lang, langAttr);
		Element desngGuidelinesNode = createCompositeElement("dc2ap:designGuidelines", bag, designGuidelines);
				desngGuidelinesNode.setAttribute(lang, langAttr);
		Element consequencesNode = createSingleElement("dc2ap:consequences", null);
				consequencesNode.setAttribute(lang, langAttr);
			Element positiveNode = createCompositeElement("dc2ap:positive", bag, positive);
					positiveNode.setAttribute(lang, langAttr);
			Element negativeNode = createCompositeElement("dc2ap:negative", bag, negative);
					negativeNode.setAttribute(lang, langAttr);
			consequencesNode.appendChild(positiveNode);
			consequencesNode.appendChild(negativeNode);
			
		//*** add elements to the description element ***//
		descriptionNode.appendChild(indetifierNode);
		descriptionNode.appendChild(titleNode);
		descriptionNode.appendChild(altTitleNode);
		descriptionNode.appendChild(creatorNode);
		descriptionNode.appendChild(subjectNode);
		descriptionNode.appendChild(descrNode);
		descriptionNode.appendChild(publisherNode);
		descriptionNode.appendChild(contributorNode);
		descriptionNode.appendChild(dateNode);
		descriptionNode.appendChild(analysisPatternNode);
		descriptionNode.appendChild(formatNode);
		descriptionNode.appendChild(sourceNode);
		descriptionNode.appendChild(languageNode);
		descriptionNode.appendChild(relationNode);
		descriptionNode.appendChild(coverageNode);
		descriptionNode.appendChild(rightsNode);
		descriptionNode.appendChild(historyNode);
		descriptionNode.appendChild(requirementsNode);
		descriptionNode.appendChild(modelingNode);
		descriptionNode.appendChild(resultContextNode);
		descriptionNode.appendChild(desngGuidelinesNode);
		descriptionNode.appendChild(consequencesNode);
		
		//*** add description to the root element ***//
		rootNode.appendChild(descriptionNode);
		
		// generate the XML document			
		DOMSource DOMsrc = new DOMSource(doc);
		StringWriter stringWriter = new StringWriter(); 			
		StreamResult result = new StreamResult(stringWriter);
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");             
        transformer.transform(DOMsrc, result);  
		
        /*
		DOMSource DOMsrc = new DOMSource(doc);  
        StreamResult result = new StreamResult(new FileOutputStream("C:/saida/" + "saida.rdf"));  
        
        TransformerFactory transFactory = TransformerFactory.newInstance();
        Transformer transformer = transFactory.newTransformer();  
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
        transformer.transform(DOMsrc, result);  */  
        
		// String of the result
		String xmlString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\">\n";
		xmlString += stringWriter.toString();
        
        System.out.println(xmlString); 
        
        return xmlString;
	}
	
	/**
	 * Create a single XML node with a text content (value).
	 * 
	 * @param tagName the name of the node
	 * @param value the text content of the node (value)
	 * @return Node Element
	 */
	private Element createSingleElement(String tagName, String value){
		Element element = doc.createElement(tagName);
		
		if(value != null){
			element.setTextContent(value);
		}
		
		return element;
	}
	
	/**
	 * Create a single XML node with a text content (value). Append the value
	 * as an attribute of the element. 
	 * 
	 * @param tagName the name of the node
	 * @param attrName the name of the attribute
	 * @param value the text content of the node (value)
	 * @return
	 */
	private Element createSingleElementAttr(String tagName, String attrName, String value){
		Element element = doc.createElement(tagName);		
		
		value = value != null ? value : "";
		
		element.setAttribute(attrName, value);
		
		return element;
	}
	
	/**
	 * Create a XML node which contains a group of values inside it.
	 * 
	 * @param tagName the name of the node
	 * @param compositeType the type of composite, Bag, Seq, Alt...
	 * @param listValue the list of text contents. Values of each line of the element
	 * @return Node Element
	 */
	private Element createCompositeElement(String tagName, String compositeType, List<String> listValue){
		Element element = doc.createElement(tagName);
		Element group = doc.createElement(compositeType);
		
		for(String value : listValue){
			Element li = doc.createElement("rdf:li");
			li.setTextContent(value);
			group.appendChild(li);
		}

		element.appendChild(group);
		
		return element;
	}
	
	/**
	 * Create a XML node which contains a group of values inside it. Append the value
	 * as an attribute of the element.
	 * 
	 * @param tagName the name of the node
	 * @param compositeType the type of composite, Bag, Seq, Alt...
	 * @param attrName the name of the attribute
	 * @param listValue the list of text contents. Values of each line of the element
	 * @return Node Element
	 */
	private Element createCompositeElementAttr(String tagName, String compositeType, 
			String attrName, List<String> listValue){
		Element element = doc.createElement(tagName);
		Element group = doc.createElement(compositeType);
		
		for(String value : listValue){
			Element li = doc.createElement("rdf:li");
			li.setAttribute(attrName, value);
			group.appendChild(li);
		}

		element.appendChild(group);
		
		return element;
	}
	
	/**
	 * Create a single XML node with a date content (value).
	 * 
	 * @param tagName the name of the node
	 * @param value the text content of the node (date value)
	 * @return Node Element
	 */
	private Element createDateElement(String tagName, String value){
		Element element = doc.createElement(tagName);
		Element w3cdtf = doc.createElement("dcterms:W3CDTF");
		Element elemValue = createSingleElement("rdf:value", value);
		w3cdtf.appendChild(elemValue);
		element.appendChild(w3cdtf);
		
		return element;
	}
	
	/**
	 * Create the Format node of the XML document, with its children nodes 
	 * and content (values).
	 * 
	 * @param tagName the name of the node
	 * @param value the text content of the node (date value)
	 * @return Node Element
	 */
	private Element createFormatElement(String tagName, List<String> listValue){
		Element format = doc.createElement(tagName);
			Element formatBag = createSingleElement(bag, null);
	
				if(!listValue.isEmpty()){
					Element jpegLi = createSingleElement("rdf:li", null);
					if(listValue.contains("JPEG")){
						Element jpeg = createSingleElement("dcterms:IMT", null);
							Element jpegValue = createSingleElement("rdf:value", "image/jpeg");
							Element jpegLabel = createSingleElement("rdfs:label", "JPEG");
							jpeg.appendChild(jpegValue);
							jpeg.appendChild(jpegLabel);
						jpegLi.appendChild(jpeg);
					}
					Element pngLi = createSingleElement("rdf:li", null);
					if(listValue.contains("PNG")){
						Element png = createSingleElement("dcterms:IMT", null);
							Element pngValue = createSingleElement("rdf:value", "image/png");
							Element pngLabel = createSingleElement("rdfs:label", "PNG");
							png.appendChild(pngValue);
							png.appendChild(pngLabel);
						pngLi.appendChild(png);
					}
					Element xmiLi = createSingleElement("rdf:li", null);
					if(listValue.contains("XMI")){
						Element xmi = createSingleElement("dcterms:IMT", null);
							Element xmiValue = createSingleElement("dc2apft:xmi", null);
							Element xmiLabel = createSingleElement("rdfs:label", "XMI");
							xmi.appendChild(xmiValue);
							xmi.appendChild(xmiLabel);
						xmiLi.appendChild(xmi);
					}
					formatBag.appendChild(jpegLi);	
					formatBag.appendChild(pngLi);
					formatBag.appendChild(xmiLi);
				}
				
			format.appendChild(formatBag);
		
		return format;
	}

	/**
	 * Set the attributes of the root element
	 * @param root the root element
	 */
	private void setRootAttributes(Element root){
		root.setAttribute("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		root.setAttribute("xmlns:rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		root.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
		root.setAttribute("xmlns:dc2ap", "http://purl.org/dc2ap/elements/");
		root.setAttribute("xmlns:dc2aptp", "http://purl.org/dc2ap/type/");
		root.setAttribute("xmlns:dc2apft", "http://purl.org/dc2ap/format/");
		root.setAttribute("xmlns:dcterms", "http://purl.org/dc/terms/");
		root.setAttribute("xmlns:dcmitype", "http://purl.org/dc/dcmitype/");
		root.setAttribute("xmlns:rdaelem", "http://rdvocab.info/Elements/");
		root.setAttribute("xmlns:rdaroles", "http://rdvocab.info/roles/");
	}
}
