package dc2ap.server.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import dc2ap.server.entities.Languages;

/**
 * Service to deal with files. Open and read.
 *
 * @author Douglas A. Peixoto - 08/2012
 */
public class LanguagesService {
	// unique instance of the languages, to avoid read the file more than once
	private static List<Languages> languages = null;

	/**
	 * Return a list of Languages with the languages Value and Label.
	 * See more in: http://www.sil.org/iso639-3/codes.asp
	 * 
	 * @return Languages list with languages Value and languages Label.
	 */
	public final List<Languages> getLanguages(){
		// to read only once
		if(languages == null || languages.isEmpty()){
			languages = new ArrayList<Languages>();
			languages = readLanguages();
		}
		return languages;
	}

	/**
	 * Read from the file and return a list of Languages
	 * with the languages value and languages label.
	 * 
	 * @return list of Languages
	 */
	private List<Languages> readLanguages() {
        try {
        	BufferedReader buffer = new BufferedReader(new FileReader("/languages.txt"));

        	String line;
			while (buffer. ready()) {
				line = buffer.readLine();
				String[] tokens = line.split("\t");
			 
		    	Languages language = new Languages();
		    	language.setValue(tokens[0]);
		    	language.setLabel(tokens[1]);
  	
		    	languages.add(language);
			}

			buffer.close();
		} catch (IOException e) {
			System.out.println("Error opening languages file.");
			e.printStackTrace();
		}

			 

		return languages;
	}
}
