package dc2ap.server.services;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import dc2ap.client.entities.DC2AP;

/**
 * Service to manipulate XML (.RDF) files.
 * 
 * http://www.profissionaisti.com.br/2012/03/manipulando-xml-com-java/
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
public class XmlService {	
	// the XML document
	private Document doc;
	
	// constants
	private static final String BAG = "rdf:Bag";
	private static final String SEQ = "rdf:Seq";
	private static final String ALT = "rdf:Alt";
	private static final String RSC = "rdf:resource";
	private static final String LANG = "xml:lang";
	private static final String LI = "rdf:li";

	/**
	 * Create the script of the DC2AP RDF document.
	 * 
	 * @throws ParserConfigurationException 
	 * @throws TransformerException 
	 */
	public String generateRdfScript(
			String identifier, String title, List<String> alternativeTitle,
			List<String> creator, List<String> subject, String problem,
			List<String> motivation, List<String> examples, List<String> knownUses,
			String context, List<String> publisher, List<String> contributor,
			String created, String modified, String type, String notation, 
			List<String> format, String source, String languageValue, 
			String languageLabel, String isVersionOf, List<String> isReplacedBy,
			List<String> replaces, List<String> isPartOf, List<String> hasPart, 
			List<String> isDesignedWith, List<String> shouldAvoid,
			List<String> complementedBy, String about, List<String> coverage, 
			List<String> rights, String eventDate, List<String> author,
			String reason, String changes, List<String> functionalRequirements,
			List<String> nonFunctionalRequirements, String dependencesAndContributions,
			String dependencyGraph, String contributionGraph, List<String> conflictAndResolution,
			String prioritiesDiagram, List<String> participants, String useCaseDiagram,
			List<String> collaborationSequenceDiagrams, List<String> activityStateDiagrams,
			String classDiagram, List<String> classDescriptions, List<String> relationshipDescriptions,
			List<String> solutionVariants, List<String> resultingContext, List<String> designGuidelines,
			List<String> positive, List<String> negative	
		) throws ParserConfigurationException, TransformerException  {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();
		
		/* doc = docBuilder.parse("src/files/rdfTemplate.xml");  
		Element root = doc.getDocumentElement(); */
		
		// get the language used in this document
		String langAttr = languageValue;
		
		// creates a new document
		doc = docBuilder.newDocument();
		
		// create root element <rdf:RDF>
		Element rootNode = doc.createElement("rdf:RDF"); 
		setRootAttributes(rootNode);
		doc.appendChild(rootNode);

		// create the Description node, which will append the DC2AP element nodes
		Element descriptionNode = createSingleElement("rdf:Description", null);
				descriptionNode.setAttribute("rdf:about", "");
		
		//*** create DC2AP XML node elements ***//
		Element indetifierNode = createSingleElement("dc:identifier", identifier);
		Element titleNode = createSingleElement("dc:title", title);
		Element altTitleNode = createCompositeElement("dcterms:alternative", BAG, alternativeTitle);
		Element creatorNode = createCompositeElement("dc:creator", SEQ, creator);
		Element subjectNode = createCompositeElement("dc:subject", BAG, subject);
				subjectNode.setAttribute(LANG, langAttr);
		Element descrNode = createSingleElement("dc:description", null);
				descrNode.setAttribute(LANG, langAttr);
			Element problemNode = createSingleElement("dc2ap:problem", problem);
					problemNode.setAttribute(LANG, langAttr);
			Element motivationNode = createCompositeElement("dc2ap:motivation", BAG, motivation);
					motivationNode.setAttribute(LANG, langAttr);
			Element exampleNode = createCompositeElement("dc2ap:example", BAG, examples);
					exampleNode.setAttribute(LANG, langAttr);
			Element knownUsesNode = createCompositeElement("dc2ap:knownUses", BAG, knownUses);
					knownUsesNode.setAttribute(LANG, langAttr);
			Element contextNode = createSingleElement("dc2ap:context", context);
					contextNode.setAttribute(LANG, langAttr);
			descrNode.appendChild(problemNode);	
			descrNode.appendChild(motivationNode);
			descrNode.appendChild(exampleNode);
			descrNode.appendChild(knownUsesNode);
			descrNode.appendChild(contextNode);
		Element publisherNode = createCompositeElement("dc:publisher", SEQ, publisher);
		Element contributorNode = createCompositeElement("dc:contributor", SEQ, contributor);
		Element dateNode = createSingleElement("dc:date", null);
			Element createdNode = createDateElement("dcterms:created", created);
			Element modifiedNode = createDateElement("dcterms:modified", modified);
			dateNode.appendChild(createdNode);
			dateNode.appendChild(modifiedNode);
		Element analysisPatternNode = createSingleElement("dc2aptp:analysisPattern", null);
			Element typeNode = createSingleElement("rdfs:label", type);
					typeNode.setAttribute(LANG, langAttr);
			Element notationNode = createSingleElement("rdaelem:formOfNotation", notation);
					notationNode.setAttribute(LANG, langAttr);
			analysisPatternNode.appendChild(typeNode);
			analysisPatternNode.appendChild(notationNode);
		Element formatNode = createFormatElement("dc:format", format);
		Element sourceNode = createSingleElement("dc:source", source);
		Element languageNode = createSingleElement("dc:language", null);
			Element langTermNode = createSingleElement("dcterms:ISO639-3", null);
				Element langValueNode = createSingleElement("rdf:value", languageValue);
				Element langLabelNode = createSingleElement("rdfs:label", languageLabel);
				langTermNode.appendChild(langValueNode);
				langTermNode.appendChild(langLabelNode);
			languageNode.appendChild(langTermNode);
		Element relationNode = createSingleElement("dc:relation", null);
			Element isVersionOfNode = createSingleElement("dcterms:isVersionOf", isVersionOf);
			Element isReplacedByNode = createCompositeElement("dcterms:isReplacedBy", ALT, isReplacedBy);
			Element replacesNode = createCompositeElement("dcterms:replaces", ALT, replaces);
			Element isPartOfNode = createCompositeElement("dcterms:isPartOf", BAG, isPartOf);
			Element hasPartNode = createCompositeElement("dcterms:hasPart", BAG, hasPart);
			Element isDesignedWithNode = createCompositeElement("dc2ap:isDesignedWith", BAG, isDesignedWith);
			Element shouldAvoidNode = createCompositeElement("dc2ap:shouldAvoid", BAG, shouldAvoid);
			Element complementedByNode = createCompositeElement("dc2ap:complementedBy", BAG, complementedBy);
			Element aboutNode = createSingleElement("rdaelem:note", about);
					aboutNode.setAttribute(LANG, langAttr);
			relationNode.appendChild(isVersionOfNode);
			relationNode.appendChild(isReplacedByNode);
			relationNode.appendChild(replacesNode);
			relationNode.appendChild(isPartOfNode);
			relationNode.appendChild(hasPartNode);
			relationNode.appendChild(isDesignedWithNode);
			relationNode.appendChild(shouldAvoidNode);
			relationNode.appendChild(complementedByNode);
			relationNode.appendChild(aboutNode);
		Element coverageNode = createCompositeElement("dc:coverage", BAG, coverage);
				coverageNode.setAttribute(LANG, langAttr);
		Element rightsNode = createCompositeElement("dc:rights", BAG, rights);
				rightsNode.setAttribute(LANG, langAttr);
		Element historyNode = createSingleElement("dc2ap:history", null);
			Element historySeqNode = doc.createElement(SEQ);
				Element historyLiNode = doc.createElement(LI);
					Element eventDateNode = createDateElement("dc:date", eventDate);
					Element authorNode = createCompositeElement("rdaroles:author", SEQ, author); 
					Element reasonNode = createSingleElement("dc2ap:reason", reason);
							reasonNode.setAttribute(LANG, langAttr);
					Element changesNode = createSingleElement("dc2ap:changes", changes);
							changesNode.setAttribute(LANG, langAttr);
					historyLiNode.appendChild(eventDateNode);
					historyLiNode.appendChild(authorNode);
					historyLiNode.appendChild(reasonNode);
					historyLiNode.appendChild(changesNode);
			historySeqNode.appendChild(historyLiNode);
		historyNode.appendChild(historySeqNode);
		Element requirementsNode = createSingleElement("dc2ap:requirements", null);
			Element funcReqNode = createCompositeElement("dc2ap:functionalRequirements", SEQ, functionalRequirements);
					funcReqNode.setAttribute(LANG, langAttr);
			Element nonFuncreqNode = createCompositeElement("dc2ap:nonFunctionalRequirements", SEQ, nonFunctionalRequirements);
					nonFuncreqNode.setAttribute(LANG, langAttr);
			Element depAndContribNode = createSingleElement("dc2ap:dependenciesAndContributions", dependencesAndContributions);
					depAndContribNode.setAttribute(LANG, langAttr);
			Element dependencyGraphNode = createSingleElementAttr("dc2ap:dependencyGraph", RSC, dependencyGraph);
			Element contributionGraphNode = createSingleElementAttr("dc2ap:contributionGraph", RSC, contributionGraph);
			Element conflictAndResolNode = createCompositeElement("dc2ap:conflictAndResolution", BAG, conflictAndResolution);
					conflictAndResolNode.setAttribute(LANG, langAttr);
			Element prioritiesNode = createSingleElementAttr("dc2ap:priorities", RSC, prioritiesDiagram);
			Element participantsNode = createCompositeElement("dc2ap:participants", BAG, participants);
					participantsNode.setAttribute(LANG, langAttr);
			requirementsNode.appendChild(funcReqNode);
			requirementsNode.appendChild(nonFuncreqNode);
			requirementsNode.appendChild(depAndContribNode);
			requirementsNode.appendChild(dependencyGraphNode);
			requirementsNode.appendChild(contributionGraphNode);
			requirementsNode.appendChild(conflictAndResolNode);
			requirementsNode.appendChild(prioritiesNode);
			requirementsNode.appendChild(participantsNode);
		Element modelingNode = createSingleElement("dc2ap:modelling", null);
			Element behaviourNode = createSingleElement("dc2ap:behaviour", null);
				Element useCaseDiagNode = createSingleElementAttr("dc2ap:useCaseDiagram", RSC, useCaseDiagram);
				Element collabSeqDiagNode = createCompositeElementAttr("dc2ap:collaborationSequenceDiagrams", BAG, RSC, collaborationSequenceDiagrams);
				Element actStateDiagNode = createCompositeElementAttr("dc2ap:activityStateDiagrams", BAG, RSC, activityStateDiagrams);
				behaviourNode.appendChild(useCaseDiagNode);
				behaviourNode.appendChild(collabSeqDiagNode);
				behaviourNode.appendChild(actStateDiagNode);
			Element structureNode = createSingleElement("dc2ap:structure", null);
				Element classDiagramNode = createSingleElementAttr("dc2ap:classDiagram", RSC, classDiagram);
				Element classDescrNode = createCompositeElement("dc2ap:classDescriptions", BAG, classDescriptions);
						classDescrNode.setAttribute(LANG, langAttr);
				Element relatDescrNode = createCompositeElement("dc2ap:relationshipDescriptions", BAG, relationshipDescriptions);
						relatDescrNode.setAttribute(LANG, langAttr);
				structureNode.appendChild(classDiagramNode);
				structureNode.appendChild(classDescrNode);
				structureNode.appendChild(relatDescrNode);
			Element solVariantsNode = createCompositeElementAttr("dc2ap:solutionVariants", BAG, RSC, solutionVariants);
			modelingNode.appendChild(behaviourNode);
			modelingNode.appendChild(structureNode);
			modelingNode.appendChild(solVariantsNode);
		Element resultContextNode = createCompositeElement("dc2ap:resultingContext", BAG, resultingContext);
				resultContextNode.setAttribute(LANG, langAttr);
		Element desngGuidelinesNode = createCompositeElement("dc2ap:designGuidelines", BAG, designGuidelines);
				desngGuidelinesNode.setAttribute(LANG, langAttr);
		Element consequencesNode = createSingleElement("dc2ap:consequences", null);
				consequencesNode.setAttribute(LANG, langAttr);
			Element positiveNode = createCompositeElement("dc2ap:positive", BAG, positive);
					positiveNode.setAttribute(LANG, langAttr);
			Element negativeNode = createCompositeElement("dc2ap:negative", BAG, negative);
					negativeNode.setAttribute(LANG, langAttr);
			consequencesNode.appendChild(positiveNode);
			consequencesNode.appendChild(negativeNode);
			
		//*** add elements to the description element ***//
		descriptionNode.appendChild(indetifierNode);
		descriptionNode.appendChild(titleNode);
		descriptionNode.appendChild(altTitleNode);
		descriptionNode.appendChild(creatorNode);
		descriptionNode.appendChild(subjectNode);
		descriptionNode.appendChild(descrNode);
		descriptionNode.appendChild(publisherNode);
		descriptionNode.appendChild(contributorNode);
		descriptionNode.appendChild(dateNode);
		descriptionNode.appendChild(analysisPatternNode);
		descriptionNode.appendChild(formatNode);
		descriptionNode.appendChild(sourceNode);
		descriptionNode.appendChild(languageNode);
		descriptionNode.appendChild(relationNode);
		descriptionNode.appendChild(coverageNode);
		descriptionNode.appendChild(rightsNode);
		descriptionNode.appendChild(historyNode);
		descriptionNode.appendChild(requirementsNode);
		descriptionNode.appendChild(modelingNode);
		descriptionNode.appendChild(resultContextNode);
		descriptionNode.appendChild(desngGuidelinesNode);
		descriptionNode.appendChild(consequencesNode);
		
		//*** add description to the root element ***//
		rootNode.appendChild(descriptionNode);
		
		// generate the XML document			
		DOMSource DOMsrc = new DOMSource(doc);
		StringWriter stringWriter = new StringWriter(); 			
		StreamResult result = new StreamResult(stringWriter);
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");             
        transformer.transform(DOMsrc, result);  
		
        /*
		DOMSource DOMsrc = new DOMSource(doc);  
        StreamResult result = new StreamResult(new FileOutputStream("C:/saida/" + "saida.rdf"));  
        
        TransformerFactory transFactory = TransformerFactory.newInstance();
        Transformer transformer = transFactory.newTransformer();  
        transformer.setOutputProperty(OutputKeys.ENCODING, "ISO-8859-1");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
        transformer.transform(DOMsrc, result);  */  
        
		// String of the result
		String xmlString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
		xmlString += stringWriter.toString();
       
        return xmlString;
	}
	
	/**
	 * Create the XML string of the Metadata, in order to allow the
	 * application to save the project.
	 * 
	 * @throws ParserConfigurationException 
	 * @throws TransformerException 
	 */	
	public String generateProjectXmlScript(
			String identifier, String title, List<String> alternativeTitle,
			List<String> creator, List<String> subject, String problem,
			List<String> motivation, List<String> examples, List<String> knownUses,
			String context, List<String> publisher, List<String> contributor,
			String created, String modified, String type, String notation, 
			List<String> format, String source, String languageValue, 
			String languageLabel, String isVersionOf, List<String> isReplacedBy,
			List<String> replaces, List<String> isPartOf, List<String> hasPart, 
			List<String> isDesignedWith, List<String> shouldAvoid,
			List<String> complementedBy, String about, List<String> coverage, 
			List<String> rights, String eventDate, List<String> author,
			String reason, String changes, List<String> functionalRequirements,
			List<String> nonFunctionalRequirements, String dependencesAndContributions,
			String dependencyGraph, String contributionGraph, List<String> conflictAndResolution,
			String prioritiesDiagram, List<String> participants, String useCaseDiagram,
			List<String> collaborationSequenceDiagrams, List<String> activityStateDiagrams,
			String classDiagram, List<String> classDescriptions, List<String> relationshipDescriptions,
			List<String> solutionVariants, List<String> resultingContext, List<String> designGuidelines,
			List<String> positive, List<String> negative	
		) throws ParserConfigurationException, TransformerException  {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbf.newDocumentBuilder();

		// creates a new document
		doc = docBuilder.newDocument();
		
		// create root element <METADATA>
		Element rootNode = doc.createElement("METADATA"); 
		setRootAttributes(rootNode);
		doc.appendChild(rootNode);

		// create the Description node, which will append the DC2AP element nodes
		Element descriptionNode = createSingleElement("DESCRIPTION", null);
		
		// *** create DC2AP XML node elements *** //
		Element indetifierNode = createSingleElement("IDENTIFIER", identifier);
		Element titleNode = createSingleElement("TITLE", title);
		Element altTitleNode = createCompositeElement("ALTERNATIVE", BAG, alternativeTitle);
		Element creatorNode = createCompositeElement("CREATOR", SEQ, creator);
		Element subjectNode = createCompositeElement("SUBJECT", BAG, subject);
		Element descrNode = createSingleElement("DESCRIPTION", null);
			Element problemNode = createSingleElement("PROBLEM", problem);
			Element motivationNode = createCompositeElement("MOTIVATION", BAG, motivation);
			Element exampleNode = createCompositeElement("EXAMPLE", BAG, examples);
			Element knownUsesNode = createCompositeElement("KNOWNUSES", BAG, knownUses);
			Element contextNode = createSingleElement("CONTEXT", context);
			descrNode.appendChild(problemNode);	
			descrNode.appendChild(motivationNode);
			descrNode.appendChild(exampleNode);
			descrNode.appendChild(knownUsesNode);
			descrNode.appendChild(contextNode);
		Element publisherNode = createCompositeElement("PUBLISHER", SEQ, publisher);
		Element contributorNode = createCompositeElement("CONTRIBUTOR", SEQ, contributor);
		Element dateNode = createSingleElement("DATE", null);
			Element createdNode = createDateElement("CREATED", created);
			Element modifiedNode = createDateElement("MODIFIED", modified);
			dateNode.appendChild(createdNode);
			dateNode.appendChild(modifiedNode);
		Element analysisPatternNode = createSingleElement("ANALYSISPATTERN", null);
			Element typeNode = createSingleElement("TYPE", type);
			Element notationNode = createSingleElement("NOTATION", notation);
			analysisPatternNode.appendChild(typeNode);
			analysisPatternNode.appendChild(notationNode);
		Element formatNode = createFormatElement("FORMAT", format);
		Element sourceNode = createSingleElement("SOURCE", source);
		Element languageNode = createSingleElement("LANGUAGE", null);
			Element langTermNode = createSingleElement("dcterms:ISO639-3", null);
				Element langValueNode = createSingleElement("VALUE", languageValue);
				Element langLabelNode = createSingleElement("LABEL", languageLabel);
				langTermNode.appendChild(langValueNode);
				langTermNode.appendChild(langLabelNode);
			languageNode.appendChild(langTermNode);
		Element relationNode = createSingleElement("RELATION", null);
			Element isVersionOfNode = createSingleElement("ISVERSIONOF", isVersionOf);
			Element isReplacedByNode = createCompositeElement("ISREPLACEDBY", ALT, isReplacedBy);
			Element replacesNode = createCompositeElement("REPLACES", ALT, replaces);
			Element isPartOfNode = createCompositeElement("ISPARTOF", BAG, isPartOf);
			Element hasPartNode = createCompositeElement("HASPART", BAG, hasPart);
			Element isDesignedWithNode = createCompositeElement("ISDESIGNEDWITH", BAG, isDesignedWith);
			Element shouldAvoidNode = createCompositeElement("SHOULDAVOID", BAG, shouldAvoid);
			Element complementedByNode = createCompositeElement("COMPLEMENTEDBY", BAG, complementedBy);
			Element aboutNode = createSingleElement("ABOUT", about);
			relationNode.appendChild(isVersionOfNode);
			relationNode.appendChild(isReplacedByNode);
			relationNode.appendChild(replacesNode);
			relationNode.appendChild(isPartOfNode);
			relationNode.appendChild(hasPartNode);
			relationNode.appendChild(isDesignedWithNode);
			relationNode.appendChild(shouldAvoidNode);
			relationNode.appendChild(complementedByNode);
			relationNode.appendChild(aboutNode);
		Element coverageNode = createCompositeElement("COVERAGE", BAG, coverage);
		Element rightsNode = createCompositeElement("RIGHTS", BAG, rights);
		Element historyNode = createSingleElement("HISTORY", null);
			Element historySeqNode = doc.createElement(SEQ);
				Element historyLiNode = doc.createElement(LI);
					Element eventDateNode = createDateElement("EVENTDATE", eventDate);
					Element authorNode = createCompositeElement("AUTHOR", SEQ, author); 
					Element reasonNode = createSingleElement("REASON", reason);
					Element changesNode = createSingleElement("CHANGES", changes);
					historyLiNode.appendChild(eventDateNode);
					historyLiNode.appendChild(authorNode);
					historyLiNode.appendChild(reasonNode);
					historyLiNode.appendChild(changesNode);
			historySeqNode.appendChild(historyLiNode);
		historyNode.appendChild(historySeqNode);
		Element requirementsNode = createSingleElement("REQUIREMENTS", null);
			Element funcReqNode = createCompositeElement("FUNCTIONALREQUIREMENTS", SEQ, functionalRequirements);
			Element nonFuncreqNode = createCompositeElement("NONFUNCTIONALREQUIREMENTS", SEQ, nonFunctionalRequirements);
			Element depAndContribNode = createSingleElement("DEPENDENCIESANDCONTRIBUTIONS", dependencesAndContributions);
			Element dependencyGraphNode = createSingleElementAttr("DEPENDENCYGRAPH", RSC, dependencyGraph);
			Element contributionGraphNode = createSingleElementAttr("CONTRIBUTIONGRAPH", RSC, contributionGraph);
			Element conflictAndResolNode = createCompositeElement("CONFLICTANDRESOLUTION", BAG, conflictAndResolution);
			Element prioritiesNode = createSingleElementAttr("PRIORITIES", RSC, prioritiesDiagram);
			Element participantsNode = createCompositeElement("PARTICIPANTS", BAG, participants);
			requirementsNode.appendChild(funcReqNode);
			requirementsNode.appendChild(nonFuncreqNode);
			requirementsNode.appendChild(depAndContribNode);
			requirementsNode.appendChild(dependencyGraphNode);
			requirementsNode.appendChild(contributionGraphNode);
			requirementsNode.appendChild(conflictAndResolNode);
			requirementsNode.appendChild(prioritiesNode);
			requirementsNode.appendChild(participantsNode);
		Element modelingNode = createSingleElement("MODELING", null);
			Element behaviourNode = createSingleElement("BEHAVIOUR", null);
				Element useCaseDiagNode = createSingleElementAttr("USECASEDIAGRAM", RSC, useCaseDiagram);
				Element collabSeqDiagNode = createCompositeElementAttr("CALLABORATIONSEQUENCEDIAGRAMS", BAG, RSC, collaborationSequenceDiagrams);
				Element actStateDiagNode = createCompositeElementAttr("ACTIVITYSTATESDIAGRAMS", BAG, RSC, activityStateDiagrams);
				behaviourNode.appendChild(useCaseDiagNode);
				behaviourNode.appendChild(collabSeqDiagNode);
				behaviourNode.appendChild(actStateDiagNode);
			Element structureNode = createSingleElement("STRUCTURE", null);
				Element classDiagNode = createSingleElementAttr("CLASSDIAGRAM", RSC, null);
				Element classDescrNode = createCompositeElement("CLASSDESCRIPTIONS", BAG, classDescriptions);
				Element relatDescrNode = createCompositeElement("RELATIONSHIPDESCRIPTIONS", BAG, relationshipDescriptions);
				structureNode.appendChild(classDiagNode);
				structureNode.appendChild(classDescrNode);
				structureNode.appendChild(relatDescrNode);
			Element solVariantsNode = createCompositeElementAttr("SOLUTIONVARIANTS", BAG, RSC, solutionVariants);
			modelingNode.appendChild(behaviourNode);
			modelingNode.appendChild(structureNode);
			modelingNode.appendChild(solVariantsNode);
		Element resultContextNode = createCompositeElement("RESULTINGCONTEXT", BAG, resultingContext);
		Element desngGuidelinesNode = createCompositeElement("DESIGNGUIDELINES", BAG, designGuidelines);
		Element consequencesNode = createSingleElement("CONSEQUENCES", null);
			Element positiveNode = createCompositeElement("POSITIVE", BAG, positive);
			Element negativeNode = createCompositeElement("NEGATIVE", BAG, negative);
			consequencesNode.appendChild(positiveNode);
			consequencesNode.appendChild(negativeNode);
			
		// *** add elements to the description element *** //
		descriptionNode.appendChild(indetifierNode);
		descriptionNode.appendChild(titleNode);
		descriptionNode.appendChild(altTitleNode);
		descriptionNode.appendChild(creatorNode);
		descriptionNode.appendChild(subjectNode);
		descriptionNode.appendChild(descrNode);
		descriptionNode.appendChild(publisherNode);
		descriptionNode.appendChild(contributorNode);
		descriptionNode.appendChild(dateNode);
		descriptionNode.appendChild(analysisPatternNode);
		descriptionNode.appendChild(formatNode);
		descriptionNode.appendChild(sourceNode);
		descriptionNode.appendChild(languageNode);
		descriptionNode.appendChild(relationNode);
		descriptionNode.appendChild(coverageNode);
		descriptionNode.appendChild(rightsNode);
		descriptionNode.appendChild(historyNode);
		descriptionNode.appendChild(requirementsNode);
		descriptionNode.appendChild(modelingNode);
		descriptionNode.appendChild(resultContextNode);
		descriptionNode.appendChild(desngGuidelinesNode);
		descriptionNode.appendChild(consequencesNode);
		
		// *** add description to the root element *** //
		rootNode.appendChild(descriptionNode);
		
		// generate the XML document			
		DOMSource DOMsrc = new DOMSource(doc);
		StringWriter stringWriter = new StringWriter(); 			
		StreamResult result = new StreamResult(stringWriter);
		TransformerFactory transFactory = TransformerFactory.newInstance();
		Transformer transformer = transFactory.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");             
        transformer.transform(DOMsrc, result);  

        
		// String of the result
	    //String xmlString = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
        //String xmlString = "<project name=\"dc2ap-metadata-editor\" default=\"cb\">";
        String xmlString = stringWriter.toString();
        //xmlString += "</project>";
        
        return xmlString;
	}
	
	/**
	 * Create a single XML node with a text content (value).
	 * 
	 * @param tagName the name of the node
	 * @param value the text content of the node (value)
	 * @return Node Element
	 */
	private Element createSingleElement(String tagName, String value){
		Element element = doc.createElement(tagName);
		
		if(value != null){
			element.setTextContent(value);
		}
		
		return element;
	}
	
	/**
	 * Create a single XML node with a text content (value). Append the value
	 * as an attribute of the element. 
	 * 
	 * @param tagName the name of the node
	 * @param attrName the name of the attribute
	 * @param value the text content of the node (value)
	 * @return
	 */
	private Element createSingleElementAttr(String tagName, String attrName, String value){
		Element element = doc.createElement(tagName);		
		
		value = value != null ? value : "";
		
		element.setAttribute(attrName, value);
		
		return element;
	}
	
	/**
	 * Create a XML node which contains a group of values inside it.
	 * 
	 * @param tagName the name of the node
	 * @param compositeType the type of composite, Bag, Seq, Alt...
	 * @param listValue the list of text contents. Values of each line of the element
	 * @return Node Element
	 */
	private Element createCompositeElement(String tagName, String compositeType, List<String> listValue){
		Element element = doc.createElement(tagName);
		Element group = doc.createElement(compositeType);
		
		for(String value : listValue){
			Element liNode = doc.createElement(LI);
			liNode.setTextContent(value);
			group.appendChild(liNode);
		}

		element.appendChild(group);
		
		return element;
	}
	
	/**
	 * Create a XML node which contains a group of values inside it. Append the value
	 * as an attribute of the element.
	 * 
	 * @param tagName the name of the node
	 * @param compositeType the type of composite, Bag, Seq, Alt...
	 * @param attrName the name of the attribute
	 * @param listValue the list of text contents. Values of each line of the element
	 * @return Node Element
	 */
	private Element createCompositeElementAttr(String tagName, String compositeType, 
			String attrName, List<String> listValue){
		Element element = doc.createElement(tagName);
		Element group = doc.createElement(compositeType);
		
		for(String value : listValue){
			Element liNode = doc.createElement(LI);
			liNode.setAttribute(attrName, value);
			group.appendChild(liNode);
		}

		element.appendChild(group);
		
		return element;
	}
	
	/**
	 * Create a single XML node with a date content (value).
	 * 
	 * @param tagName the name of the node
	 * @param value the text content of the node (date value)
	 * @return Node Element
	 */
	private Element createDateElement(String tagName, String value){
		Element element = doc.createElement(tagName);
		Element w3cdtf = doc.createElement("dcterms:W3CDTF");
		Element elemValue = createSingleElement("rdf:value", value);
		w3cdtf.appendChild(elemValue);
		element.appendChild(w3cdtf);
		
		return element;
	}
	
	/**
	 * Create the Format node of the XML document, with its children nodes 
	 * and content (values).
	 * 
	 * @param tagName the name of the node
	 * @param value the text content of the node (date value)
	 * @return Node Element
	 */
	private Element createFormatElement(String tagName, List<String> listValue){
		Element format = doc.createElement(tagName);
			Element formatBag = createSingleElement(BAG, null);
	
				if(!listValue.isEmpty()){
					Element jpegLi = createSingleElement(LI, null);
					if(listValue.contains("JPEG")){
						Element jpeg = createSingleElement("dcterms:IMT", null);
							Element jpegValue = createSingleElement("rdf:value", "image/jpeg");
							Element jpegLabel = createSingleElement("rdfs:label", "JPEG");
							jpeg.appendChild(jpegValue);
							jpeg.appendChild(jpegLabel);
						jpegLi.appendChild(jpeg);
					}
					Element pngLi = createSingleElement(LI, null);
					if(listValue.contains("PNG")){
						Element png = createSingleElement("dcterms:IMT", null);
							Element pngValue = createSingleElement("rdf:value", "image/png");
							Element pngLabel = createSingleElement("rdfs:label", "PNG");
							png.appendChild(pngValue);
							png.appendChild(pngLabel);
						pngLi.appendChild(png);
					}
					Element xmiLi = createSingleElement(LI, null);
					if(listValue.contains("XMI")){
						Element xmi = createSingleElement("dc2apves:format", null);
							Element xmiValue = createSingleElement("dc2apft:xmi", null);
							Element xmiLabel = createSingleElement("rdfs:label", "XMI");
							xmi.appendChild(xmiValue);
							xmi.appendChild(xmiLabel);
						xmiLi.appendChild(xmi);
					}
					formatBag.appendChild(jpegLi);	
					formatBag.appendChild(pngLi);
					formatBag.appendChild(xmiLi);
				}
				
			format.appendChild(formatBag);
		
		return format;
	}

	/**
	 * Set the attributes of the root element
	 * @param root the root element
	 */
	private void setRootAttributes(Element root){
		root.setAttribute("xmlns:rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
		root.setAttribute("xmlns:rdfs", "http://www.w3.org/2000/01/rdf-schema#");
		root.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
		root.setAttribute("xmlns:dc2ap", "http://purl.org/dc2ap/elements/");
		root.setAttribute("xmlns:dc2aptp", "http://purl.org/dc2ap/type/");
		root.setAttribute("xmlns:dc2apft", "http://purl.org/dc2ap/format/");
		root.setAttribute("xmlns:dcterms", "http://purl.org/dc/terms/");
		root.setAttribute("xmlns:dcmitype", "http://purl.org/dc/dcmitype/");
		root.setAttribute("xmlns:rdaelem", "http://rdvocab.info/Elements/");
		root.setAttribute("xmlns:rdaroles", "http://rdvocab.info/roles/"); 
		root.setAttribute("xmlns:dc2apves", "http://purl.org/dc2ap/ves/");
	}
	

	/**
	 * Read a XML .rdf file from the server and process its information by
	 * setting its fields into a DC2AP object. (Turn a .rdf file into a 
	 * DC2AP object)
	 * 
	 * @param fullFilePath The complete path of the RDF file in the server
	 * 
	 * @return A DC2AP object
	 * 
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws SAXException
	 */
	public DC2AP readRdfFile(String fullFilePath) throws IOException, ParserConfigurationException, SAXException{

		// Parse the file and create the XML document
		File file = new File(fullFilePath);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(file);
		
		// ** get the root element
		Element root = doc.getDocumentElement();
		
		// get values of single nodes
		String identifier = getNodeValue(root, "dc:identifier");
		String title = getNodeValue(root, "dc:title");
		String source = getNodeValue(root, "dc:source");
		String problem = getNodeValue(root, "dc2ap:problem");
		String context = getNodeValue(root, "dc2ap:context");
		String isVersionOf = getNodeValue(root, "dcterms:isVersionOf");
		String about = getNodeValue(root, "rdaelem:note");
		String reason = getNodeValue(root, "dc2ap:reason");
		String changes = getNodeValue(root, "dc2ap:changes");
		String depAndContrib = getNodeValue(root, "dc2ap:dependenciesAndContributions");
		String dependencyGraph = getNodeAttributeValue(root, "dc2ap:dependencyGraph");
		String contributionGraph = getNodeAttributeValue(root, "dc2ap:contributionGraph");
		String prioritiesDiagram = getNodeAttributeValue(root, "dc2ap:priorities");
		String useCaseDiagram = getNodeAttributeValue(root, "dc2ap:useCaseDiagram");
		String classDiagram = getNodeAttributeValue(root, "dc2ap:classDiagram");
		
		// ** get values of multiple nodes
		List<String> altTitleList = getNodeValuesList(root, "dcterms:alternative");
		List<String> creatorList = getNodeValuesList(root, "dc:creator");
		List<String> subjectList = getNodeValuesList(root, "dc:subject");
		List<String> motivationList = getNodeValuesList(root, "dc2ap:motivation");
		List<String> exampleList = getNodeValuesList(root, "dc2ap:example");
		List<String> knownUsesList = getNodeValuesList(root, "dc2ap:knownUses");
		List<String> publiserList = getNodeValuesList(root, "dc:publisher");
		List<String> contributorList = getNodeValuesList(root, "dc:contributor");
		List<String> isRepByList = getNodeValuesList(root, "dcterms:isReplacedBy");
		List<String> replacesList = getNodeValuesList(root, "dcterms:replaces");
		List<String> isPartOfList = getNodeValuesList(root, "dcterms:isPartOf");
		List<String> hasPartList = getNodeValuesList(root, "dcterms:hasPart");
		List<String> isDesWithList = getNodeValuesList(root, "dc2ap:isDesignedWith");
		List<String> shouldAvList = getNodeValuesList(root, "dc2ap:shouldAvoid");
		List<String> compByList = getNodeValuesList(root, "dc2ap:complementedBy");
		List<String> coverageList = getNodeValuesList(root, "dc:coverage");
		List<String> rightsList = getNodeValuesList(root, "dc:rights");
		List<String> authorList = getNodeValuesList(root, "rdaroles:author");
		List<String> funcReqList = getNodeValuesList(root, "dc2ap:functionalRequirements");
		List<String> nonFuncReqList = getNodeValuesList(root, "dc2ap:nonFunctionalRequirements");
		List<String> confAndReslList = getNodeValuesList(root, "dc2ap:conflictAndResolution");
		List<String> participantsList = getNodeValuesList(root, "dc2ap:participants");
		List<String> classDescrList = getNodeValuesList(root, "dc2ap:classDescriptions");
		List<String> relatDescrList = getNodeValuesList(root, "dc2ap:relationshipDescriptions");
		List<String> resContList = getNodeValuesList(root, "dc2ap:resultingContext");
		List<String> desGuidelinesList = getNodeValuesList(root, "dc2ap:designGuidelines");
		List<String> positiveList = getNodeValuesList(root, "dc2ap:positive");
		List<String> negativeList = getNodeValuesList(root, "dc2ap:negative");
		List<String> colSeqDiagList = getNodeAttributeValuesList(root, "dc2ap:collaborationSequenceDiagrams");
		List<String> actStatDiagList = getNodeAttributeValuesList(root, "dc2ap:activityStateDiagrams");
		List<String> solutionVariants = getNodeAttributeValuesList(root, "dc2ap:solutionVariants");
		
		// ** other fields
		// Date created
		String created=null;
		NodeList createdNodeList = root.getElementsByTagName("dcterms:created");
		Element createdElem = (Element) createdNodeList.item(0);
		NodeList createdValueList = createdElem.getElementsByTagName("rdf:value");
		if(createdValueList.getLength() > 0){
			Node createdNode = createdValueList.item(0).getFirstChild();
			created = createdNode.getNodeValue();
		}
		
		// Dated modified
		String modified=null;
		NodeList modifiedNodeList = root.getElementsByTagName("dcterms:modified");
		Element modifiedElem = (Element) modifiedNodeList.item(0);
		NodeList modifiedValueList = modifiedElem.getElementsByTagName("rdf:value");
		if(modifiedValueList.getLength() > 0){
			Node modifiedNode = modifiedValueList.item(0).getFirstChild();
			modified = modifiedNode.getNodeValue();
		}
		
		// Type and Notation
		String type=null;
		String notation=null;
		NodeList anPtnNodeList = root.getElementsByTagName("dc2aptp:analysisPattern");
		Element anPtnElem = (Element) anPtnNodeList.item(0);
		NodeList typeNodeList = anPtnElem.getElementsByTagName("rdfs:label");
		NodeList notationList = anPtnElem.getElementsByTagName("rdaelem:formOfNotation");
		if(typeNodeList.getLength() > 0){
			Node typeNode = typeNodeList.item(0).getFirstChild();
			type = typeNode.getNodeValue();
		}
		if(notationList.getLength() > 0){
			Node notationNode = notationList.item(0).getFirstChild();
			notation = notationNode.getNodeValue();
		}
	
		// Format
		List<String> formatList = new ArrayList<String>();
		NodeList formatNodeList = root.getElementsByTagName("dc:format");
		Element formatElem = (Element) formatNodeList.item(0);
		NodeList formats = formatElem.getElementsByTagName("rdfs:label");
		for(int i=0; i<formats.getLength(); i++){
			Node formatNode = formats.item(i).getFirstChild();
			formatList.add(formatNode.getNodeValue());
		}
		
		// Language
		String languageValue=null;
		String languageLabel=null;
		NodeList langNodeList = root.getElementsByTagName("dc:language");
		Element langElem = (Element) langNodeList.item(0);
		NodeList valueList = langElem.getElementsByTagName("rdf:value");
		NodeList labelList = langElem.getElementsByTagName("rdfs:label");
		if(valueList.getLength() > 0){
			Node valueNode = valueList.item(0).getFirstChild();
			languageValue = valueNode.getNodeValue();
		}
		if(labelList.getLength() > 0){
			Node labelNode = labelList.item(0).getFirstChild();
			languageLabel = labelNode.getNodeValue();
		}
		
		// Event date
		String eventDate=null;
		NodeList historyNodeList = root.getElementsByTagName("dc2ap:history");
		Element historyElem = (Element) historyNodeList.item(0);
		NodeList eDateNodeList = historyElem.getElementsByTagName("dc:date");
		Element eDateElem = (Element) eDateNodeList.item(0);
		NodeList valueDateNodeList = eDateElem.getElementsByTagName("rdf:value");
		if(valueDateNodeList.getLength() > 0){
			Node dNode = valueDateNodeList.item(0).getFirstChild();
			eventDate = dNode.getNodeValue();
		}
		
		// set the properties into the DC2AP object
		DC2AP object = new DC2AP();

		object.setIdentifier(identifier);
		object.setTitle(title);
		object.setAlternativeTitle(altTitleList);
		object.setCreator(creatorList);
		object.setSubject(subjectList);
		object.setProblem(problem);
		object.setMotivation(motivationList);
		object.setExample(exampleList);
		object.setKnownUses(knownUsesList);
		object.setContext(context);
		object.setPublisher(publiserList);
		object.setContributor(contributorList);
		object.setCreated(created);
		object.setModified(modified);
		object.setType(type);
		object.setNotation(notation);
		object.setFormat(formatList);
		object.setSource(source);
		object.setLanguageValue(languageValue);
		object.setLanguageLabel(languageLabel);
		object.setIsVersionOf(isVersionOf);
		object.setIsReplacedBy(isRepByList);
		object.setReplaces(replacesList);
		object.setIsPartOf(isPartOfList);
		object.setHasPart(hasPartList);
		object.setIsDesignedWith(isDesWithList);
		object.setShouldAvoid(shouldAvList);
		object.setComplementedBy(compByList);
		object.setAbout(about);
		object.setCoverage(coverageList);
		object.setRights(rightsList);
		object.setEventDate(eventDate);
		object.setAuthor(authorList);
		object.setReason(reason);
		object.setChanges(changes);
		object.setFunctionalRequirements(funcReqList);
		object.setNonFunctionalRequirements(nonFuncReqList);
		object.setDependencesAndContributions(depAndContrib);
		object.setDependencyGraph(dependencyGraph);
		object.setContributionGraph(contributionGraph);
		object.setConflictAndResolution(confAndReslList);
		object.setPrioritiesDiagram(prioritiesDiagram);		
		object.setParticipants(participantsList);
		object.setUseCaseDiagram(useCaseDiagram);
		object.setCollaborationSequenceDiagrams(colSeqDiagList);
		object.setActivityStateDiagrams(actStatDiagList);
		object.setClassDiagram(classDiagram);
		object.setClassDescriptions(classDescrList);
		object.setRelationshipDescriptions(relatDescrList);	
		object.setSolutionVariants(solutionVariants);
		object.setResultingContext(resContList);
		object.setDesignGuidelines(desGuidelinesList);
		object.setPositive(positiveList);
		object.setNegative(negativeList);

		return object;
	}
	
	/**
	 * Return the value of a simple node from the RDF file.
	 * 
	 * @param root The root element of the RDF file.
	 * @param nodeName The name of the node to open.
	 * 
	 * @return The value of the node.
	 */
	private String getNodeValue(Element root, String nodeName){
		// get the node
		NodeList nodeList = root.getElementsByTagName(nodeName);
		if(nodeList.getLength() > 0){
			Node node = nodeList.item(0).getFirstChild();
			return node.getNodeValue();
		} else {
			return "";
		}		
	}
	
	/**
	 * Return a list of values from a node of the RDF file.
	 * The node can be a 'bag', 'seq' or 'alt'.
	 * All elements must be in a 'rdf:li' row.
	 * 
	 * @param root The root element of the RDF file.
	 * @param nodeName The name of the node to open.
	 * 
	 * @return A list of the values of the node.
	 */
	private List<String> getNodeValuesList(Element root, String nodeName){
		// get the node from the root element
		NodeList nodeList = root.getElementsByTagName(nodeName);
		Element elem = (Element) nodeList.item(0);  // bag, seq or alt
		NodeList liList = elem.getElementsByTagName(LI);
		
		List<String> listValues = new ArrayList<String>();
		for(int i=0; i<liList.getLength(); i++){
			Node node = liList.item(i).getFirstChild();
			listValues.add(node.getNodeValue());
		}
		
		return listValues;
	}
	
	/**
	 * Return the attribute value of a simple node from the RDF file.
	 * 
	 * @param root The root element of the RDF file.
	 * @param nodeName The name of the node to open.
	 * 
	 * @return The attribute value of the node.
	 */
	private String getNodeAttributeValue(Element root, String nodeName){
		// get the node
		NodeList nodeList = root.getElementsByTagName(nodeName);
		Element elem = (Element) nodeList.item(0);
		
		// get node attribute
		Attr attr = elem.getAttributeNode(RSC);
		
		if(attr != null){
			return attr.getValue();
		} else {
			return "";
		}		
	}

	
	/**
	 * Return a list of attribute values from a node
	 * of the RDF file.
	 * The node can be a 'bag', 'seq' or 'alt'.
	 * All elements must be in a 'rdf:li' row.
	 * 
	 * @param root The root element of the RDF file.
	 * @param nodeName The name of the node to open.
	 * 
	 * @return A list of the attribute values of the node.
	 */
	private List<String> getNodeAttributeValuesList(Element root, String nodeName){
		// get the node from the root element
		NodeList nodeList = root.getElementsByTagName(nodeName);
		Element elem = (Element) nodeList.item(0);  // bag, seq or alt	
		NodeList liList = elem.getElementsByTagName(LI);
		
		List<String> listValues = new ArrayList<String>();
		for(int i=0; i<liList.getLength(); i++){
			// get node attribute
			Element liElem = (Element) liList.item(i);
			Attr attr = liElem.getAttributeNode(RSC);
			
			if(attr != null){
				listValues.add(attr.getValue());
			}		
		}
		
		return listValues;
	}
	
	/*	
	// CLASS MAIN USED FOR TEST REASONS
	public static void main(String args[]){
		String filePath = "C:/Users/Douglas/Dropbox/patternportal/war/temporaryFiles/readedFile.rdf";
		
		DC2AP obj = new DC2AP();
		try {
			obj = readRdfFile(filePath);
			
			System.out.println("created: " + obj.getCreated());
			System.out.println("modified: " + obj.getModified());
			
			for(String s : obj.getActivityStateDiagrams()){
				System.out.println("ACT: " + s);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
	*/
}
