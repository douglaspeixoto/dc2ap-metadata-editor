package dc2ap.server.services;

import java.util.ArrayList;
import java.util.List;

import dc2ap.server.entities.Languages;

/**
 * Service to validate the DC2AP metadata.
 * 
 * @author Douglas A. Peixoto - 08/2012
 */
public class ValidateService {
	// list of errors found
	private List<String> errorLog = null;
	// days of each month
	private int[] monthDays = 
		{0,31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	// the languages to use in the validation process
	private LanguagesService fileService = new LanguagesService();
	
	/**
	 * Validate the metadata according with the DC2AP specification. 
	 * Here, we validate the Mandatory and Date fields; you are free 
	 * to implement the validation of the conditional field as you wish.
	 * 
	 * @return A list of error found. If the validation is successful, then 
	 * return an empty list, otherwise returns a string with the Error Log, 
	 * which contains the problems found in the validation.
	 * 
	 * @param - all DC2AP field which must be validated.
	 */
	public List<String> validateMetadata(
			String identifier, String title, List<String> creator,
			List<String> subject, String problem, List<String> motivation, 
			List<String> examples, String context, String created, String modified, 
			String type, String notation, List<String> format, String source, 
			String languageValue, String languageLabel, String eventDate, 
			List<String> author, String reason, List<String> functionalRequirements,
			String dependencesAndContributions, String dependencyGraph,
			String prioritiesDiagram, List<String> participants, String useCaseDiagram,
			List<String> collaborationSequenceDiagrams, String classDiagram, 
			List<String> classDescriptions, List<String> relationshipDescriptions,
			List<String> positive, List<String> negative){
		
		errorLog = new ArrayList<String>();
		
		//** fields to be validated **//
		
		// Mandatory fields
		validateSingleField(identifier, "1. Identifier");
		validateSingleField(title, "2. Title");
		validateMultipleField(creator, "3. Creator");
		validateMultipleField(subject, "4. Subject");
		validateSingleField(problem, "5.1. Problem");
		validateMultipleField(motivation,"5.2. Motivation");
		validateMultipleField(examples,"5.2.1. Example");
		validateSingleField(context, "5.3. Context");
		validateSingleField(created, "8.1. Created");
		validateDateField(created, "8.1. Created");
		validateDateField(modified, "8.2. Modified");
		validateSingleField(type, "9. Type");
		validateSingleField(notation, "9.1. Notation");
		validateMultipleField(format, "10. Format");
		validateSingleField(source, "11. Source");
		validateLanguageField(languageValue, languageLabel, "12. Language");
		validateSingleField(eventDate, "16.1. Event Date");
		validateDateField(eventDate, "16.1. Event Date");
		validateMultipleField(author, "16.2. Author");
		validateSingleField(reason, "16.3. Reason");
		validateMultipleField(functionalRequirements, "17.1. Functional Requirements");
		validateSingleField(dependencesAndContributions, "17.3. Dependencies and Contribution");
		validateSingleField(dependencyGraph, "17.3.1. Dependency Graph");
		validateSingleField(prioritiesDiagram, "17.5. Priorities Diagram");
		validateMultipleField(participants, "17.6. Participants");
		validateSingleField(useCaseDiagram, "18.1.1. Use Case Diagram");
		validateMultipleField(collaborationSequenceDiagrams, "18.1.2. Collaboration/Sequence Diagram");
		validateSingleField(classDiagram, "18.2.1. Class Diagram");
		validateMultipleField(classDescriptions, "18.2.2. Class Description");
		validateMultipleField(relationshipDescriptions, "18.2.3. Relationship Description");
		validateMultipleField(positive, "21.1. Positive");
		validateMultipleField(negative, "21.2. Negative");
		
		//** Optional and Conditional fields don't need to be validated **//
	
		return errorLog;
	}
	
	/**
	 * Validate the single fields of the resource.
	 * 
	 * @param field The field itself
	 * @param fieldName The name of the field
	 */
	private void validateSingleField(String field, String fieldName){
		if("".equals(field) || field == null){
			String msg = "The field (" + fieldName + ") can not be empty.";
			errorLog.add(msg);
		}
	}
	
	/**
	 * Validate the multiple fields of the resource.
	 * 
	 * @param field The field itself
	 * @param fieldName The name of the field
	 */
	private void validateMultipleField(List<String> list, String fieldName){
		if(list.size() == 0 || list == null){
			String msg = "The field (" + fieldName + ") can not be empty. " +
					"Add at least one element.";
			errorLog.add(msg);
		}
	}
	
	/**
	 * Validate the Date fields of the resource.
	 * 
	 * @param field The field itself
	 * @param fieldName The name of the field
	 */
	private void validateDateField(String date, String fieldName){
		if(!"".equals(date) && date != null){
			if(!verifyIfStringIsADate(date)){
				String msg = "The field (" + fieldName + ") is not a valid date.";
				errorLog.add(msg);
			} 
		}
	}
	
	/**
	 * Validate the field Languages.
	 * 
	 * @param languageValue The language value field
	 * @param languageLabel The language label field
	 * @param fieldName The name of the field
	 */
	private void validateLanguageField(String languageValue, String languageLabel, String fieldName){	
		if("".equals(languageValue) || languageValue == null){
			String msg = "The field (" + fieldName + " Value) can not be empty.";
			errorLog.add(msg);
		} 
		if ("".equals(languageLabel) || languageLabel == null){
			String msg = "The field (" + fieldName + " Label) can not be empty.";
			errorLog.add(msg);
		}
		
		if(!"".equals(languageValue) && languageValue != null){
			for(Languages lang : fileService.getLanguages()){
				if( (lang.getValue().toLowerCase()).equals(languageValue.toLowerCase()) && 
				   !(lang.getLabel().toLowerCase()).equals(languageLabel.toLowerCase()))
				{
					String msg = "The field (" + fieldName + ") does not match with its Value. " +
							"The label for this language value must be: '" + lang.getLabel() + "'";
					errorLog.add(msg);
				}
			}
		}
	}

	/**
	 * Verify if the String is a valid date.
	 * Obs: Date format must be (yyyy-MM-dd)
	 * 
	 * @return True if the string is a valid date,
	 * otherwise returns false.
	 */
	private boolean verifyIfStringIsADate(String date){
		String[] tokens = date.split("-");
	
		if(tokens.length < 3){
			return false;
		} 
		
		String y = tokens[0];
		String m = tokens[1];
		String d = tokens[2];
		
		int year, month, day;
		
		try {
			year = Integer.parseInt(y);
			month = Integer.parseInt(m);
			day = Integer.parseInt(d);
		} catch (NumberFormatException e) {
			return false;
		}
		
		// leap year
		if((year%4==0 && year%100!=0) || year%400==0){
			monthDays[2] = 29;
		}
		
		// Check the year (4 digits)
		if(y.length() != 4 || year < 0){
			return false;
		}
		// Check the month (2 digits)
		if(m.length() != 2 || month < 1 || month > 12){
			return false;
		}
		// Check the day (2 digits)
		if(d.length() != 2 || day < 0 || day > monthDays[month]){
			return false;
		}
		
		return true;
	}
}
