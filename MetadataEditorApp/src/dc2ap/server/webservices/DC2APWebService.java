package dc2ap.server.webservices;

import java.util.Arrays;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import dc2ap.server.services.ValidateService;
import dc2ap.server.services.XmlService;

/**
 * This class implements the Web Services of the 
 * DC2AP pattern used in the patternportal project.
 * 
 * NEVER change the methods of this class. 
 * Instead of it, you can add new methods or change the methods 
 * of the classes contained in the package dc2ap.server.services
 * 
 * @author Douglas A. Peixoto - 07/2012
 *
 */
@WebService
public class DC2APWebService {
	private ValidateService validateService = new ValidateService();
	private XmlService xmlService = new XmlService();
	
	/**
	 * Validate the resource according with the DC2AP specification. 
	 * Here, we validate the Mandatory and Date fields; you are free 
	 * to implement the validation of conditional fields as you wish.
	 * 
	 * @param - all DC2AP field which might be filled.
	 * 
	 * @return A list of error found. If the validation is successful, then 
	 * return an empty list, otherwise returns a string with the Error Log, 
	 * which contains the problems found in the validation.
	 */
	@WebMethod
	public String[] validateResource(
			String identifier, String title, String[] creator,
			String[] subject, String problem,
			String[] motivation, String[] example, String[] knownUses,
			String context, String[] publisher, String[] contributor,
			String created, String modified, String type, String notation, 
			String[] format, String source, String languageValue, 
			String languageLabel, String isVersionOf, String[] isReplacedBy,
			String[] replaces, String[] isPartOf, String[] hasPart, 
			String[] isDesignedWith, String[] shouldAvoid,
			String[] complementedBy, String about, String[] coverage, 
			String[] rights, String eventDate, String[] author,
			String reason, String changes, String[] functionalRequirements,
			String[] nonFunctionalRequirements, String dependencesAndContributions,
			String dependencyGraph, String contributionGraph, String[] conflictAndResolution,
			String prioritiesDiagram, String[] participants, String useCaseDiagram,
			String[] collaborationSequenceDiagrams, String[] activityStateDiagrams,
			String classDiagram, String[] classDescriptions, String[] relationshipDescriptions,
			String[] solutionVariants, String[] resultingContext, String[] designGuidelines,
			String[] positive, String[] negative	
		){
		
		List<String> errorLog = validateService.validateMetadata(identifier, title, 
				Arrays.asList(creator), Arrays.asList(subject), problem, 
				Arrays.asList(motivation), Arrays.asList(example), context, created, modified, 
				type, notation, Arrays.asList(format), source, languageValue, languageLabel, 
				eventDate, Arrays.asList(author), reason, Arrays.asList(functionalRequirements), 
				dependencesAndContributions, dependencyGraph, prioritiesDiagram, 
				Arrays.asList(participants), useCaseDiagram, Arrays.asList(collaborationSequenceDiagrams), 
				classDiagram, Arrays.asList(classDescriptions), Arrays.asList(relationshipDescriptions), 
				Arrays.asList(positive), Arrays.asList(negative));
		
		String[] arrayError = errorLog.toArray(new String[errorLog.size()]);
		
		return arrayError;
	}

	
	/**
	 * Validate and convert the fields of a DC2AP object into a RDF document.
	 * Return a string with the XML script of the resource, according with
	 * the DC2AP specification. Return error message in case of non validation 
	 * of the fields.
	 * 
	 * @param - all DC2AP field which might be filled.
	 * 
	 * @return if the fields are in accordance with the DC2AP specification, 
	 * then return a String with the script of the RDF document.
	 * 
	 * @throws Throws exception if the resource is not valid; in this case 
	 * return the message: 'ValidationException'. You might use the method 
	 * 'ValidateResource()' in order to check the error log.
	 * @throws Throws exception if the resource could not be parsed; in this 
	 * case return the message: 'ParserConfigurationException'.
	 * @throws Throws exception if the resource could not be transformed into a
	 * XML structure; in this case return the message: 'TransformerException'.
	 */
	@WebMethod
	public String generateRdfScript(
			String identifier, String title, String[] alternativeTitle,
			String[] creator, String[] subject, String problem,
			String[] motivation, String[] example, String[] knownUses,
			String context, String[] publisher, String[] contributor,
			String created, String modified, String type, String notation, 
			String[] format, String source, String languageValue, 
			String languageLabel, String isVersionOf, String[] isReplacedBy,
			String[] replaces, String[] isPartOf, String[] hasPart, 
			String[] isDesignedWith, String[] shouldAvoid,
			String[] complementedBy, String about, String[] coverage, 
			String[] rights, String eventDate, 	String[] author,
			String reason, String changes, String[] functionalRequirements,
			String[] nonFunctionalRequirements, String dependencesAndContributions,
			String dependencyGraph, String contributionGraph, String[] conflictAndResolution,
			String prioritiesDiagram, String[] participants, String useCaseDiagram,
			String[] collaborationSequenceDiagrams, String[] activityStateDiagrams,
			String classDiagram, String[] classDescriptions, String[] relationshipDescriptions,
			String[] solutionVariants, String[] resultingContext, String[] designGuidelines,
			String[] positive, String[] negative	
		){
		
		// validate the resource
		if(!validateService.validateMetadata(identifier, title, Arrays.asList(alternativeTitle),
				Arrays.asList(subject), problem,  Arrays.asList(motivation), Arrays.asList(example), 
				context, created, modified, type, notation, Arrays.asList(format), source, 
				languageValue, languageLabel, eventDate, Arrays.asList(author), reason, 
				Arrays.asList(functionalRequirements), dependencesAndContributions, dependencyGraph, 
				prioritiesDiagram, Arrays.asList(participants), useCaseDiagram,	
				Arrays.asList(collaborationSequenceDiagrams), classDiagram, 
				Arrays.asList(classDescriptions), Arrays.asList(relationshipDescriptions), 
				Arrays.asList(positive), Arrays.asList(negative)).isEmpty()){
			
			return "ValidationException";
		}

		try {
			return xmlService.generateRdfScript(identifier, title, Arrays.asList(alternativeTitle),
					Arrays.asList(creator), Arrays.asList(subject), problem, Arrays.asList(motivation), 
					Arrays.asList(example), Arrays.asList(knownUses), context, Arrays.asList(publisher),
					Arrays.asList(contributor), created, modified, type, notation, Arrays.asList(format), 
					source, languageValue, languageLabel, isVersionOf, Arrays.asList(isReplacedBy), 
					Arrays.asList(replaces), Arrays.asList(isPartOf), Arrays.asList(hasPart), 
					Arrays.asList(isDesignedWith), Arrays.asList(shouldAvoid), Arrays.asList(complementedBy), 
					about, Arrays.asList(coverage), Arrays.asList(rights), eventDate, Arrays.asList(author),
					reason, changes, Arrays.asList(functionalRequirements), 
					Arrays.asList(nonFunctionalRequirements), dependencesAndContributions, dependencyGraph, 
					contributionGraph, Arrays.asList(conflictAndResolution), prioritiesDiagram, 
					Arrays.asList(participants), useCaseDiagram, Arrays.asList(collaborationSequenceDiagrams), 
					Arrays.asList(activityStateDiagrams), classDiagram, Arrays.asList(classDescriptions), 
					Arrays.asList(relationshipDescriptions), Arrays.asList(solutionVariants), 
					Arrays.asList(resultingContext), Arrays.asList(designGuidelines), Arrays.asList(positive), 
					Arrays.asList(negative));
			
		} catch (ParserConfigurationException e) {
			return "ParserConfigurationException";
		} catch (TransformerException e) {
			return "TransformerException";
		}
	}
}